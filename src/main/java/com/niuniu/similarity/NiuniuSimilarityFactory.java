package com.niuniu.similarity;

import org.apache.lucene.search.similarities.Similarity;
import org.apache.solr.common.params.SolrParams;
import org.apache.solr.schema.SimilarityFactory;

public class NiuniuSimilarityFactory extends SimilarityFactory{
	protected boolean discountOverlaps;
	
	@Override
	public void init(SolrParams params){
		super.init(params);
		discountOverlaps = params.getBool("discountOverlaps", false);
	}

	@Override
	public Similarity getSimilarity() {
		NiuniuSimilarity niuniuStandardSimilarity = new NiuniuSimilarity();
		niuniuStandardSimilarity.setDiscountOverlaps(discountOverlaps);
		return niuniuStandardSimilarity;
	}
}
