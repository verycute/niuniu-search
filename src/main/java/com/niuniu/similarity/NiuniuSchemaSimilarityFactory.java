package com.niuniu.similarity;

import org.apache.lucene.search.similarities.*;
import org.apache.solr.schema.FieldType;
import org.apache.solr.schema.IndexSchema;
import org.apache.solr.schema.SchemaAware;
import org.apache.solr.schema.SimilarityFactory;

public class NiuniuSchemaSimilarityFactory extends SimilarityFactory implements SchemaAware{
	private Similarity similarity;
	  private Similarity niuniuSimilarity = new NiuniuSimilarity();

	  public void inform(final IndexSchema schema) {
	    similarity = new PerFieldSimilarityWrapper() {
	      @Override
	      public Similarity get(String name) {
	        FieldType fieldType = schema.getFieldTypeNoEx(name);
	        if (fieldType == null) {
	          return niuniuSimilarity;
	        } else {
	          Similarity similarity = fieldType.getSimilarity();
	          return similarity == null ? niuniuSimilarity : similarity;
	        }
	      }
	    };
	  }

	  @Override
	  public Similarity getSimilarity() {
	    assert similarity != null : "inform must be called first";
	    return similarity;
	  }
}
