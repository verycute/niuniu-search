package com.niuniu.similarity;

import org.apache.lucene.index.FieldInvertState;
import org.apache.lucene.search.similarities.TFIDFSimilarity;
import org.apache.lucene.util.BytesRef;

public class NiuniuStyleSimilarity extends TFIDFSimilarity {

	public NiuniuStyleSimilarity() {
	}

	public float coord(int overlap, int maxOverlap) {
		// return overlap / (float)maxOverlap;
		return 1.0f;
	}

	public float queryNorm(float sumOfSquaredWeights) {
		return 1.0f;
		// return (float)(1.0 / Math.sqrt(sumOfSquaredWeights));
	}

	public float lengthNorm(FieldInvertState state) {
		final int numTerms;
		if (discountOverlaps)
			numTerms = state.getLength() - state.getNumOverlap();
		else
			numTerms = state.getLength();
		return state.getBoost() * (1.0f + 0.15F * ((float) (1.0 / Math.sqrt(numTerms))));
	}

	public float tf(float freq) {
		return freq > 1.0f ? 1.0f : freq;
	}

	/** Implemented as <code>1 / (distance + 1)</code>. */
	@Override
	public float sloppyFreq(int distance) {
		return 1.0f / (distance + 1);
	}

	/** The default implementation returns <code>1</code> */
	@Override
	public float scorePayload(int doc, int start, int end, BytesRef payload) {
		return 1;
	}

	/** Implemented as <code>log(numDocs/(docFreq+1)) + 1</code>. */
	@Override
	public float idf(long docFreq, long numDocs) {
		return 1.0f;
	}

	/**
	 * True if overlap tokens (tokens with a position of increment of zero) are
	 * discounted from the document's length.
	 */
	protected boolean discountOverlaps = true;

	/**
	 * Determines whether overlap tokens (Tokens with 0 position increment) are
	 * ignored when computing norm. By default this is true, meaning overlap
	 * tokens do not count when computing norms.
	 *
	 * @lucene.experimental
	 *
	 * @see #computeNorm
	 */
	public void setDiscountOverlaps(boolean v) {
		discountOverlaps = v;
	}

	/**
	 * Returns true if overlap tokens are discounted from the document's length.
	 * 
	 * @see #setDiscountOverlaps
	 */
	public boolean getDiscountOverlaps() {
		return discountOverlaps;
	}

	@Override
	public String toString() {
		return "hehe";
	}
}
