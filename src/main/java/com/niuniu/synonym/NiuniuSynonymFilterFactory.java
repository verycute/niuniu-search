package com.niuniu.synonym;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CodingErrorAction;
import java.text.ParseException;
import java.util.List;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.core.LowerCaseFilter;
import org.apache.lucene.analysis.core.WhitespaceTokenizer;
import org.apache.lucene.analysis.synonym.SynonymMap;
import org.apache.lucene.analysis.synonym.SolrSynonymParser;
import org.apache.lucene.analysis.synonym.WordnetSynonymParser;
import org.apache.lucene.analysis.util.*;
import org.apache.lucene.util.Version;

public class NiuniuSynonymFilterFactory extends TokenFilterFactory implements ResourceLoaderAware {
	private SynonymMap map;
	private boolean ignoreCase;

	public TokenStream create(TokenStream input) {
		// if the fst is null, it means there's actually no synonyms... just
		// return the original stream
		// as there is nothing to do here.
		return map.fst == null ? input : new NiuniuSynonymFilter(input, map, ignoreCase);
	}

	public void inform(ResourceLoader loader) throws IOException {
		final boolean ignoreCase = getBoolean("ignoreCase", false);
		this.ignoreCase = ignoreCase;

		String tf = args.get("tokenizerFactory");

		final TokenizerFactory factory = tf == null ? null : loadTokenizerFactory(loader, tf);

		Analyzer analyzer = new Analyzer() {
			protected TokenStreamComponents createComponents(String fieldName, Reader reader) {
				Tokenizer tokenizer = factory == null ? new WhitespaceTokenizer(Version.LUCENE_42, reader)
						: factory.create(reader);
				TokenStream stream = ignoreCase ? new LowerCaseFilter(Version.LUCENE_42, tokenizer) : tokenizer;
				return new TokenStreamComponents(tokenizer, stream);
			}
		};

		String format = args.get("format");
		try {
			if (format == null || format.equals("solr")) {
				// TODO: expose dedup as a parameter?
				map = loadSolrSynonyms(loader, true, analyzer);
			} else if (format.equals("wordnet")) {
				map = loadWordnetSynonyms(loader, true, analyzer);
			} else {
				// TODO: somehow make this more pluggable
				throw new IllegalArgumentException("Unrecognized synonyms format: " + format);
			}
		} catch (ParseException e) {
			throw new IOException("Exception thrown while loading synonyms", e);
		}
	}

	/**
	 * Load synonyms from the solr format, "format=solr".
	 */
	private SynonymMap loadSolrSynonyms(ResourceLoader loader, boolean dedup, Analyzer analyzer)
			throws IOException, ParseException {
		final boolean expand = getBoolean("expand", true);
		String synonyms = args.get("synonyms");
		if (synonyms == null)
			throw new IllegalArgumentException("Missing required argument 'synonyms'.");

		CharsetDecoder decoder = Charset.forName("UTF-8").newDecoder().onMalformedInput(CodingErrorAction.REPORT)
				.onUnmappableCharacter(CodingErrorAction.REPORT);

		SolrSynonymParser parser = new SolrSynonymParser(dedup, expand, analyzer);
		File synonymFile = new File(synonyms);
		if (synonymFile.exists()) {
			decoder.reset();
			parser.add(new InputStreamReader(loader.openResource(synonyms), decoder));
		} else {
			List<String> files = splitFileNames(synonyms);
			for (String file : files) {
				decoder.reset();
				parser.add(new InputStreamReader(loader.openResource(file), decoder));
			}
		}
		return parser.build();
	}

	/**
	 * Load synonyms from the wordnet format, "format=wordnet".
	 */
	private SynonymMap loadWordnetSynonyms(ResourceLoader loader, boolean dedup, Analyzer analyzer)
			throws IOException, ParseException {
		final boolean expand = getBoolean("expand", true);
		String synonyms = args.get("synonyms");
		if (synonyms == null)
			throw new IllegalArgumentException("Missing required argument 'synonyms'.");

		CharsetDecoder decoder = Charset.forName("UTF-8").newDecoder().onMalformedInput(CodingErrorAction.REPORT)
				.onUnmappableCharacter(CodingErrorAction.REPORT);

		WordnetSynonymParser parser = new WordnetSynonymParser(dedup, expand, analyzer);
		File synonymFile = new File(synonyms);
		if (synonymFile.exists()) {
			decoder.reset();
			parser.add(new InputStreamReader(loader.openResource(synonyms), decoder));
		} else {
			List<String> files = splitFileNames(synonyms);
			for (String file : files) {
				decoder.reset();
				parser.add(new InputStreamReader(loader.openResource(file), decoder));
			}
		}
		return parser.build();
	}

	private TokenizerFactory loadTokenizerFactory(ResourceLoader loader, String cname) throws IOException {
		TokenizerFactory tokFactory = loader.newInstance(cname, TokenizerFactory.class);
		tokFactory.setLuceneMatchVersion(luceneMatchVersion);
		tokFactory.init(args);
		if (tokFactory instanceof ResourceLoaderAware) {
			((ResourceLoaderAware) tokFactory).inform(loader);
		}
		return tokFactory;
	}
}