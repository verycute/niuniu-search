package com.niuniu.queryparser;

import java.util.regex.Pattern;

import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.BooleanClause.Occur;
import org.apache.solr.common.params.SolrParams;
import org.apache.solr.common.util.NamedList;
import org.apache.solr.request.SolrQueryRequest;
import org.apache.solr.search.QParser;
import org.apache.solr.search.QParserPlugin;
import org.apache.solr.search.SyntaxError;

public class NiuniuDualQueryParserPlugin extends QParserPlugin{
	
	String regex = "\\d-\\d";
	Pattern dashPattern = null;

	String year_regex = "(20)?\\d{2}款";
	Pattern yearPattern = null;
	
	@Override
	@SuppressWarnings("rawtypes")
	public void init(NamedList args) {
		dashPattern = Pattern.compile(this.regex);
		yearPattern = Pattern.compile(this.year_regex);
	}
	
	public QParser createParser(String qstr, SolrParams localParams, SolrParams params, SolrQueryRequest req) {
		return new NiuniuDualQueryParser(qstr, localParams, params, req, dashPattern, yearPattern);
	}
	
	public class NiuniuDualQueryParser extends QParser{
		
		Pattern dashPattern = null;
		Pattern yearPattern = null;
		BooleanQuery q = null;
		
		public NiuniuDualQueryParser(String qstr, SolrParams localParams, SolrParams params, SolrQueryRequest req,
				Pattern dashPattern, Pattern yearPattern) {
			super(qstr, localParams, params, req);
			this.dashPattern = dashPattern;
			this.yearPattern = yearPattern;
			q = new BooleanQuery();
		}

		@Override
		public Query parse() throws SyntaxError {
			String oq = getString();
			String[] dual_queries = oq.split(" OR ");
			QParser qParser = null;
			for(String sub_q:dual_queries){
				qParser = new NiuniuQueryParser(sub_q, localParams, params, req, dashPattern, yearPattern);
				Query qq = qParser.parse();
				q.add(qq, Occur.SHOULD);
			}
			return q;
		}
		
	}
}
