package com.niuniu.queryparser;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.math.NumberUtils;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.CachingTokenFilter;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.PositionIncrementAttribute;
import org.apache.lucene.analysis.tokenattributes.TermToBytesRefAttribute;
import org.apache.lucene.analysis.tokenattributes.TypeAttribute;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanClause.Occur;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.WildcardQuery;
import org.apache.lucene.util.BytesRef;
import org.apache.solr.common.params.SolrParams;
import org.apache.solr.request.SolrQueryRequest;
import org.apache.solr.search.QParser;

public class NiuniuQueryParser extends QParser {

	private class SynonymUnit {
		BytesRef bytesRef;
		String type;

		public SynonymUnit(BytesRef bytesRef, String type) {
			this.bytesRef = bytesRef;
			this.type = type;
		}

		public BytesRef getBytesRef() {
			return bytesRef;
		}

		public String getType() {
			return type;
		}

	}

	Map<String, ArrayList<SynonymUnit>> synonym_map;
	ArrayList<BytesRef> brand_arr = null;
	ArrayList<BytesRef> model_arr = null;
	ArrayList<BytesRef> standard_arr = null;
	ArrayList<BytesRef> style_arr = null;
	ArrayList<BytesRef> price_model_arr = null;
	ArrayList<BytesRef> price_style_arr = null;
	ArrayList<BytesRef> guiding_arr = null;
	ArrayList<BytesRef> other_arr = null;
	ArrayList<BytesRef> price_model_style_arr = null;
	ArrayList<BytesRef> model_style_arr = null;
	ArrayList<BytesRef> area_arr = null;
	ArrayList<BytesRef> firm_arr = null;
	ArrayList<BytesRef> year_arr = null;
	Set<String> token_set;

	String[] fields = { "base_car_style", "remark","configuration_remark" };
	ArrayList<String> field_arr = null;

	Occur occurance = null;

	BooleanQuery q = null;
	Pattern dashPattern = null;
	Pattern yearPattern = null;

	public NiuniuQueryParser(String qstr, SolrParams localParams, SolrParams params, SolrQueryRequest req,
			Pattern dashPattern, Pattern yearPattern) {
		super(qstr, localParams, params, req);
		brand_arr = new ArrayList<BytesRef>();
		model_arr = new ArrayList<BytesRef>();
		standard_arr = new ArrayList<BytesRef>();
		style_arr = new ArrayList<BytesRef>();
		price_style_arr = new ArrayList<BytesRef>();
		price_model_arr = new ArrayList<BytesRef>();
		price_model_style_arr = new ArrayList<BytesRef>();
		model_style_arr = new ArrayList<BytesRef>();
		guiding_arr = new ArrayList<BytesRef>();
		other_arr = new ArrayList<BytesRef>();
		area_arr = new ArrayList<BytesRef>();
		firm_arr = new ArrayList<BytesRef>();
		year_arr = new ArrayList<BytesRef>();

		field_arr = new ArrayList<String>();
		synonym_map = new HashMap<String, ArrayList<SynonymUnit>>();

		for (int i = 0; i < fields.length; i++) {
			field_arr.add(fields[i]);
		}

		occurance = Occur.MUST;
		String search_level = params.get("search_level");
		if (search_level != null && "low".equals(search_level))
			occurance = Occur.SHOULD;

		this.dashPattern = dashPattern;
		this.yearPattern = yearPattern;
		
		this.token_set = new HashSet<String>();
	}

	// 全半角转换
	// 去除特殊字符 => IK tokenizer will do it
	// 繁体字转换？
	public String preProcess(String str) {
		Matcher m = dashPattern.matcher(str);
		while (m.find())
			str = str.replace(m.group(0), m.group(0).replaceAll("-", " "));
		return str.replaceAll("-", "");
	}

	private boolean isYearInfo(String str) {
		Matcher matcher = yearPattern.matcher(str);
		return matcher.matches();
	}

	private void reset() {
		brand_arr.clear();
		model_arr.clear();
		standard_arr.clear();
		style_arr.clear();
		price_style_arr.clear();
		price_model_arr.clear();
		price_model_style_arr.clear();
		model_style_arr.clear();
		guiding_arr.clear();
		other_arr.clear();
		firm_arr.clear();
		year_arr.clear();
		synonym_map.clear();
		token_set.clear();
		q = null;
	}

	// 根据term的tag决定去哪个field中查询该term
	private void termBelong(TypeAttribute type, BytesRef bytes) {
		if (type != null) {
			String token_type = type.type();
			if (token_type.equals("BRAND")) {
				brand_arr.add(BytesRef.deepCopyOf(bytes));
			} else if (token_type.equals("MODEL")) {
				String str = bytes.utf8ToString();
				if ( (str.length() > 2 && (str.endsWith("型") || str.endsWith("版") || str.endsWith("款"))) || (str.endsWith("级") && str.length()>1 && str.charAt(str.length()-2)>='a' && str.charAt(str.length()-2)<='z') ) {
					str = str.substring(0, str.length() - 1);
					BytesRef bytes2 = new BytesRef(str);
					model_arr.add(BytesRef.deepCopyOf(bytes2));
				} else {
					model_arr.add(BytesRef.deepCopyOf(bytes));
				}
			} else if (token_type.equals("STANDARD")) {
				standard_arr.add(BytesRef.deepCopyOf(bytes));
			} else if (token_type.equals("STYLE")) {
				String str = bytes.utf8ToString();
				if (isYearInfo(str)) {
					year_arr.add(BytesRef.deepCopyOf(bytes));
				} else {
					if ( (str.length() > 2 && (str.endsWith("型") || str.endsWith("版") || str.endsWith("款"))) || (str.endsWith("级") && str.length()>1 && str.charAt(str.length()-2)>='a' && str.charAt(str.length()-2)<='z') ) {
						str = str.substring(0, str.length() - 1);
						BytesRef bytes2 = new BytesRef(str);
						style_arr.add(BytesRef.deepCopyOf(bytes2));
					} else {
						style_arr.add(BytesRef.deepCopyOf(bytes));
					}
				}
			} else if (token_type.equals("MODEL_PRICE")) {
				price_model_arr.add(BytesRef.deepCopyOf(bytes));
			} else if (token_type.equals("STYLE_PRICE")) {
				price_style_arr.add(BytesRef.deepCopyOf(bytes));
			} else if (token_type.equals("FPRICE")) {
				guiding_arr.add(BytesRef.deepCopyOf(bytes));
			} else if (token_type.equals("MODEL_STYLE")) {
				String str = bytes.utf8ToString();
				if ( (str.length() > 2 && (str.endsWith("型") || str.endsWith("版") || str.endsWith("款"))) || (str.endsWith("级") && str.length()>1 && str.charAt(str.length()-2)>='a' && str.charAt(str.length()-2)<='z') ) {
					str = str.substring(0, str.length() - 1);
					BytesRef bytes2 = new BytesRef(str);
					model_style_arr.add(BytesRef.deepCopyOf(bytes2));
				} else {
					model_style_arr.add(BytesRef.deepCopyOf(bytes));
				}
			} else if (token_type.equals("AREA")) {
				area_arr.add(BytesRef.deepCopyOf(bytes));
			} else if (token_type.equals("MODEL_STYLE_PRICE")) {
				price_model_style_arr.add(BytesRef.deepCopyOf(bytes));
			} else if (token_type.equals("FIRM")) {
				firm_arr.add(BytesRef.deepCopyOf(bytes));
			} else if (token_type.startsWith("SYNONYM")) {
				processSynonymTerm(type, bytes);
			} else {
				String str = bytes.utf8ToString();
				if ( (str.length() > 2 && (str.endsWith("型") || str.endsWith("版") || str.endsWith("款"))) || (str.endsWith("级") && str.length()>1 && str.charAt(str.length()-2)>='a' && str.charAt(str.length()-2)<='z') ) {
					str = str.substring(0, str.length() - 1);
					BytesRef bytes2 = new BytesRef(str);
					other_arr.add(BytesRef.deepCopyOf(bytes2));
				} else {
					other_arr.add(BytesRef.deepCopyOf(bytes));
				}
			}
		} else {
			other_arr.add(BytesRef.deepCopyOf(bytes));
		}
	}

	private void generateQuery() {
		BooleanQuery brandQuery = new BooleanQuery();// MUST brand_arr
		BooleanQuery modelQuery = new BooleanQuery();
		BooleanQuery standardQuery = new BooleanQuery();// MUST standard_arr
		BooleanQuery styleQuery = new BooleanQuery();
		BooleanQuery guidePriceQuery = new BooleanQuery();// MUST guiding_arr
		BooleanQuery areaQuery = new BooleanQuery();
		BooleanQuery firmQuery = new BooleanQuery();
		BooleanQuery yearQuery = new BooleanQuery();
		BooleanQuery otherQuery = new BooleanQuery();

		// ========================================
		// || ||
		// || MUST ||
		// || ||
		// ========================================
		for (int i = 0; i < brand_arr.size(); i++) {
			Query tmpQuery = new TermQuery(new Term("brand_name", BytesRef.deepCopyOf(brand_arr.get(i))));
			brandQuery.add(tmpQuery, BooleanClause.Occur.SHOULD);
		}
		for (int i = 0; i < model_arr.size(); i++) {
			Query tmpQuery = new TermQuery(new Term("car_model_name", BytesRef.deepCopyOf(model_arr.get(i))));
			modelQuery.add(tmpQuery, BooleanClause.Occur.MUST);
		}

		for (int i = 0; i < area_arr.size(); i++) {
			Query tmpQuery1 = new TermQuery(new Term("user_prov_name_index", BytesRef.deepCopyOf(area_arr.get(i))));
			Query tmpQuery2 = new TermQuery(new Term("user_city_name_index", BytesRef.deepCopyOf(area_arr.get(i))));
			Query tmpQuery3 = new TermQuery(new Term("filter_car_in_region_index", BytesRef.deepCopyOf(area_arr.get(i))));
			Query tmpQuery4 = new TermQuery(new Term("filter_car_in_area_index", BytesRef.deepCopyOf(area_arr.get(i))));
			tmpQuery1.setBoost(0.2f);
			tmpQuery2.setBoost(0.2f);
			tmpQuery3.setBoost(0.5f);
			tmpQuery4.setBoost(0.5f);
			BooleanQuery bq = new BooleanQuery();
			bq.add(tmpQuery1, BooleanClause.Occur.SHOULD);
			bq.add(tmpQuery2, BooleanClause.Occur.SHOULD);
			bq.add(tmpQuery3, BooleanClause.Occur.SHOULD);
			bq.add(tmpQuery4, BooleanClause.Occur.SHOULD);
			areaQuery.add(bq, BooleanClause.Occur.MUST);
		}
		for (int i = 0; i < standard_arr.size(); i++) {
			Query tmpQuery = new TermQuery(new Term("standard_name", BytesRef.deepCopyOf(standard_arr.get(i))));
			standardQuery.add(tmpQuery, BooleanClause.Occur.SHOULD);
		}
		for (int i = 0; i < firm_arr.size(); i++) {
			Query tmpQuery = new TermQuery(new Term("firm_name", BytesRef.deepCopyOf(firm_arr.get(i))));
			firmQuery.add(tmpQuery, BooleanClause.Occur.SHOULD);
		}
		for (int i = 0; i < style_arr.size(); i++) {
			Query tmpQuery = new TermQuery(new Term("base_car_style", BytesRef.deepCopyOf(style_arr.get(i))));
			String str = style_arr.get(i).utf8ToString();
			if (str.endsWith("款")) {
				tmpQuery.setBoost(0.2f);
			} else {
				tmpQuery.setBoost(0.5f);
			}
			styleQuery.add(tmpQuery, BooleanClause.Occur.SHOULD);
		}
		for (int i = 0; i < year_arr.size(); i++) {
			Query tmpQuery = new TermQuery(new Term("base_car_year_info", BytesRef.deepCopyOf(year_arr.get(i))));
			tmpQuery.setBoost(0.1f);
			yearQuery.add(tmpQuery, BooleanClause.Occur.SHOULD);
		}
		for (int i = 0; i < guiding_arr.size(); i++) {
			Query tmpQuery = new TermQuery(new Term("guiding_price", BytesRef.deepCopyOf(guiding_arr.get(i))));
			tmpQuery.setBoost(0.5f);
			guidePriceQuery.add(tmpQuery, BooleanClause.Occur.SHOULD);
		}

		for (int i = 0; i < price_model_arr.size(); i++) {
			BooleanQuery tmpBq = new BooleanQuery();
			Query tmpQuery = new TermQuery(new Term("base_car_NO", BytesRef.deepCopyOf(price_model_arr.get(i))));
			tmpBq.add(tmpQuery, BooleanClause.Occur.SHOULD);
			tmpQuery = new TermQuery(new Term("last_price_NO", BytesRef.deepCopyOf(price_model_arr.get(i))));
			tmpBq.add(tmpQuery, BooleanClause.Occur.SHOULD);
			tmpQuery = new TermQuery(new Term("parent_price_NO", BytesRef.deepCopyOf(price_model_arr.get(i))));
			tmpBq.add(tmpQuery, BooleanClause.Occur.SHOULD);
			// priceQuery.add(tmpQuery, BooleanClause.Occur.SHOULD);
			tmpQuery = new TermQuery(new Term("car_model_name", BytesRef.deepCopyOf(price_model_arr.get(i))));
			tmpBq.add(tmpQuery, BooleanClause.Occur.SHOULD);
			// modelQuery.add(tmpQuery, BooleanClause.Occur.SHOULD);
			q.add(tmpBq, BooleanClause.Occur.MUST);
		}

		for (int i = 0; i < price_style_arr.size(); i++) {
			BooleanQuery tmpBq = new BooleanQuery();
			Query tmpQuery = new TermQuery(new Term("base_car_NO", BytesRef.deepCopyOf(price_style_arr.get(i))));
			tmpQuery.setBoost(0.5f);
			tmpBq.add(tmpQuery, BooleanClause.Occur.SHOULD);
			tmpQuery = new TermQuery(new Term("last_price_NO", BytesRef.deepCopyOf(price_style_arr.get(i))));
			tmpQuery.setBoost(0.5f);
			tmpBq.add(tmpQuery, BooleanClause.Occur.SHOULD);
			tmpQuery = new TermQuery(new Term("parent_price_NO", BytesRef.deepCopyOf(price_style_arr.get(i))));
			tmpQuery.setBoost(0.5f);
			tmpBq.add(tmpQuery, BooleanClause.Occur.SHOULD);
			// priceQuery.add(tmpQuery, BooleanClause.Occur.SHOULD);
			tmpQuery = new TermQuery(new Term("base_car_style", BytesRef.deepCopyOf(price_style_arr.get(i))));
			tmpQuery.setBoost(0.5f);
			tmpBq.add(tmpQuery, BooleanClause.Occur.SHOULD);
			// styleQuery.add(tmpQuery, BooleanClause.Occur.SHOULD);
			q.add(tmpBq, BooleanClause.Occur.MUST);
		}

		for (int i = 0; i < model_style_arr.size(); i++) {
			BooleanQuery tmpBq = new BooleanQuery();
			Query tmpQuery = new TermQuery(new Term("base_car_style", BytesRef.deepCopyOf(model_style_arr.get(i))));
			tmpQuery.setBoost(0.5f);
			tmpBq.add(tmpQuery, BooleanClause.Occur.SHOULD);
			// priceQuery.add(tmpQuery, BooleanClause.Occur.SHOULD);
			tmpQuery = new TermQuery(new Term("car_model_name", BytesRef.deepCopyOf(model_style_arr.get(i))));
			tmpBq.add(tmpQuery, BooleanClause.Occur.SHOULD);
			// modelQuery.add(tmpQuery, BooleanClause.Occur.SHOULD);
			q.add(tmpBq, BooleanClause.Occur.MUST);
		}
		for (int i = 0; i < price_model_style_arr.size(); i++) {
			BooleanQuery tmpBq = new BooleanQuery();
			Query tmpQuery = new TermQuery(new Term("base_car_NO", BytesRef.deepCopyOf(price_model_style_arr.get(i))));
			tmpQuery.setBoost(0.5f);
			tmpBq.add(tmpQuery, BooleanClause.Occur.SHOULD);
			tmpQuery = new TermQuery(new Term("last_price_NO", BytesRef.deepCopyOf(price_model_style_arr.get(i))));
			tmpQuery.setBoost(0.5f);
			tmpBq.add(tmpQuery, BooleanClause.Occur.SHOULD);
			tmpQuery = new TermQuery(new Term("parent_price_NO", BytesRef.deepCopyOf(price_model_style_arr.get(i))));
			tmpQuery.setBoost(0.5f);
			tmpBq.add(tmpQuery, BooleanClause.Occur.SHOULD);
			// priceQuery.add(tmpQuery, BooleanClause.Occur.SHOULD);
			tmpQuery = new TermQuery(new Term("base_car_style", BytesRef.deepCopyOf(price_model_style_arr.get(i))));
			tmpQuery.setBoost(0.5f);
			tmpBq.add(tmpQuery, BooleanClause.Occur.SHOULD);
			// styleQuery.add(tmpQuery, BooleanClause.Occur.SHOULD);
			tmpQuery = new TermQuery(new Term("car_model_name", BytesRef.deepCopyOf(price_model_style_arr.get(i))));
			tmpBq.add(tmpQuery, BooleanClause.Occur.SHOULD);
			// modelQuery.add(tmpQuery, BooleanClause.Occur.SHOULD);
			q.add(tmpBq, BooleanClause.Occur.MUST);
		}

		if (brand_arr.size() > 0) {
			q.add(brandQuery, BooleanClause.Occur.MUST);
		}
		if (model_arr.size() > 0) {
			q.add(modelQuery, BooleanClause.Occur.MUST);
		}
		if (standard_arr.size() > 0) {
			q.add(standardQuery, BooleanClause.Occur.MUST);
		}
		if (firm_arr.size() > 0) {
			q.add(firmQuery, BooleanClause.Occur.SHOULD);
		}
		if (style_arr.size() > 0) {
			q.add(styleQuery, occurance);
		}
		if (year_arr.size() > 0) {
			q.add(yearQuery, occurance);
		}
		if (guiding_arr.size() > 0) {
			q.add(guidePriceQuery, occurance);
		}

		if (area_arr.size() > 0) {
			areaQuery.setBoost(0.2f);
			q.add(areaQuery, occurance);
		}
		// ========================================
		// || ||
		// || MUST ||
		// || ||
		// ========================================

		// ========================================
		// || ||
		// || SHOULD ||
		// || ||
		// ========================================
		for (int i = 0; i < other_arr.size(); i++) {
			for (int j = 0; j < field_arr.size(); j++) {
				Query tmpQuery = new TermQuery(new Term(field_arr.get(j), BytesRef.deepCopyOf(other_arr.get(i))));
				if (field_arr.get(j).equals("remark") || field_arr.get(j).equals("configuration_remark")) {
					tmpQuery.setBoost(0.001f);
				} else
					tmpQuery.setBoost(0.01f);
				otherQuery.add(tmpQuery, BooleanClause.Occur.SHOULD);
			}
			
			Query cq = null;
			try{
				String tmp_str = other_arr.get(i).utf8ToString();
				QParser parser = QParser.getParser("outer_color:" + tmp_str + " OR " + "inner_color:" + tmp_str, "lucene", req);
				cq = parser.getQuery();
			}catch(Exception e){
				e.printStackTrace();
			}
			if(cq!=null){
				cq.setBoost(0.02f);
				otherQuery.add(cq, Occur.SHOULD);
			}
		}
		if (other_arr.size() > 0) {
			q.add(otherQuery, occurance);
			// System.out.println(other_arr.size());
		}
		// ========================================
		// || ||
		// || SHOULD ||
		// || ||
		// ========================================

	}

	private void processSynonymTerm(TypeAttribute type, BytesRef bytes) {
		String typeStr = type.type();
		int idx = typeStr.indexOf('|');
		String originalTerm = typeStr.substring(idx + 1);
		String realTypeStr = typeStr.substring(0, idx);
		if (synonym_map.containsKey(originalTerm)) {
			synonym_map.get(originalTerm).add(new SynonymUnit(BytesRef.deepCopyOf(bytes), realTypeStr));
		} else {
			ArrayList<SynonymUnit> subArray = new ArrayList<SynonymUnit>();
			subArray.add(new SynonymUnit(BytesRef.deepCopyOf(bytes), realTypeStr));
			synonym_map.put(originalTerm, subArray);
		}
	}

	private void appendSynonymQuery() {
		Iterator<Map.Entry<String, ArrayList<SynonymUnit>>> entries = synonym_map.entrySet().iterator();
		while (entries.hasNext()) {
			Map.Entry<String, ArrayList<SynonymUnit>> entry = entries.next();
			ArrayList<SynonymUnit> value = (ArrayList<SynonymUnit>) entry.getValue();
			BooleanQuery tmpBq = new BooleanQuery();
			Occur local_occurance = occurance;
			for (SynonymUnit su : value) {
				String type = su.getType();
				BytesRef bytes = su.getBytesRef();
				Query tmpQ = null;
				if (type.equals("SYNONYM_MODEL")) {
					local_occurance = BooleanClause.Occur.MUST;
					tmpQ = new TermQuery(new Term("car_model_name", BytesRef.deepCopyOf(bytes)));
				} else if (type.equals("SYNONYM_STYLE")) {
					tmpQ = new TermQuery(new Term("base_car_style", BytesRef.deepCopyOf(bytes)));
					String str = bytes.utf8ToString();
					if (str.endsWith("款")) {
						tmpQ.setBoost(0.2f);
					} else {
						tmpQ.setBoost(0.5f);
					}
				} else if (type.equals("SYNONYM_PRICE")) {
					BooleanQuery tmp = new BooleanQuery();
					Query tmpQuery = new TermQuery(new Term("base_car_NO", BytesRef.deepCopyOf(bytes)));
					tmp.add(tmpQuery, BooleanClause.Occur.SHOULD);
					tmpQuery = new TermQuery(new Term("last_price_NO", BytesRef.deepCopyOf(bytes)));
					tmp.add(tmpQuery, BooleanClause.Occur.SHOULD);
					tmpQuery = new TermQuery(new Term("parent_price_NO", BytesRef.deepCopyOf(bytes)));
					tmp.add(tmpQuery, BooleanClause.Occur.SHOULD);
					
					tmp = (BooleanQuery) tmpQ;
				}
				if (tmpQ != null)
					tmpBq.add(tmpQ, BooleanClause.Occur.SHOULD);
			}
			q.add(tmpBq, local_occurance);
		}
	}

	/**
	 * query:奥迪a4宝马x6 => (brand:奥迪 OR brand:宝马) AND (model:a4 OR model:x6)
	 *
	 */
	@SuppressWarnings("resource")
	@Override
	public Query parse() {
		reset();
		if (this.qstr.equals("*:*")) {
			String defaultField = getReq().getSchema().getDefaultSearchFieldName();
			Query q = new WildcardQuery(new Term(defaultField, "*"));
			return q;
		}
		this.qstr = preProcess(this.qstr);
		String df = req.getSchema().getDefaultSearchFieldName();
		Analyzer analyzer = req.getSchema().getQueryAnalyzer();
		TokenStream source;
		try {
			source = analyzer.tokenStream(df, new StringReader(this.qstr));
			source.reset();
		} catch (IOException e) {
			return null;
		}
		CachingTokenFilter buffer = new CachingTokenFilter(source);
		TermToBytesRefAttribute termAtt = null;
		PositionIncrementAttribute posIncrAtt = null;
		TypeAttribute type = null;
		int numTokens = 0;

		buffer.reset();

		if (buffer.hasAttribute(TermToBytesRefAttribute.class)) {
			termAtt = buffer.getAttribute(TermToBytesRefAttribute.class);
		}
		if (buffer.hasAttribute(PositionIncrementAttribute.class)) {
			posIncrAtt = buffer.getAttribute(PositionIncrementAttribute.class);
		}
		if (buffer.hasAttribute(TypeAttribute.class)) {
			type = buffer.getAttribute(TypeAttribute.class);
		}

		int positionCount = 0;

		boolean hasMoreTokens = false;
		if (termAtt != null) {
			try {
				hasMoreTokens = buffer.incrementToken();
				while (hasMoreTokens) {
					numTokens++;
					int positionIncrement = (posIncrAtt != null) ? posIncrAtt.getPositionIncrement() : 1;
					if (positionIncrement != 0) {
						positionCount += positionIncrement;
					}
					hasMoreTokens = buffer.incrementToken();
				}
			} catch (IOException e) {
				// ignore
			}
		}
		try {
			// rewind the buffer stream
			buffer.reset();
			// close original stream - all tokens buffered
			source.close();
		} catch (IOException e) {
			return null;
		}

		BytesRef bytes = termAtt == null ? null : termAtt.getBytesRef();

		if (numTokens == 0)
			return null;
		else {
			q = new BooleanQuery(positionCount == 1);
			String str, s_type, tmp_key;
			for (int i = 0; i < numTokens; i++) {
				try {
					boolean hasNext = buffer.incrementToken();
					assert hasNext == true;
					termAtt.fillBytesRef();
				} catch (IOException e) {
					// safe to ignore, because we know the number of
					// tokens
				}
				str = bytes.utf8ToString();
				s_type = type.type();
				tmp_key = str + "_" + s_type;
				if(i==0){
					 int str_i = NumberUtils.toInt(str,0);
					 if(str_i>0 && str_i<10)
						 continue;
				}
				if(!token_set.contains(tmp_key)){
					termBelong(type, bytes);
					token_set.add(tmp_key);
				}
			}
			generateQuery();
			appendSynonymQuery();
			// boolean niuFacet = params.getBool("niufacet", false);
			// return niuFacet?q:new NiuniuQuery(q);
			return q;
		}
	}
}
