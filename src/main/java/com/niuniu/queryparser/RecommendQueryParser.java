package com.niuniu.queryparser;

import java.io.IOException;
import java.io.StringReader;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.CachingTokenFilter;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.PositionIncrementAttribute;
import org.apache.lucene.analysis.tokenattributes.TermToBytesRefAttribute;
import org.apache.lucene.analysis.tokenattributes.TypeAttribute;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanClause.Occur;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.WildcardQuery;
import org.apache.lucene.util.BytesRef;
import org.apache.solr.common.params.SolrParams;
import org.apache.solr.request.SolrQueryRequest;
import org.apache.solr.search.QParser;

public class RecommendQueryParser extends QParser {
	Map<BytesRef, Float> brand_arr = null;
	Map<BytesRef, Float> model_arr = null;
	Map<BytesRef, Float> standard_arr = null;
	Map<BytesRef, Float> style_arr = null;
	Map<BytesRef, Float> price_arr = null;
	Map<BytesRef, Float> guiding_arr = null;
	Map<BytesRef, Float> other_arr = null;
	Map<BytesRef, Float> area_arr = null;
	HashSet<String> brandInfo = null;
	HashSet<String> modelInfo = null;
	String[] fields = { "base_car_style", "remark", "inner_color", "outer_color", "configuration_remark", "firm_name",
			"show_price" };
	ArrayList<String> field_arr = null;
	int total_term_count = 0;
	Occur priceOccur = Occur.MUST;
	HashMap<String, HashSet<String>> modelBrandMapping;
	BooleanQuery q = null;

	public RecommendQueryParser(String qstr, SolrParams localParams, SolrParams params, SolrQueryRequest req,
			HashMap<String, HashSet<String>> modelBrandMapping) {
		super(qstr, localParams, params, req);
		brand_arr = new HashMap<BytesRef, Float>();
		model_arr = new HashMap<BytesRef, Float>();
		standard_arr = new HashMap<BytesRef, Float>();
		style_arr = new HashMap<BytesRef, Float>();
		price_arr = new HashMap<BytesRef, Float>();
		guiding_arr = new HashMap<BytesRef, Float>();
		other_arr = new HashMap<BytesRef, Float>();
		area_arr = new HashMap<BytesRef, Float>();

		this.modelBrandMapping = modelBrandMapping;
		brandInfo = new HashSet<String>();
		modelInfo = new HashSet<String>();

		field_arr = new ArrayList<String>();
		for (int i = 0; i < fields.length; i++) {
			field_arr.add(fields[i]);
		}
	}

	// 全半角转换
	// 去除特殊字符 => IK tokenizer will do it
	// 繁体字转换？
	public String preProcess(String str) {
		// TODO
		return "";
	}

	private void reset() {
		brand_arr.clear();
		model_arr.clear();
		standard_arr.clear();
		style_arr.clear();
		guiding_arr.clear();
		price_arr.clear();
		area_arr.clear();
		other_arr.clear();

		brandInfo.clear();
		modelInfo.clear();
		q = null;
	}

	private void addToMap(Map<BytesRef, Float> mm, BytesRef bytes) {
		float value = 0.0f;
		if (mm.containsKey(bytes)) {
			value = mm.get(bytes);
		}
		mm.put(bytes, value + 1.0f);
	}

	private void modelBrandMapping() {
		for (String model : modelInfo) {
			if (!modelBrandMapping.containsKey(model))
				continue;
			HashSet<String> brandSet = modelBrandMapping.get(model);
			boolean status = false;
			for (String brand : brandInfo) {
				if (brandSet.contains(brand)) {
					status = true;
					break;
				}
			}
			if (!status) {
				for (String str : brandSet) {
					addToMap(brand_arr, new BytesRef(str));
				}
			}
		}
	}

	// 根据term的tag决定去哪个field中查询该term
	private void termBelong(TypeAttribute type, BytesRef bytes) {
		total_term_count += 1;
		if (type != null) {
			String token_type = type.type();
			if (token_type.equals("BRAND")) {
				brandInfo.add(bytes.utf8ToString());
				addToMap(brand_arr, BytesRef.deepCopyOf(bytes));
			} else if (token_type.equals("MODEL")) {
				modelInfo.add(bytes.utf8ToString());
				addToMap(model_arr, BytesRef.deepCopyOf(bytes));
			} else if (token_type.equals("STANDARD")) {
				addToMap(standard_arr, BytesRef.deepCopyOf(bytes));
			} else if (token_type.equals("STYLE")) {
				addToMap(style_arr, BytesRef.deepCopyOf(bytes));
			} else if (token_type.equals("MODEL_PRICE")) {
				modelInfo.add(bytes.utf8ToString());
				addToMap(model_arr, BytesRef.deepCopyOf(bytes));
				addToMap(price_arr, BytesRef.deepCopyOf(bytes));
			} else if (token_type.equals("STYLE_PRICE")) {
				addToMap(price_arr, BytesRef.deepCopyOf(bytes));
				addToMap(style_arr, BytesRef.deepCopyOf(bytes));
			} else if (token_type.equals("FPRICE")) {
				addToMap(guiding_arr, BytesRef.deepCopyOf(bytes));
			} else if (token_type.equals("MODEL_STYLE")) {
				addToMap(model_arr, BytesRef.deepCopyOf(bytes));
				addToMap(style_arr, BytesRef.deepCopyOf(bytes));
			} else if (token_type.equals("AREA")) {
				addToMap(area_arr, BytesRef.deepCopyOf(bytes));
			} else if (token_type.equals("MODEL_STYLE_PRICE")) {
				addToMap(price_arr, BytesRef.deepCopyOf(bytes));
				addToMap(model_arr, BytesRef.deepCopyOf(bytes));
				addToMap(style_arr, BytesRef.deepCopyOf(bytes));
			} else {
				total_term_count -= 1;
				addToMap(other_arr, BytesRef.deepCopyOf(bytes));
			}
		} else {
			total_term_count -= 1;
			addToMap(other_arr, BytesRef.deepCopyOf(bytes));
		}
	}

	private float round(float value, int scale) {
		BigDecimal bdBigDecimal = new BigDecimal(value);
		bdBigDecimal = bdBigDecimal.setScale(scale, BigDecimal.ROUND_HALF_UP);
		return bdBigDecimal.floatValue();
	}

	private void generateQuery() {
		total_term_count += 1;
		BooleanQuery brandQuery = new BooleanQuery();// MUST brand_arr
		BooleanQuery modelQuery = new BooleanQuery();
		BooleanQuery priceQuery = new BooleanQuery();
		BooleanQuery standardQuery = new BooleanQuery();// MUST standard_arr
		BooleanQuery styleQuery = new BooleanQuery();
		BooleanQuery guidePriceQuery = new BooleanQuery();// MUST guiding_arr
		BooleanQuery areaQuery = new BooleanQuery();
		BooleanQuery otherQuery = new BooleanQuery();

		// ========================================
		// || ||
		// || MUST ||
		// || ||
		// ========================================
		for (Map.Entry<BytesRef, Float> entry : brand_arr.entrySet()) {
			Query tmpQuery = new TermQuery(new Term("brand_name", BytesRef.deepCopyOf(entry.getKey())));
			float weight = round(entry.getValue() / (float) total_term_count, 4);
			tmpQuery.setBoost(weight);
			brandQuery.add(tmpQuery, BooleanClause.Occur.SHOULD);
		}
		for (Map.Entry<BytesRef, Float> entry : model_arr.entrySet()) {
			Query tmpQuery = new TermQuery(new Term("car_model_name", BytesRef.deepCopyOf(entry.getKey())));
			float weight = round(entry.getValue() / (float) total_term_count, 4);
			tmpQuery.setBoost(weight);
			modelQuery.add(tmpQuery, BooleanClause.Occur.SHOULD);
		}
		for (Map.Entry<BytesRef, Float> entry : area_arr.entrySet()) {
			float weight = round(entry.getValue() / (float) total_term_count, 4);
			Query tmpQuery1 = new TermQuery(new Term("user_prov_name_index", BytesRef.deepCopyOf(entry.getKey())));
			Query tmpQuery2 = new TermQuery(new Term("user_city_name_index", BytesRef.deepCopyOf(entry.getKey())));
			tmpQuery1.setBoost(weight);
			tmpQuery2.setBoost(weight);
			BooleanQuery bq = new BooleanQuery();
			bq.add(tmpQuery1, BooleanClause.Occur.SHOULD);
			bq.add(tmpQuery2, BooleanClause.Occur.SHOULD);
			areaQuery.add(bq, BooleanClause.Occur.SHOULD);
		}
		for (Map.Entry<BytesRef, Float> entry : standard_arr.entrySet()) {
			Query tmpQuery = new TermQuery(new Term("standard_name", BytesRef.deepCopyOf(entry.getKey())));
			float weight = round(entry.getValue() / (float) total_term_count, 4);
			tmpQuery.setBoost(weight);
			standardQuery.add(tmpQuery, BooleanClause.Occur.SHOULD);
		}
		for (Map.Entry<BytesRef, Float> entry : style_arr.entrySet()) {
			Query tmpQuery = new TermQuery(new Term("base_car_style", BytesRef.deepCopyOf(entry.getKey())));
			float weight = round(entry.getValue() / (float) total_term_count, 4);
			String str = entry.getKey().utf8ToString();
			if (str.endsWith("款")) {
				tmpQuery.setBoost(0.2f * weight);
			} else {
				tmpQuery.setBoost(0.7f * weight);
			}
			styleQuery.add(tmpQuery, BooleanClause.Occur.SHOULD);
		}
		for (Map.Entry<BytesRef, Float> entry : guiding_arr.entrySet()) {
			Query tmpQuery = new TermQuery(new Term("guiding_price", BytesRef.deepCopyOf(entry.getKey())));
			float weight = round(entry.getValue() / (float) total_term_count, 4);
			tmpQuery.setBoost(weight);
			guidePriceQuery.add(tmpQuery, BooleanClause.Occur.SHOULD);
		}
		for (Map.Entry<BytesRef, Float> entry : price_arr.entrySet()) {
			float weight = round(entry.getValue() / (float) total_term_count, 4);
			Query tmpQuery1 = new TermQuery(new Term("base_car_NO", BytesRef.deepCopyOf(entry.getKey())));
			tmpQuery1.setBoost(weight);
			priceQuery.add(tmpQuery1, BooleanClause.Occur.SHOULD);
		}

		if (brand_arr.size() > 0) {
			q.add(brandQuery, BooleanClause.Occur.MUST);
			// System.out.println(brand_arr.size());
		}
		if (model_arr.size() > 0) {
			q.add(modelQuery, BooleanClause.Occur.MUST);
			// System.out.println(model_arr.size());
		}
		if (standard_arr.size() > 0) {
			q.add(standardQuery, BooleanClause.Occur.SHOULD);
			// System.out.println(standard_arr.size());
		}
		if (style_arr.size() > 0) {
			q.add(styleQuery, BooleanClause.Occur.SHOULD);
			// System.out.println(standard_arr.size());
		}
		if (guiding_arr.size() > 0) {
			q.add(guidePriceQuery, BooleanClause.Occur.SHOULD);
		}
		if (price_arr.size() > 0) {
			q.add(priceQuery, priceOccur);
		}

		if (area_arr.size() > 0) {
			areaQuery.setBoost(0.2f);
			q.add(areaQuery, BooleanClause.Occur.SHOULD);
		}
		// ========================================
		// || ||
		// || MUST ||
		// || ||
		// ========================================

		// ========================================
		// || ||
		// || SHOULD ||
		// || ||
		// ========================================
		for (Map.Entry<BytesRef, Float> entry : other_arr.entrySet()) {
			for (int j = 0; j < field_arr.size(); j++) {
				Query tmpQuery = new TermQuery(new Term(field_arr.get(j), BytesRef.deepCopyOf(entry.getKey())));
				float weight = round(entry.getValue() / (float) total_term_count, 4);
				tmpQuery.setBoost(weight * 0.2f);
				otherQuery.add(tmpQuery, BooleanClause.Occur.SHOULD);
			}
		}
		if (other_arr.size() > 0) {
			q.add(otherQuery, BooleanClause.Occur.SHOULD);
			// System.out.println(other_arr.size());
		}
		// ========================================
		// || ||
		// || SHOULD ||
		// || ||
		// ========================================

	}

	/**
	 * query:奥迪a4宝马x6 => (brand:奥迪 OR brand:宝马) AND (model:a4 OR model:x6)
	 *
	 */
	@SuppressWarnings("resource")
	public Query parse() {
		reset();
		if (this.qstr.equals("*:*")) {
			String defaultField = getReq().getSchema().getDefaultSearchFieldName();
			Query q = new WildcardQuery(new Term(defaultField, "*"));
			return q;
		}
		if (this.qstr.startsWith("niuniu")) {
			this.qstr = this.qstr.substring(7);
			priceOccur = Occur.SHOULD;
		}
		String df = req.getSchema().getDefaultSearchFieldName();
		Analyzer analyzer = req.getSchema().getQueryAnalyzer();
		TokenStream source;
		try {
			source = analyzer.tokenStream(df, new StringReader(this.qstr));
			source.reset();
		} catch (IOException e) {
			return null;
		}
		CachingTokenFilter buffer = new CachingTokenFilter(source);
		TermToBytesRefAttribute termAtt = null;
		PositionIncrementAttribute posIncrAtt = null;
		TypeAttribute type = null;
		int numTokens = 0;

		buffer.reset();

		if (buffer.hasAttribute(TermToBytesRefAttribute.class)) {
			termAtt = buffer.getAttribute(TermToBytesRefAttribute.class);
		}
		if (buffer.hasAttribute(PositionIncrementAttribute.class)) {
			posIncrAtt = buffer.getAttribute(PositionIncrementAttribute.class);
		}
		if (buffer.hasAttribute(TypeAttribute.class)) {
			type = buffer.getAttribute(TypeAttribute.class);
		}

		int positionCount = 0;

		boolean hasMoreTokens = false;
		if (termAtt != null) {
			try {
				hasMoreTokens = buffer.incrementToken();
				while (hasMoreTokens) {
					numTokens++;
					int positionIncrement = (posIncrAtt != null) ? posIncrAtt.getPositionIncrement() : 1;
					if (positionIncrement != 0) {
						positionCount += positionIncrement;
					}
					hasMoreTokens = buffer.incrementToken();
				}
			} catch (IOException e) {
				// ignore
			}
		}
		try {
			// rewind the buffer stream
			buffer.reset();
			// close original stream - all tokens buffered
			source.close();
		} catch (IOException e) {
			return null;
		}

		BytesRef bytes = termAtt == null ? null : termAtt.getBytesRef();

		if (numTokens == 0)
			return null;
		else {
			q = new BooleanQuery(positionCount == 1);
			for (int i = 0; i < numTokens; i++) {
				try {
					boolean hasNext = buffer.incrementToken();
					assert hasNext == true;
					termAtt.fillBytesRef();
				} catch (IOException e) {
					// safe to ignore, because we know the number of
					// tokens
				}
				termBelong(type, bytes);
			}
			modelBrandMapping();
			generateQuery();
			return q;
		}
	}
}
