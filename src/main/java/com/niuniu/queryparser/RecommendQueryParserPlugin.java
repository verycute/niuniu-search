package com.niuniu.queryparser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.HashSet;

import org.apache.lucene.analysis.util.ResourceLoader;
import org.apache.lucene.analysis.util.ResourceLoaderAware;
import org.apache.solr.common.params.SolrParams;
import org.apache.solr.common.util.NamedList;
import org.apache.solr.request.SolrQueryRequest;
import org.apache.solr.search.QParser;
import org.apache.solr.search.QParserPlugin;

/*
 * 通过群资源来找寻车
 * 有一种特殊情况
 * 群资源信息较为复杂，会出现这种case “A4 黑黑 2889 A3 黑白1699 捷豹fx518 捷豹fx618”
 * 按照之前的queryparser，会去检索捷豹下的“a4, a3, fx518 fx618”
 */

public class RecommendQueryParserPlugin extends QParserPlugin implements ResourceLoaderAware {

	private HashMap<String, HashSet<String>> modelBrandMapping;

	private String mappingFileName;

	public HashMap<String, HashSet<String>> getModelBrandMapping() {
		return modelBrandMapping;
	}

	@Override
	@SuppressWarnings("rawtypes")
	public void init(NamedList args) {
		this.mappingFileName = (String) args.get("modelBrandMapping");
	}

	public void readModelBrandMapping(ResourceLoader loader, String filename) {
		if (filename == null) {
			return;
		}
		InputStream is = null;
		try {
			is = loader.openResource(filename);
			if (is == null) {
				throw new RuntimeException(filename + " Dictionary not found!!!");
			}
			BufferedReader br = new BufferedReader(new InputStreamReader(is, "UTF-8"), 512);
			String theWord = null;
			if (modelBrandMapping == null) {
				modelBrandMapping = new HashMap<String, HashSet<String>>();
			}
			do {
				theWord = br.readLine();
				if (theWord != null && !"".equals(theWord.trim()) && theWord.startsWith("#") == false) {
					String[] strs = theWord.toLowerCase().split("\t");
					if (strs.length != 2)
						continue;
					if (modelBrandMapping.containsKey(strs[0])) {
						HashSet<String> brandSet = modelBrandMapping.get(strs[0]);
						if (!brandSet.contains(strs[1])) {
							brandSet.add(strs[1]);
						}
					} else {
						HashSet<String> brandSet = new HashSet<String>();
						brandSet.add(strs[1]);
						modelBrandMapping.put(strs[0], brandSet);
					}
				}
			} while (theWord != null);

		} catch (IOException ioe) {
			System.err.println("modelBrandMapping file loading exception.");
			ioe.printStackTrace();
		} finally {
			try {
				if (is != null) {
					is.close();
					is = null;
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public QParser createParser(String qstr, SolrParams localParams, SolrParams params, SolrQueryRequest req) {
		return new RecommendQueryParser(qstr, localParams, params, req, modelBrandMapping);
	}

	@Override
	public void inform(ResourceLoader loader) throws IOException {
		if (!this.mappingFileName.isEmpty())
			readModelBrandMapping(loader, this.mappingFileName);
	}

}
