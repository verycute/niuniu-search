package com.niuniu.queryparser;

import java.util.regex.Pattern;

import org.apache.solr.common.params.SolrParams;
import org.apache.solr.common.util.NamedList;
import org.apache.solr.request.SolrQueryRequest;
import org.apache.solr.search.QParser;
import org.apache.solr.search.QParserPlugin;

public class NiuniuQueryParserPlugin extends QParserPlugin {
	String regex = "\\d-\\d";
	Pattern dashPattern = null;

	String year_regex = "(20)?\\d{2}款";
	Pattern yearPattern = null;

	@Override
	@SuppressWarnings("rawtypes")
	public void init(NamedList args) {
		dashPattern = Pattern.compile(this.regex);
		yearPattern = Pattern.compile(this.year_regex);
	}

	@Override
	public QParser createParser(String qstr, SolrParams localParams, SolrParams params, SolrQueryRequest req) {
		return new NiuniuQueryParser(qstr, localParams, params, req, dashPattern, yearPattern);
	}

}
