package com.niuniu.queryparser;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.CachingTokenFilter;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.PositionIncrementAttribute;
import org.apache.lucene.analysis.tokenattributes.TermToBytesRefAttribute;
import org.apache.lucene.analysis.tokenattributes.TypeAttribute;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.WildcardQuery;
import org.apache.lucene.util.BytesRef;
import org.apache.solr.common.params.SolrParams;
import org.apache.solr.request.SolrQueryRequest;
import org.apache.solr.search.QParser;

public class RawResourceQueryParser extends QParser {
	Map<BytesRef, Float> brand_arr = null;
	Map<BytesRef, Float> model_arr = null;
	Map<BytesRef, Float> standard_arr = null;
	Map<BytesRef, Float> style_arr = null;
	Map<BytesRef, Float> price_arr = null;
	Map<BytesRef, Float> guiding_arr = null;
	Map<BytesRef, Float> other_arr = null;
	Map<BytesRef, Float> area_arr = null;

	String[] fields = { "base_car_style", "remark", "inner_color", "outer_color", "configuration_remark", "firm_name",
			"show_price" };
	ArrayList<String> field_arr = null;
	int total_term_count = 0;

	BooleanQuery q = null;

	public RawResourceQueryParser(String qstr, SolrParams localParams, SolrParams params, SolrQueryRequest req) {
		super(qstr, localParams, params, req);
		brand_arr = new HashMap<BytesRef, Float>();
		model_arr = new HashMap<BytesRef, Float>();
		standard_arr = new HashMap<BytesRef, Float>();
		style_arr = new HashMap<BytesRef, Float>();
		price_arr = new HashMap<BytesRef, Float>();
		guiding_arr = new HashMap<BytesRef, Float>();
		other_arr = new HashMap<BytesRef, Float>();
		area_arr = new HashMap<BytesRef, Float>();

		field_arr = new ArrayList<String>();

		for (int i = 0; i < fields.length; i++) {
			field_arr.add(fields[i]);
		}
	}

	// 全半角转换
	// 去除特殊字符 => IK tokenizer will do it
	// 繁体字转换？
	public String preProcess(String str) {
		// TODO
		return "";
	}

	private void reset() {
		brand_arr.clear();
		model_arr.clear();
		standard_arr.clear();
		style_arr.clear();
		price_arr.clear();
		guiding_arr.clear();
		other_arr.clear();
		q = null;
	}

	private void addToMap(Map<BytesRef, Float> mm, BytesRef bytes) {
		float value = 0.0f;
		if (mm.containsKey(bytes)) {
			value = mm.get(bytes);
		}
		mm.put(bytes, value + 1.0f);
	}

	// 根据term的tag决定去哪个field中查询该term
	private void termBelong(TypeAttribute type, BytesRef bytes) {
		total_term_count += 1;
		if (type != null) {
			String token_type = type.type();
			if (token_type.equals("BRAND")) {
				addToMap(brand_arr, BytesRef.deepCopyOf(bytes));
			} else if (token_type.equals("MODEL")) {
				addToMap(model_arr, BytesRef.deepCopyOf(bytes));
			} else if (token_type.equals("STANDARD")) {
				addToMap(standard_arr, BytesRef.deepCopyOf(bytes));
			} else if (token_type.equals("STYLE")) {
				addToMap(style_arr, BytesRef.deepCopyOf(bytes));
			} else if (token_type.equals("MODEL_PRICE")) {
				addToMap(model_arr, BytesRef.deepCopyOf(bytes));
				addToMap(price_arr, BytesRef.deepCopyOf(bytes));
			} else if (token_type.equals("STYLE_PRICE")) {
				addToMap(price_arr, BytesRef.deepCopyOf(bytes));
				addToMap(style_arr, BytesRef.deepCopyOf(bytes));
			} else if (token_type.equals("FPRICE")) {
				addToMap(guiding_arr, BytesRef.deepCopyOf(bytes));
			} else if (token_type.equals("MODEL_STYLE")) {
				addToMap(model_arr, BytesRef.deepCopyOf(bytes));
				addToMap(style_arr, BytesRef.deepCopyOf(bytes));
			} else if (token_type.equals("AREA")) {
				addToMap(area_arr, BytesRef.deepCopyOf(bytes));
			} else if (token_type.equals("MODEL_STYLE_PRICE")) {
				addToMap(price_arr, BytesRef.deepCopyOf(bytes));
				addToMap(model_arr, BytesRef.deepCopyOf(bytes));
				addToMap(style_arr, BytesRef.deepCopyOf(bytes));
			} else {
				total_term_count -= 1;
				addToMap(other_arr, BytesRef.deepCopyOf(bytes));
			}
		} else {
			total_term_count -= 1;
			addToMap(other_arr, BytesRef.deepCopyOf(bytes));
		}
	}

	private void generateQuery() {
		BooleanQuery brandQuery = new BooleanQuery();// MUST brand_arr
		BooleanQuery modelQuery = new BooleanQuery();
		BooleanQuery guidePriceQuery = new BooleanQuery();// MUST guiding_arr
		BooleanQuery priceQuery = new BooleanQuery();// MUST guiding_arr
		total_term_count += 1;
		// ========================================
		// || ||
		// || MUST ||
		// || ||
		// ========================================
		for (Map.Entry<BytesRef, Float> entry : brand_arr.entrySet()) {
			Query tmpQuery = new TermQuery(new Term("content", BytesRef.deepCopyOf(entry.getKey())));
			brandQuery.add(tmpQuery, BooleanClause.Occur.SHOULD);
		}
		for (Map.Entry<BytesRef, Float> entry : model_arr.entrySet()) {
			Query tmpQuery = new TermQuery(new Term("content", BytesRef.deepCopyOf(entry.getKey())));
			modelQuery.add(tmpQuery, BooleanClause.Occur.SHOULD);
		}

		for (Map.Entry<BytesRef, Float> entry : guiding_arr.entrySet()) {
			Query tmpQuery = new TermQuery(new Term("content", BytesRef.deepCopyOf(entry.getKey())));
			guidePriceQuery.add(tmpQuery, BooleanClause.Occur.SHOULD);
		}
		for (Map.Entry<BytesRef, Float> entry : price_arr.entrySet()) {
			Query tmpQuery1 = new TermQuery(new Term("content", BytesRef.deepCopyOf(entry.getKey())));
			priceQuery.add(tmpQuery1, BooleanClause.Occur.SHOULD);
		}

		if (brand_arr.size() > 0) {
			q.add(brandQuery, BooleanClause.Occur.MUST);
		}
		if (model_arr.size() > 0) {
			q.add(modelQuery, BooleanClause.Occur.MUST);
		}
		if (guiding_arr.size() > 0) {
			q.add(guidePriceQuery, BooleanClause.Occur.MUST);
		}
		if (price_arr.size() > 0) {
			q.add(priceQuery, BooleanClause.Occur.MUST);
		}
		// ========================================
		// || ||
		// || MUST ||
		// || ||
		// ========================================

	}

	/**
	 * query:奥迪a4宝马x6 => (brand:奥迪 OR brand:宝马) AND (model:a4 OR model:x6)
	 *
	 */
	@Override
	@SuppressWarnings("resource")
	public Query parse() {
		reset();
		if (this.qstr.equals("*:*")) {
			String defaultField = getReq().getSchema().getDefaultSearchFieldName();
			Query q = new WildcardQuery(new Term(defaultField, "*"));
			return q;
		}
		String df = req.getSchema().getDefaultSearchFieldName();
		Analyzer analyzer = req.getSchema().getQueryAnalyzer();
		TokenStream source;
		try {
			source = analyzer.tokenStream(df, new StringReader(this.qstr));
			source.reset();
		} catch (IOException e) {
			return null;
		}
		CachingTokenFilter buffer = new CachingTokenFilter(source);
		TermToBytesRefAttribute termAtt = null;
		PositionIncrementAttribute posIncrAtt = null;
		TypeAttribute type = null;
		int numTokens = 0;

		buffer.reset();

		if (buffer.hasAttribute(TermToBytesRefAttribute.class)) {
			termAtt = buffer.getAttribute(TermToBytesRefAttribute.class);
		}
		if (buffer.hasAttribute(PositionIncrementAttribute.class)) {
			posIncrAtt = buffer.getAttribute(PositionIncrementAttribute.class);
		}
		if (buffer.hasAttribute(TypeAttribute.class)) {
			type = buffer.getAttribute(TypeAttribute.class);
		}

		int positionCount = 0;

		boolean hasMoreTokens = false;
		if (termAtt != null) {
			try {
				hasMoreTokens = buffer.incrementToken();
				while (hasMoreTokens) {
					numTokens++;
					int positionIncrement = (posIncrAtt != null) ? posIncrAtt.getPositionIncrement() : 1;
					if (positionIncrement != 0) {
						positionCount += positionIncrement;
					}
					hasMoreTokens = buffer.incrementToken();
				}
			} catch (IOException e) {
				// ignore
			}
		}
		try {
			// rewind the buffer stream
			buffer.reset();
			// close original stream - all tokens buffered
			source.close();
		} catch (IOException e) {
			return null;
		}

		BytesRef bytes = termAtt == null ? null : termAtt.getBytesRef();

		if (numTokens == 0)
			return null;
		else {
			q = new BooleanQuery(positionCount == 1);
			for (int i = 0; i < numTokens; i++) {
				try {
					boolean hasNext = buffer.incrementToken();
					assert hasNext == true;
					termAtt.fillBytesRef();
				} catch (IOException e) {
					// safe to ignore, because we know the number of
					// tokens
				}
				termBelong(type, bytes);
			}
			generateQuery();
			return q;
		}
	}
}
