package com.niuniu.queryparser;

import java.io.IOException;

import org.apache.lucene.index.AtomicReaderContext;
import org.apache.lucene.queries.CustomScoreProvider;
import org.apache.lucene.queries.CustomScoreQuery;
import org.apache.lucene.search.Query;

public class NiuniuQuery extends CustomScoreQuery {

	public NiuniuQuery(Query subQuery) {
		super(subQuery);
	}

	@Override
	protected CustomScoreProvider getCustomScoreProvider(AtomicReaderContext context) throws IOException {
		return new NiuniuScoreProvider(context);
	}
}
