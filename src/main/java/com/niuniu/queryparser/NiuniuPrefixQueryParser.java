package com.niuniu.queryparser;

import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanClause.Occur;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.WildcardQuery;
import org.apache.solr.common.params.SolrParams;
import org.apache.solr.request.SolrQueryRequest;
import org.apache.solr.search.QParser;
import org.apache.solr.search.SyntaxError;

import com.niuniu.utils.NiuniuUtils;

public class NiuniuPrefixQueryParser extends QParser {

	public NiuniuPrefixQueryParser(String qstr, SolrParams localParams, SolrParams params, SolrQueryRequest req) {
		super(qstr, localParams, params, req);
	}

	@Override
	public Query parse() throws SyntaxError {
		String qString = this.qstr.trim().toLowerCase();
		qString = qString.replaceAll(" +", " ");
		String[] arrs = qString.split(" ");
		BooleanQuery query = new BooleanQuery();
		for (int i = 0; i < arrs.length - 1; i++) {
			BooleanQuery q = new BooleanQuery();
			q.add(new TermQuery(new Term("query", arrs[i])), BooleanClause.Occur.SHOULD);
			q.add(new TermQuery(new Term("query_str", arrs[i])), BooleanClause.Occur.SHOULD);
			q.add(new TermQuery(new Term("query_pinyin", NiuniuUtils.toPinyin(arrs[i], ""))),
					BooleanClause.Occur.SHOULD);
			q.add(new TermQuery(new Term("query_pinyin_abbrev", arrs[i])), BooleanClause.Occur.SHOULD);
			query.add(q, Occur.MUST);
		}
		Query q1 = new WildcardQuery(new Term("query", arrs[arrs.length - 1] + "*"));
		Query q2 = new WildcardQuery(new Term("query_str", arrs[arrs.length - 1] + "*"));
		Query q3 = new WildcardQuery(new Term("query_pinyin", NiuniuUtils.toPinyin(arrs[arrs.length - 1], "") + "*"));
		Query q4 = new WildcardQuery(new Term("query_pinyin_abbrev", arrs[arrs.length - 1] + "*"));
		BooleanQuery qq = new BooleanQuery();
		qq.add(q1, Occur.SHOULD);
		qq.add(q2, Occur.SHOULD);
		qq.add(q3, Occur.SHOULD);
		qq.add(q4, Occur.SHOULD);
		query.add(qq, Occur.MUST);
		return query;
	}

}
