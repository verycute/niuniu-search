package com.niuniu.queryparser;

import java.io.IOException;
import java.io.StringReader;

import org.apache.commons.lang.math.NumberUtils;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.CachingTokenFilter;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.OffsetAttribute;
import org.apache.lucene.analysis.tokenattributes.PositionIncrementAttribute;
import org.apache.lucene.analysis.tokenattributes.TermToBytesRefAttribute;
import org.apache.lucene.analysis.tokenattributes.TypeAttribute;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.WildcardQuery;
import org.apache.lucene.search.BooleanClause.Occur;
import org.apache.lucene.util.BytesRef;
import org.apache.solr.common.params.SolrParams;
import org.apache.solr.common.util.NamedList;
import org.apache.solr.request.SolrQueryRequest;
import org.apache.solr.search.QParser;
import org.apache.solr.search.QParserPlugin;
import org.apache.solr.search.SyntaxError;

public class NiuniuBasecarFixQueryParserPlugin extends QParserPlugin {

	@Override
	@SuppressWarnings("rawtypes")
	public void init(NamedList args) {
	}

	@Override
	public QParser createParser(String qstr, SolrParams localParams, SolrParams params, SolrQueryRequest req) {
		return new NiuniuBasecarFixQueryParser(qstr, localParams, params, req);
	}

	public class NiuniuBasecarFixQueryParser extends QParser {

		public NiuniuBasecarFixQueryParser(String qstr, SolrParams localParams, SolrParams params,
				SolrQueryRequest req) {
			super(qstr, localParams, params, req);
		}

		private boolean appendQuery(BytesRef bytes, boolean isChineseToken, boolean isLastIntegerToken,
				BooleanQuery numQuery, BooleanQuery charQuery, BooleanQuery lastQ) {
			String str = bytes.utf8ToString();
			if (NumberUtils.toFloat(str, -1) == 0)
				return false;
			if (!isChineseToken) {
				// 检索style，价格和年份
				// 如果是最后一个token，则构造wildcardquery
				if (!isLastIntegerToken) {
					if (NumberUtils.toFloat(str, -1) != -1) {
						// 数字
						Query tmpQuery = new TermQuery(new Term("base_car_style", BytesRef.deepCopyOf(bytes)));
						numQuery.add(tmpQuery, Occur.SHOULD);
						tmpQuery = new TermQuery(new Term("base_car_NO", BytesRef.deepCopyOf(bytes)));
						numQuery.add(tmpQuery, Occur.SHOULD);
						tmpQuery = new TermQuery(new Term("year", BytesRef.deepCopyOf(bytes)));
						numQuery.add(tmpQuery, Occur.SHOULD);
					} else {
						Query tmpQuery = new TermQuery(new Term("base_car_style", BytesRef.deepCopyOf(bytes)));
						charQuery.add(tmpQuery, BooleanClause.Occur.SHOULD);
					}
				} else {
					if (NumberUtils.toFloat(str, -1) != -1) {
						Query q1 = new WildcardQuery(new Term("base_car_style", str + "*"));
						Query q2 = new WildcardQuery(new Term("base_car_NO", str + "*"));
						Query q3 = new WildcardQuery(new Term("year", str + "*"));
						lastQ.add(q1, Occur.SHOULD);
						lastQ.add(q2, Occur.SHOULD);
						lastQ.add(q3, Occur.SHOULD);
					} else {
						Query q1 = new WildcardQuery(new Term("base_car_style", str + "*"));
						lastQ.add(q1, Occur.SHOULD);
					}
				}
			} else {
				// 只检索style
				Query tmpQuery = new TermQuery(new Term("base_car_style", BytesRef.deepCopyOf(bytes)));
				charQuery.add(tmpQuery, BooleanClause.Occur.SHOULD);
			}
			return true;
		}

		@Override
		public Query parse() throws SyntaxError {
			if (this.qstr.equals("*:*") || this.qstr.equals("*")) {
				String defaultField = getReq().getSchema().getDefaultSearchFieldName();
				Query q = new WildcardQuery(new Term(defaultField, "*"));
				return q;
			}
			BooleanQuery q = null;
			String df = req.getSchema().getDefaultSearchFieldName();
			Analyzer analyzer = req.getSchema().getQueryAnalyzer();
			TokenStream source;
			try {
				source = analyzer.tokenStream(df, new StringReader(this.qstr));
				source.reset();
			} catch (IOException e) {
				return null;
			}
			CachingTokenFilter buffer = new CachingTokenFilter(source);
			TermToBytesRefAttribute termAtt = null;
			PositionIncrementAttribute posIncrAtt = null;
			OffsetAttribute offset = null;
			TypeAttribute type = null;
			int numTokens = 0;

			buffer.reset();

			if (buffer.hasAttribute(TermToBytesRefAttribute.class)) {
				termAtt = buffer.getAttribute(TermToBytesRefAttribute.class);
			}
			if (buffer.hasAttribute(PositionIncrementAttribute.class)) {
				posIncrAtt = buffer.getAttribute(PositionIncrementAttribute.class);
			}

			if (buffer.hasAttribute(OffsetAttribute.class)) {
				offset = buffer.getAttribute(OffsetAttribute.class);
			}

			if (buffer.hasAttribute(TypeAttribute.class)) {
				type = buffer.getAttribute(TypeAttribute.class);
			}

			int positionCount = 0;

			boolean hasMoreTokens = false;
			int last_token_index = -1;
			if (termAtt != null) {
				try {
					hasMoreTokens = buffer.incrementToken();
					while (hasMoreTokens) {
						numTokens++;
						if (offset.endOffset() > last_token_index)
							last_token_index = offset.endOffset();
						int positionIncrement = (posIncrAtt != null) ? posIncrAtt.getPositionIncrement() : 1;
						if (positionIncrement != 0) {
							positionCount += positionIncrement;
						}
						hasMoreTokens = buffer.incrementToken();
					}
				} catch (IOException e) {
					// ignore
				}
			}
			try {
				// rewind the buffer stream
				buffer.reset();
				// close original stream - all tokens buffered
				source.close();
			} catch (IOException e) {
				return null;
			}

			BooleanQuery num_query = new BooleanQuery();
			BooleanQuery char_query = new BooleanQuery();
			BooleanQuery last_query = new BooleanQuery();
			BytesRef bytes = termAtt == null ? null : termAtt.getBytesRef();
			if (numTokens == 0) {
				return null;
			} else {
				q = new BooleanQuery(positionCount == 1);
				String str;
				for (int i = 0; i < numTokens; i++) {
					try {
						boolean hasNext = buffer.incrementToken();
						assert hasNext == true;
						termAtt.fillBytesRef();
					} catch (IOException e) {
						// safe to ignore, because we know the number of
						// tokens
					}
					str = bytes.utf8ToString();
					System.out.println(str);
					System.out.println(offset.startOffset() + "  " + offset.endOffset());
					System.out.println(type.type());

					appendQuery(bytes, "chineses".equals(type.type()), offset.endOffset() == last_token_index,
							num_query, char_query, last_query);
				}
				if (!char_query.clauses().isEmpty())
					q.add(char_query, Occur.MUST);
				if (!num_query.clauses().isEmpty())
					q.add(num_query, Occur.MUST);
				if (!last_query.clauses().isEmpty())
					q.add(last_query, Occur.MUST);
				// boolean niuFacet = params.getBool("niufacet", false);
				// return niuFacet?q:new NiuniuQuery(q);
				return q;
			}
		}
	}
}
