package com.niuniu.queryparser;

import java.io.IOException;

import org.apache.commons.lang.math.NumberUtils;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.AtomicReaderContext;
import org.apache.lucene.queries.CustomScoreProvider;

public class NiuniuScoreProvider extends CustomScoreProvider {
	public NiuniuScoreProvider(AtomicReaderContext context) {
		super(context);
	}

	@Override
	public float customScore(int doc, float subQueryScore, float valSrcScore) throws IOException {
		return customScore(doc, subQueryScore, new float[] { valSrcScore });
	}

	@Override
	public float customScore(int doc, float subQueryScore, float[] valSrcScores) throws IOException {
		// Method is called for every
		// matching document of the subQuery
		// 0.2 previously
		Document document = context.reader().document(doc);
		float search_score = NumberUtils.toFloat(document.get("search_score_1")); // Float.parseFloat(document.get("search_score_1"));
		return subQueryScore + search_score;
	}

	/*
	 * public Explanation CustomExplain(int doc, Explanation subQueryExpl,
	 * Explanation[] valSrcExpls) { if (valSrcExpls.Length == 1) { return
	 * CustomExplain(doc, subQueryExpl, valSrcExpls[0]); } if
	 * (valSrcExpls.Length == 0) { return subQueryExpl; } float valSrcScore = 1;
	 * foreach (Explanation valSrcExpl in valSrcExpls) { valSrcScore *=
	 * valSrcExpl.Value; } Explanation exp = new Explanation(valSrcScore *
	 * subQueryExpl.Value, "custom score: product of:");
	 * exp.AddDetail(subQueryExpl); foreach (Explanation valSrcExpl in
	 * valSrcExpls) { exp.AddDetail(valSrcExpl); } return exp; }
	 * 
	 * /// <summary> /// Explain the custom score. /// Whenever overriding
	 * <seealso cref="#customScore(int, float, float)"/>, /// this method should
	 * also be overridden to provide the correct explanation /// for the part of
	 * the custom scoring. /// </summary> /// <param name="doc"> doc being
	 * explained. </param> /// <param name="subQueryExpl"> explanation for the
	 * sub-query part. </param> /// <param name="valSrcExpl"> explanation for
	 * the value source part. </param> /// <returns> an explanation for the
	 * custom score </returns> public Explanation CustomExplain(int doc,
	 * Explanation subQueryExpl, Explanation valSrcExpl) { float valSrcScore =
	 * 1; if (valSrcExpl != null) { valSrcScore *= valSrcExpl.Value; }
	 * Explanation exp = new Explanation(valSrcScore * subQueryExpl.Value,
	 * "custom score: product of:"); exp.AddDetail(subQueryExpl);
	 * exp.AddDetail(valSrcExpl); return exp; }
	 */
}
