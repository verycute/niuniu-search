package com.niuniu.update.plugin;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.math.NumberUtils;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.Token;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.tokenattributes.FlagsAttribute;
import org.apache.lucene.analysis.tokenattributes.OffsetAttribute;
import org.apache.lucene.analysis.tokenattributes.PayloadAttribute;
import org.apache.lucene.analysis.tokenattributes.PositionIncrementAttribute;
import org.apache.lucene.analysis.tokenattributes.TypeAttribute;
import org.apache.lucene.analysis.util.ResourceLoader;
import org.apache.solr.common.SolrInputDocument;
import org.apache.solr.common.util.NamedList;
import org.apache.solr.core.SolrCore;
import org.apache.solr.request.SolrQueryRequest;
import org.apache.solr.response.SolrQueryResponse;
import org.apache.solr.update.AddUpdateCommand;
import org.apache.solr.update.processor.UpdateRequestProcessor;
import org.apache.solr.update.processor.UpdateRequestProcessorFactory;
import org.apache.solr.util.plugin.SolrCoreAware;

/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

public class BusinessCircleProcessorFactory extends UpdateRequestProcessorFactory implements SolrCoreAware {

	Map<String, Integer> tagWeightForBrand = null;
	String defaultField = null;
	Set<String> brand_set = null;
	Map<String, String> model_map = null;
	Map<String, String> price_map = null;
	String brand_file;
	String model_file;
	String price_file;
	int maxTagCount = 0;

	@SuppressWarnings("rawtypes")
	public void init(NamedList args) {
		defaultField = args.get("default_field").toString();
		String[] tags = args.get("tags").toString().split(",");
		String[] weights = args.get("weights").toString().split(",");
		maxTagCount = NumberUtils.toInt(args.get("max_tag").toString().trim());
		if (tags.length == weights.length) {
			tagWeightForBrand = new HashMap<String, Integer>();
			for (int i = 0; i < tags.length; i++) {
				tagWeightForBrand.put(tags[i].trim(), NumberUtils.toInt(weights[i].trim()));
			}
		}
		brand_set = new HashSet<String>();
		model_map = new HashMap<String, String>();
		price_map = new HashMap<String, String>();
		this.brand_file = args.get("brand").toString();
		this.model_file = args.get("model").toString();
		this.price_file = args.get("price").toString();
	};

	private void readBrandFile(ResourceLoader loader, String filename) {
		if (filename == null) {
			return;
		}
		InputStream is = null;
		try {
			is = loader.openResource(filename);
			if (is == null) {
				throw new RuntimeException(filename + " Dictionary not found!!!");
			}
			BufferedReader br = new BufferedReader(new InputStreamReader(is, "UTF-8"), 512);
			String line = null;
			brand_set.clear();
			do {
				line = br.readLine();
				if (line == null)
					break;
				line = line.trim().toLowerCase();
				brand_set.add(line);
			} while (line != null);
		} catch (IOException ioe) {
			System.err.println("brand file loading exception.");
			ioe.printStackTrace();
		}
	}

	private void readMappingFromFile(ResourceLoader loader, String filename, Map<String, String> map) {
		if (filename == null) {
			return;
		}
		InputStream is = null;
		try {
			is = loader.openResource(filename);
			if (is == null) {
				throw new RuntimeException(filename + " Dictionary not found!!!");
			}
			BufferedReader br = new BufferedReader(new InputStreamReader(is, "UTF-8"), 512);
			String line = null;
			map.clear();
			do {
				line = br.readLine();
				if (line == null)
					break;
				line = line.trim().toLowerCase();
				String[] arrs = line.split("\t");
				map.put(arrs[0], arrs[1]);
			} while (line != null);
		} catch (IOException ioe) {
			System.err.println(filename + " file loading exception.");
			ioe.printStackTrace();
		}
	}

	@Override
	public UpdateRequestProcessor getInstance(SolrQueryRequest req, SolrQueryResponse rsp,
			UpdateRequestProcessor next) {
		Analyzer analyzer = req.getSchema().getQueryAnalyzer();
		return new BussinessCircleProcessor(next, analyzer, req.getParams().get("segment_field", defaultField),
				tagWeightForBrand, brand_set, model_map, price_map);
	}

	class BussinessCircleProcessor extends UpdateRequestProcessor {

		Analyzer analyzer = null;
		String segment_field = null;
		Map<String, Integer> tagWeightForBrand = null;
		Set<String> brandSet = null;
		Map<String, String> modelMap = null;
		Map<String, String> priceMap = null;

		public BussinessCircleProcessor(UpdateRequestProcessor next, Analyzer analyzer, String segment_field,
				Map<String, Integer> tagWeightForBrand, Set<String> brand_set, Map<String, String> model_map, Map<String, String> price_map) {
			super(next);
			this.analyzer = analyzer;
			this.segment_field = segment_field;
			this.tagWeightForBrand = tagWeightForBrand;
			this.brandSet = brand_set;
			this.modelMap = model_map;
			this.priceMap = price_map;
		}

		private Collection<Token> getTokens(String q, Analyzer analyzer, String sfield) throws IOException {
			Collection<Token> result = new ArrayList<Token>();
			assert analyzer != null;
			TokenStream ts = analyzer.tokenStream(sfield, new StringReader(q));
			ts.reset();
			// TODO: support custom attributes
			CharTermAttribute termAtt = ts.addAttribute(CharTermAttribute.class);
			OffsetAttribute offsetAtt = ts.addAttribute(OffsetAttribute.class);
			TypeAttribute typeAtt = ts.addAttribute(TypeAttribute.class);
			FlagsAttribute flagsAtt = ts.addAttribute(FlagsAttribute.class);
			PayloadAttribute payloadAtt = ts.addAttribute(PayloadAttribute.class);
			PositionIncrementAttribute posIncAtt = ts.addAttribute(PositionIncrementAttribute.class);

			while (ts.incrementToken()) {
				Token token = new Token();
				token.copyBuffer(termAtt.buffer(), 0, termAtt.length());
				token.setOffset(offsetAtt.startOffset(), offsetAtt.endOffset());
				token.setType(typeAtt.type());
				token.setFlags(flagsAtt.getFlags());
				token.setPayload(payloadAtt.getPayload());
				token.setPositionIncrement(posIncAtt.getPositionIncrement());
				result.add(token);
			}
			ts.end();
			ts.close();
			return result;
		}

		public void addToBrandIndicator(String str, int weight, Map<String, Integer> brand_indicator) {
			if (brand_indicator.containsKey(str)) {
				brand_indicator.put(str, brand_indicator.get(str) + weight);
			} else {
				brand_indicator.put(str, weight);
			}
		}

		private void getLatentBrand(String term, String type, Map<String, Integer> brand_indicator) {
			if (type.equals("BRAND")) {
				if (brandSet.contains(term)) {
					addToBrandIndicator(term, this.tagWeightForBrand.get("brand"), brand_indicator);
				}
			}
			if (type.contains("MODEL")) {
				if (modelMap.containsKey(term)) {
					String[] brands_list = modelMap.get(term).split(",");
					for (String s : brands_list) {
						addToBrandIndicator(s, this.tagWeightForBrand.get("model"), brand_indicator);
					}
				}
			}

		}

		private void getLatentBrandFromPrice(String term, String type, Map<String, Integer> brand_indicator) {
			if (type.contains("PRICE")) {
				if (term.length() < 3)
					return;
				if (priceMap.containsKey(term)) {
					String[] brands_list = priceMap.get(term).split(",");
					for (String s : brands_list) {
						if (brand_indicator.containsKey(s))
							addToBrandIndicator(s, this.tagWeightForBrand.get("price"), brand_indicator);
					}
				}
			}
		}

		private int selectKBest(List<Map.Entry<String, Integer>> infoIds) {
			int reserve = infoIds.get(0).getValue();
			int top = 0;
			for (int i = 1; i < infoIds.size() && i < maxTagCount; i++) {
				int tmp = infoIds.get(i).getValue();
				if ((tmp + tmp / 2) >= reserve || tmp >= 2 * tagWeightForBrand.get("brand")) {
					reserve = tmp;
					top++;
				} else {
					break;
				}
			}
			if (top == infoIds.size())
				top--;
			return top;
		}

		private String getStrFieldFromDocument(SolrInputDocument doc, String field) {
			Object v = doc.getFieldValue(field);
			return v == null ? null : v.toString();
		}

		public void processAdd(AddUpdateCommand cmd) throws IOException {
			SolrInputDocument doc = cmd.getSolrInputDocument();
			Map<String, Integer> brand_indicator = new HashMap<String, Integer>();

			String id = getStrFieldFromDocument(doc, "id");
			if (!id.contains("topic") && !id.contains("discussion")) {
				doc.remove("id");
				doc.addField("id", "discussion" + id);
			}
			String uid = getStrFieldFromDocument(doc, "user_id");
			String con = getStrFieldFromDocument(doc, "content");
			String hashkey = uid + con;
			doc.addField("hashkey", hashkey.hashCode());
			/*
			 * 预测生意圈所属的品牌，最多3个品牌哟!!!
			 */
			Object v = doc.getFieldValue("content");
			if (v != null) {

				String content = v.toString().replaceAll("-", "");
				Collection<Token> tokens = null;
				// tokens = getTokens(content, analyzer, segment_field);
				tokens = getTokens(content, analyzer, "content");
				for (Token token : tokens) {
					String term = token.toString();
					String type = token.type();
					if (type.equals("BRAND") || type.contains("MODEL")) {
						getLatentBrand(term, type, brand_indicator);
					}
				}
				for (Token token : tokens) {
					int start = token.startOffset();
					String term = token.toString();
					String type = token.type();
					if (type.contains("PRICE")) {
						// 价格前是 “下” 或者 "⬇️" 则跳过
						if ((start >= 1 && content.charAt(start - 1) == '下')
								|| (start >= 2 && content.substring(start - 2, start).equals("⬇️"))) {
							continue;
						}
						getLatentBrandFromPrice(term, type, brand_indicator);
					}
				}
			}
			if (brand_indicator.isEmpty()) {
				doc.addField("general", 1);
			} else {
				List<Map.Entry<String, Integer>> infoIds = new ArrayList<Map.Entry<String, Integer>>(
						brand_indicator.entrySet());
				if (!brand_indicator.isEmpty()) {
					Collections.sort(infoIds, new Comparator<Map.Entry<String, Integer>>() {
						public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
							return (o2.getValue().compareTo(o1.getValue()));
						}
					});
				}
				int kBest = selectKBest(infoIds);
				for (int i = 0; i <= kBest; i++) {
					doc.addField("brand_tags", infoIds.get(i).getKey());
				}
			}
			String deleted_at = getStrFieldFromDocument(doc, "deleted_at");
			if (deleted_at == null || deleted_at.isEmpty()) {
				doc.addField("status", 1);
			} else {
				doc.addField("status", -1);
			}
			doc.removeField("deleted_at");
			super.processAdd(cmd);
		}
	}

	@Override
	public void inform(SolrCore core) {
		readBrandFile(core.getResourceLoader(), brand_file);
		readMappingFromFile(core.getResourceLoader(), model_file, model_map);
		readMappingFromFile(core.getResourceLoader(), price_file, price_map);
	}
}
