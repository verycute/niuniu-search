package com.niuniu.update.plugin;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import org.apache.commons.lang.math.NumberUtils;
import org.apache.solr.common.SolrInputDocument;
import org.apache.solr.request.SolrQueryRequest;
import org.apache.solr.response.SolrQueryResponse;
import org.apache.solr.update.AddUpdateCommand;
import org.apache.solr.update.processor.UpdateRequestProcessor;
import org.apache.solr.update.processor.UpdateRequestProcessorFactory;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Days;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

public class NiuniuProcessorFactory extends UpdateRequestProcessorFactory {
	@Override
	public UpdateRequestProcessor getInstance(SolrQueryRequest req, SolrQueryResponse rsp,
			UpdateRequestProcessor next) {
		return new NiuniuProcessor(next);
	}
}

class NiuniuProcessor extends UpdateRequestProcessor {
	private final String regEx = "[-`·~!@#$%^&*()+=|{}':;',//[//]<>/?~！@#￥%……&*（）—+|{}【】‘；：”“’。，、？_]";
	private Pattern specialCharPattern = null;

	private final String[] user_level_mapping = { "个人手机认证", "个人身份认证", "认证资源", "认证综展", "4S" };
	private final String[] user_vip_level_mapping = { "regular", "normal", "buy", "sell", "senior" };

	private DateTime cur_time = null;
	private DateTimeFormatter utc_formatter = null;
	
	public NiuniuProcessor(UpdateRequestProcessor next) {
		super(next);
		this.cur_time = new DateTime();
		utc_formatter = ISODateTimeFormat.dateTime().withZoneUTC();
		specialCharPattern = Pattern.compile(regEx);
	}

	private DateTime toUTCTime(String time){
		DateTime tTime = new DateTime(time,DateTimeZone.UTC);
		return tTime;
	}

	private String escape(String str) {
		if (str.equals("不限") || str.equals("全国") || str.equals("其他"))
			return "";
		return str;
	}

	/*
	 * 把品牌，车型等字段中的特殊字符去掉，例如阿尔法·罗密欧中的·
	 */
	private String escapeFromSpecialCh(String str, boolean delete) {
		if (!delete) {
			StringBuffer sb = new StringBuffer();
			Matcher m = specialCharPattern.matcher(str);
			while (m.find()) {
				m.appendReplacement(sb, "\\" + m.group());
			}
			m.appendTail(sb);
			return sb.toString();
		} else {
			Matcher m = specialCharPattern.matcher(str);
			return m.replaceAll("").trim();
		}
	}

	/*
	 * 奔驰base_car_style特殊业务逻辑 例如 E 260 L => E260L
	 */
	private String benzBaseCarStyle(String str) {
		String regEx = "[a-zA-Z]{1,2}\\s+[1-9][0-9]{0,2}\\s[a-zA-Z]{1}(\\s|$)";
		String regEx2 = "[a-zA-Z]{1,2}\\s+[1-9][0-9]{0,2}(\\s|$)";
		Pattern p = Pattern.compile(regEx);
		Matcher m = p.matcher(str);
		if (!m.find()) {
			Pattern p2 = Pattern.compile(regEx2);
			Matcher m2 = p2.matcher(str);
			if (m2.find())
				str = str.replace(m2.group(0), m2.group(0).replaceAll(" ", "") + " ");
			return str.trim();
		}
		str = str.replace(m.group(0), m.group(0).replaceAll(" ", "") + " ");
		return str.trim();
	}

	private int calcDateDiff(DateTime updated_at){
		int days = Days.daysBetween(updated_at, cur_time).getDays();
		return days;
	}

	/*
	 * for niuniu search
	 */
	private long createdAtMsFormat(DateTime created_at){
		if(created_at!=null)
			return created_at.getMillis();
		return 0;
	}

	private float round(float value, int scale) {
		BigDecimal bdBigDecimal = new BigDecimal(value);
		bdBigDecimal = bdBigDecimal.setScale(scale, BigDecimal.ROUND_HALF_UP);
		return bdBigDecimal.floatValue();
	}

	/*
	 * 构造show_price
	 */
	private String niuniuShowPrice(int discount_way, String expect_price) {
		String constant = "电议";
		if (discount_way == 5 || expect_price == null || expect_price.isEmpty() || expect_price.equals("0")) {
			return constant;
		}
		return expect_price + "万";
	}

	/*
	 * 生成facet筛选下的style信息
	 */
	private String generateStyleFullName(int status, String style, String base_price) {
		if("0.00".equals(base_price))
			return Integer.toString(status) + " % " + style + " % ";
		return Integer.toString(status) + " % " + style + " % " + base_price + "万";
	}

	private String generateStyleYearName(int status, String year, String style, String short_year) {
		year = year.length() == 4 ? year : "0";
		if (status == 1 && !year.equals("0"))
			return Integer.toString(status) + " % " + short_year + " " + style + " % " + year;
		return Integer.toString(status) + " % " + style + " % " + year;
	}

	private String reGenerateStyle(String style, String short_year) {
		String regEx = "[0-9]{2}款";
		Pattern p = Pattern.compile(regEx);
		Matcher m = p.matcher(style);
		if (!m.matches())
			style = short_year + " " + style;
		return style;
	}

	public String escapeZero(String str) throws PatternSyntaxException {
		String regEx = "0*$";
		Pattern p = Pattern.compile(regEx);
		Matcher m = p.matcher(str);
		String res = m.replaceAll("").trim();
		if (res.isEmpty())
			return str;
		return res;
	}

	private String generateBrandFirmStandard(String brand_pinyin, int standard_id, String firm_name) {
		if (firm_name == null || firm_name.isEmpty()) {
			return brand_pinyin + ",8,其他";
		}
		String ret;
		switch (standard_id) {
		case 1:
			ret = "1," + firm_name;
			break;
		case 2:
			ret = "2," + firm_name;
			break;
		default:
			ret = "3," + firm_name;
		}
		return brand_pinyin + "," + ret;
	}

	private String concatString(String... strs) {
		String title = "";
		for (String s : strs) {
			if (s != null)
				title += s + " ";
		}
		return title.trim();
	}

	private String generateTitle(int post_type, String brand_name, String standard_name, String car_model_name,
			String base_car_style, String base_car_NO, int base_car_status, String short_year) {
		if (post_type == 1 || post_type == 3) {
			if(standard_name.equals("国产") || standard_name.equals("中规"))
				return concatString("寻", standard_name, car_model_name, base_car_NO);
			else{
				return concatString("寻", standard_name, car_model_name, short_year, base_car_style);
			}
		}
		String base_car_style_escape = base_car_style.replace("[0-9]{2}款", "");
		if (base_car_style.equals(base_car_style_escape) && base_car_status == 1) {
			return concatString(car_model_name, short_year, base_car_style);
		} else if (base_car_status == 0) {
			return concatString(brand_name, car_model_name, base_car_style);
		} else {
			return concatString(car_model_name, base_car_style);
		}

	}

	private String trimZeroForFloat(String str) {
		if (str == null)
			return null;
		if (str.indexOf(".") > 0) {
			str = str.replaceAll("0+?$", "");
			str = str.replaceAll("[.]$", "");
		}
		return str;
	}

	private String generateInfoPrice(String configuration_price, String guiding_price, String expect_price,
			int discount_way, String discount_content) {
		float f_configuration_price = NumberUtils.toFloat(configuration_price, 0.0f);
		float f_guiding_price = NumberUtils.toFloat(guiding_price, 0.0f);
		float f_expect_price = NumberUtils.toFloat(expect_price, 0.0f);
		float f_discount_content = NumberUtils.toFloat(discount_content, 0.0f);
		float calculate_price = f_configuration_price == 0.0f ? f_guiding_price : f_configuration_price;
		String price1 = null, price2 = null;
		String ans = "";
		if (calculate_price == 0.0f) {
			return ans;
		} else {
			if (f_configuration_price == 0) {
				price1 = "指导价: " + guiding_price + "万";
			} else {
				price1 = "配置价: " + configuration_price + "万";
			}

			switch (discount_way) {
			case 1:
				price2 = f_discount_content == 0 ? null : "下" + Float.toString(round(f_discount_content, 2)) + "点";
				break;
			case 2:
				price2 = f_discount_content == 0 ? null : "下" + Float.toString(round(f_discount_content, 2)) + "万";
				break;
			case 3:
				price2 = f_discount_content == 0 ? null : "加" + Float.toString(round(f_discount_content, 2)) + "万";
				break;
			case 4:
				if (calculate_price > f_expect_price) {
					price2 = "下" + Float.toString(round(calculate_price - f_expect_price, 2)) + "万";
				}
				if (calculate_price < f_expect_price) {
					price2 = "加" + Float.toString(round(f_expect_price - calculate_price, 2)) + "万";
				}
				break;
			case 5:
				break;
			default:
				break;
			}
			ans = price2 == null ? price1 : price1 + "/" + price2;
		}
		return ans;
	}

	private String generateInfoPriceAmount(String configuration_price, String guiding_price, String expect_price,
			int discount_way, String discount_content) {
		float f_configuration_price = NumberUtils.toFloat(configuration_price, 0.0f);
		float f_guiding_price = NumberUtils.toFloat(guiding_price, 0.0f);
		float f_expect_price = NumberUtils.toFloat(expect_price, 0.0f);
		float calculate_price = f_configuration_price == 0.0f ? f_guiding_price : f_configuration_price;
		String price1 = null, price2 = null;
		String ans = "";
		if (calculate_price == 0.0f) {
			return "";
		} else {
			if (f_configuration_price == 0) {
				price1 = "指导价: " + guiding_price + "万";
			} else {
				price1 = "配置价: " + configuration_price + "万";
			}

			if (discount_way != 5) {
				if (calculate_price > f_expect_price) {
					price2 = "下" + Float.toString(round(calculate_price - f_expect_price, 2)) + "万";
				}
				if (calculate_price < f_expect_price) {
					price2 = "加" + Float.toString(round(f_expect_price - calculate_price, 2)) + "万";
				}
			}

			ans = price2 == null ? price1 : price1 + "/" + price2;
		}
		return ans;
	}

	private String generateInfoPricePercentage(String configuration_price, String guiding_price, String expect_price,
			int discount_way, String discount_content) {
		float f_configuration_price = NumberUtils.toFloat(configuration_price, 0.0f);
		float f_guiding_price = NumberUtils.toFloat(guiding_price, 0.0f);
		float f_expect_price = NumberUtils.toFloat(expect_price, 0.0f);
		float calculate_price = f_configuration_price == 0.0f ? f_guiding_price : f_configuration_price;
		String price1 = null, price2 = null;
		String ans = "";
		if (calculate_price == 0.0f) {
			return "";
		} else {
			if (f_configuration_price == 0) {
				price1 = "指导价: " + guiding_price + "万";
			} else {
				price1 = "配置价: " + configuration_price + "万";
			}

			if (discount_way != 5) {
				if (calculate_price > f_expect_price) {
					price2 = "下" + Float.toString(round(100 * (calculate_price - f_expect_price) / calculate_price, 2))
							+ "点";
				}
				if (calculate_price < f_expect_price) {
					price2 = "加" + Float.toString(round(100 * (f_expect_price - calculate_price) / calculate_price, 2))
							+ "点";
				}
			}

			ans = price2 == null ? price1 : price1 + "/" + price2;
		}
		return ans;
	}

	String getStrFieldFromDocument(SolrInputDocument doc, String field) {
		Object v = doc.getFieldValue(field);
		return v == null ? null : v.toString();
	}

	boolean isValid(String s) {
		return s != null && !s.isEmpty();
	}

	String generateExtendInfo(String outer_color, String inner_color, String ticket_pack_time, String car_in_area,
			String take_car_area) {
		String ans = "";
		String color = "";

		if (!outer_color.isEmpty() && !inner_color.isEmpty()) {
			color = outer_color + "/" + inner_color;
		} else if (outer_color.isEmpty()) {
			if (!inner_color.isEmpty()) {
				color = inner_color;
			}
		} else {
			color = outer_color;
		}
		ans = color;
		if (isValid(ticket_pack_time)) {
			ans += "|" + ticket_pack_time;
		}
		if (isValid(take_car_area)) {
			ans += "|销" + take_car_area;
		}
		if (isValid(car_in_area)) {
			ans += "|车在" + car_in_area;
		}
		return ans;
	}

	private String appendEngineInfo(String base_car_style, String engine_capacity){
		if(engine_capacity==null || engine_capacity.isEmpty())
			return base_car_style;
		String l1 = base_car_style.toLowerCase();
		String l2 = engine_capacity.toLowerCase();
		if(!l1.contains(l2)){
			base_car_style = base_car_style + " " + engine_capacity;
		}
		return base_car_style.trim();
	}
	
	private ArrayList<Integer> generatePriceNOFromBase(String base_price){
		if(base_price==null || base_price.isEmpty())
			return null;
		float f = NumberUtils.toFloat(base_price);
		if(f==0)
			return null;
		int n1 = (int)round(f*100,1);
		int n2 = (int)(round(f*10000,1));
		int n3=n1;
		while(n3%10==0)
			n3 /= 10;
		ArrayList<Integer> ans = new ArrayList<Integer>();
		ans.add(n1);
		ans.add(n2);
		if(n1!=n3)
			ans.add(n3);
		return ans;
	}
	
	// generate identical random factor for resource on different server
	private int generateRandomFactorForResource(int post_id){
		long ans = (long)(post_id*17 + 23);
		ans *= cur_time.getDayOfYear();
		int mode = (int)(Math.pow(10, cur_time.getDayOfWeek()%3));
		return (int)((ans/mode)%1000);
	}
	
	public void processAdd(AddUpdateCommand cmd) throws IOException {
		/*
		 * 牛牛资源、寻车业务逻辑!!!
		 */

		SolrInputDocument doc = cmd.getSolrInputDocument();
		Collection<String> search_features = doc.getFieldNames();
		Map<String, String> search_feature_score = new HashMap<String, String>();
		for (String s : search_features) {
			if (s.startsWith("niuniu_search"))
				search_feature_score.put(s, getStrFieldFromDocument(doc, s));
		}
		try {
			int id = NumberUtils.toInt(getStrFieldFromDocument(doc, "id"), 0);
			int post_status = NumberUtils.toInt(getStrFieldFromDocument(doc, "post_status"), 0);
			String base_car_NO = getStrFieldFromDocument(doc, "base_car_NO");
			String inner_color = getStrFieldFromDocument(doc, "inner_color");
			String outer_color = getStrFieldFromDocument(doc, "outer_color");
			String user_id = getStrFieldFromDocument(doc, "user_id");
			String company_owner_id = getStrFieldFromDocument(doc, "company_owner_id");
			String user_name = getStrFieldFromDocument(doc, "user_name");
			String firm_name = getStrFieldFromDocument(doc, "firm_name");
			if (firm_name == null) {
				firm_name = "";
			}
			int post_type = NumberUtils.toInt(getStrFieldFromDocument(doc, "post_type"), 0);
			String guiding_price = getStrFieldFromDocument(doc, "guiding_price");
			int resource_type = NumberUtils.toInt(getStrFieldFromDocument(doc, "resource_type"), 0);
			String standard_name = getStrFieldFromDocument(doc, "standard");
			String base_car_year = getStrFieldFromDocument(doc, "base_car_year");
			String configuration_remark = getStrFieldFromDocument(doc, "configuration_remark");
			int base_car_status = NumberUtils.toInt(getStrFieldFromDocument(doc, "base_car_status"), 0);
			String base_car_style = getStrFieldFromDocument(doc, "base_car_style");
			String brand_name = getStrFieldFromDocument(doc, "brand");
			String car_model_name = getStrFieldFromDocument(doc, "car_model_name");
			String user_city_name = getStrFieldFromDocument(doc, "user_city_name");
			String user_prov_name = getStrFieldFromDocument(doc, "user_prov_name");
			String remark = getStrFieldFromDocument(doc, "remark");
			int user_vip_level = NumberUtils.toInt(getStrFieldFromDocument(doc, "user_vip_level"), 0);
			String created_at = getStrFieldFromDocument(doc, "created_at");
			String user_created_at = getStrFieldFromDocument(doc, "user_created_at");
			String updated_at = getStrFieldFromDocument(doc, "updated_at");
			String expired_at = getStrFieldFromDocument(doc, "expired_at");
			String maintained_at = getStrFieldFromDocument(doc, "maintained_at");
			String expect_price = getStrFieldFromDocument(doc, "expect_price");
			int discount_way = NumberUtils.toInt(getStrFieldFromDocument(doc, "discount_way"), 0);
			String discount_content = getStrFieldFromDocument(doc, "discount_content");

			String base_price = getStrFieldFromDocument(doc, "base_price");
			String aasm_state = getStrFieldFromDocument(doc, "aasm_state");
			String source_prov_name = getStrFieldFromDocument(doc, "source_prov_name");
			String short_name = getStrFieldFromDocument(doc, "short_name");
			String photo_image = getStrFieldFromDocument(doc, "photo_image");
			String non_member_price = getStrFieldFromDocument(doc, "non_member_price");
			String configuration_price = getStrFieldFromDocument(doc, "configuration_price");
			String frame_number = getStrFieldFromDocument(doc, "frame_number");
			int standard_id = NumberUtils.toInt(getStrFieldFromDocument(doc, "standard_id"), 0);
			int car_model_status = NumberUtils.toInt(getStrFieldFromDocument(doc, "car_model_status"), 0);

			String brand_pinyin = getStrFieldFromDocument(doc, "brand_pinyin");
			int brand_id = NumberUtils.toInt(getStrFieldFromDocument(doc, "brand_id"), 0);

			String car_in_area = getStrFieldFromDocument(doc, "car_in_area");
			String take_car_area = getStrFieldFromDocument(doc, "take_car_area");
			String ticket_pack_time = getStrFieldFromDocument(doc, "ticket_pack_time");
			String complete_order_count = getStrFieldFromDocument(doc, "complete_order_count");
			String filter_car_in_region = getStrFieldFromDocument(doc, "filter_car_in_region");
			String filter_car_in_area = getStrFieldFromDocument(doc, "filter_car_in_area");
			if(filter_car_in_area==null)
				filter_car_in_area = "省份未填";

			int is_batch_resource = NumberUtils.toInt(getStrFieldFromDocument(doc, "is_batch_resource"), 0);
			int has_no_price = discount_way == 5 ? 1 : 0;

			/*
			 * search field
			 */
			int user_level = NumberUtils.toInt(getStrFieldFromDocument(doc, "user_level"), 0);
			int post_margin = NumberUtils.toInt(getStrFieldFromDocument(doc, "post_margin"), 0);
			int is_vip_resource = NumberUtils.toInt(getStrFieldFromDocument(doc, "is_vip_resource"), 0);

			long car_model_id = NumberUtils.toLong(getStrFieldFromDocument(doc, "car_model_id"), 0l);
			long base_car_id = NumberUtils.toLong(getStrFieldFromDocument(doc, "base_car_id"), 0l);
			String engine_capacity = getStrFieldFromDocument(doc, "engine_capacity");
			
			String last_base_price = getStrFieldFromDocument(doc, "last_base_price");
			String parent_base_price = getStrFieldFromDocument(doc, "parent_base_price");
			//TODO
			//String base_car_structure = getStrFieldFromDocument(doc, "base_car_structure");
			
			int user_deposit = NumberUtils.toInt(getStrFieldFromDocument(doc, "user_deposit"), 0);
			String promotion_resource_expired_at = getStrFieldFromDocument(doc, "promotion_resource_expired_at");
			long promotion_resource_expired_at_ms = 0;
			
			
			guiding_price = trimZeroForFloat(guiding_price);
			expect_price = trimZeroForFloat(expect_price);
			configuration_price = trimZeroForFloat(configuration_price);

			DateTime dt_updated_at = toUTCTime(updated_at);
			DateTime dt_created_at = toUTCTime(created_at);
			DateTime dt_user_created_at = toUTCTime(user_created_at);
			DateTime dt_expired_at = toUTCTime(expired_at);
			
			updated_at = dt_updated_at.toString(utc_formatter);
			created_at = dt_created_at.toString(utc_formatter);
			user_created_at = dt_user_created_at.toString(utc_formatter);
			expired_at = dt_expired_at.toString(utc_formatter);
			
			if (maintained_at == null || maintained_at.isEmpty())
				maintained_at = updated_at;
			DateTime dt_maintained_at = toUTCTime(maintained_at);
			if(promotion_resource_expired_at!=null && !promotion_resource_expired_at.isEmpty()){
				DateTime dt_promotion_resource_expired_at = toUTCTime(promotion_resource_expired_at);
				promotion_resource_expired_at_ms = createdAtMsFormat(dt_promotion_resource_expired_at);
			}

			long created_at_ms = createdAtMsFormat(dt_created_at);
			long updated_at_ms = createdAtMsFormat(dt_updated_at);
			long maintained_at_ms = createdAtMsFormat(dt_maintained_at);

			if (company_owner_id != null && company_owner_id.equals(user_id)) {
				user_name = short_name;
			} else {
				user_name = user_name + "(" + user_city_name + ")";
			}

			/*
			 * 在base_car_style中去掉brand_name和car_model_name信息
			 */
			String short_year = base_car_year.equals("0") ? "" : base_car_year.substring(2) + "款";// 15款

			int is_member_price = non_member_price == null || non_member_price.isEmpty() ? 0 : 1;
			int has_photo = photo_image == null || photo_image.isEmpty() ? 0 : 1;

			String title = generateTitle(post_type, brand_name, standard_name, car_model_name, base_car_style,
					base_car_NO, base_car_status, short_year);

			String info_price = generateInfoPrice(configuration_price, guiding_price, expect_price, discount_way,
					discount_content);
			String info_price_percentage = "";
			String info_price_amount = "";
			if (standard_id < 3 && post_type % 2 == 0) {
				info_price_percentage = generateInfoPricePercentage(configuration_price, guiding_price,
						expect_price, discount_way, discount_content);
				info_price_amount = generateInfoPriceAmount(configuration_price, guiding_price, expect_price,
						discount_way, discount_content);
			}

			car_model_name = escape(car_model_name);
			user_prov_name = escape(user_prov_name);
			outer_color = escapeFromSpecialCh(outer_color, true);
			inner_color = escapeFromSpecialCh(inner_color, true);
			outer_color = outer_color.replaceAll("\\.", "");
			inner_color = inner_color.replaceAll("\\.", "");
			String outer_color_s = outer_color;
			String inner_color_s = inner_color;
			if (outer_color.isEmpty()) {
				outer_color = "不限";
				outer_color_s = "不限";
			}
			if (inner_color.isEmpty()) {
				inner_color = "不限";
				inner_color_s = "不限";
			}
			standard_name = escape(standard_name);
			base_car_style = escape(base_car_style);
			firm_name = escape(firm_name);

			String brand_name_s = brand_name;
			brand_name = escapeFromSpecialCh(brand_name, true);
			String car_model_name_s = car_model_name;
			car_model_name = escapeFromSpecialCh(car_model_name, true);
			String firm_name_s = firm_name;
			firm_name = escapeFromSpecialCh(firm_name, true);
			String base_car_style_s = base_car_style;
			base_car_style = escapeFromSpecialCh(base_car_style, true);

			if (car_model_name.contains(brand_name)) {
				car_model_name = car_model_name.replaceAll(brand_name, "");
			}

			if (brand_name.equals("奔驰")) {
				base_car_style = benzBaseCarStyle(base_car_style);
			}

			if (base_car_style.contains(brand_name)) {
				base_car_style = base_car_style.replaceAll(brand_name, "");
			}

			String base_car_style_by_year_name_s = generateStyleYearName(base_car_status, base_car_year,
					base_car_style_s, short_year);
			String base_car_style_full_name = generateStyleFullName(base_car_status, base_car_style_s, base_price);
			base_car_year = base_car_year.equals("0") ? "其它" : base_car_year + "款";

			String base_car_name = standard_id > 2 && base_car_status == 1 ? (short_year + " " + base_car_style_s)
					: base_car_style_s;
			base_car_style = reGenerateStyle(base_car_style, short_year);
			base_car_style = appendEngineInfo(base_car_style, engine_capacity);
			String brand_firm_standard_s = generateBrandFirmStandard(brand_pinyin, standard_id, firm_name_s);
			String standard_name_with_id = Integer.toString(standard_id) + "%" + standard_name;

			String resource_type_s = null;
			if (resource_type == 1)
				resource_type_s = "期货";
			if (resource_type == 0)
				resource_type_s = "现车";

			String subtitle = (frame_number != null && !frame_number.isEmpty())
					? standard_name + "/" + resource_type_s + "/" + "车架号" + frame_number
					: standard_name + "/" + resource_type_s;

			String show_price = niuniuShowPrice(discount_way, expect_price);
			// update_at 到now的时间折合成天数
			int updateday_today_diff = calcDateDiff(dt_updated_at);
			String days_diff_level = null;
			if (updateday_today_diff < 3) {
				days_diff_level = "1";
			} else if (updateday_today_diff >= 3 && updateday_today_diff < 7) {
				days_diff_level = "2";
			} else { // if(updateday_today_diff>=7 && updateday_today_diff<30){
				days_diff_level = "3";
			}

			if (remark != null) {
				if (remark.trim().isEmpty())
					remark = null;
				else
					remark = "备注: " + escapeFromSpecialCh(remark, false);
			}
			if (configuration_remark != null) {
				if (configuration_remark.trim().isEmpty())
					configuration_remark = null;
				else
					configuration_remark = "配置: " + escapeFromSpecialCh(configuration_remark, false);
			}

			doc.clear();

			doc.addField("id", id);
			doc.addField("show_price", show_price);
			doc.addField("guiding_price", guiding_price);
			doc.addField("base_car_style", base_car_style);
			
			//这三个价格不能相同冗余
			ArrayList<Integer> base_price_NO = generatePriceNOFromBase(base_price);
			if(base_price_NO!=null){
				for(Integer it:base_price_NO){
					doc.addField("base_car_NO", it);
				}
			}else{
				doc.addField("base_car_NO", 0);
			}
			ArrayList<Integer> last_price_NO = generatePriceNOFromBase(last_base_price);
			if(last_price_NO!=null && !last_base_price.equals(base_price)){
				for(Integer it:last_price_NO){
					doc.addField("last_price_NO", it);
				}
			}else{
				doc.addField("last_price_NO", 0);
			}
			ArrayList<Integer> parent_price_NO = generatePriceNOFromBase(parent_base_price);
			if(parent_price_NO!=null && !parent_base_price.equals(base_price) && !parent_base_price.equals(last_base_price)){
				for(Integer it:parent_price_NO){
					doc.addField("parent_price_NO", it);
				}
			}else{
				doc.addField("parent_price_NO", 0);
			}
			
			
			doc.addField("inner_color", inner_color);
			doc.addField("outer_color", outer_color);
			doc.addField("configuration_remark", configuration_remark);
			doc.addField("standard_name", standard_name);
			doc.addField("brand_name", brand_name);
			doc.addField("firm_name", firm_name.toLowerCase());

			doc.addField("car_model_name", car_model_name);
			doc.addField("user_city_name", user_city_name);
			doc.addField("user_prov_name", user_prov_name);
			doc.addField("user_city_name_index", user_city_name);
			doc.addField("user_prov_name_index", user_prov_name);
			doc.addField("remark", remark);
			doc.addField("created_at", created_at);
			doc.addField("updated_at", updated_at);
			doc.addField("expired_at", expired_at);
			doc.addField("created_at_ms", created_at_ms);
			doc.addField("updated_at_ms", updated_at_ms);
			doc.addField("maintained_at_ms", maintained_at_ms);
			doc.addField("user_id", user_id);
			doc.addField("user_name", user_name);
			doc.addField("user_vip_level_name", user_vip_level_mapping[user_vip_level]);
			doc.addField("user_level_name", user_level_mapping[user_level]);
			doc.addField("user_vip_level", user_vip_level);

			doc.addField("title", title);
			doc.addField("subtitle", subtitle);
			doc.addField("info_price", info_price);
			doc.addField("is_member_price", is_member_price);
			doc.addField("has_photo", has_photo);
			doc.addField("configuration_price", configuration_price);

			doc.addField("base_car_year_s", base_car_year);
			doc.addField("base_car_style_full_name_s", base_car_style_full_name);
			doc.addField("base_car_style_by_year_name_s", base_car_style_by_year_name_s);
			doc.addField("base_car_name_s", base_car_name.trim());
			doc.addField("brand_firm_standard_s", brand_firm_standard_s);
			doc.addField("firm_name_s", firm_name_s);
			doc.addField("car_model_status_i", car_model_status);
			doc.addField("inner_color_s", inner_color_s);
			doc.addField("outer_color_s", outer_color_s);
			doc.addField("post_status_i", post_status);
			doc.addField("post_type_i", post_type);
			doc.addField("brand_name_s", brand_name_s);
			doc.addField("standard_name_s", standard_name);
			doc.addField("car_model_name_s", car_model_name_s);
			doc.addField("standard_name_with_id_s", standard_name_with_id);
			doc.addField("auction_aasm_state", aasm_state);
			doc.addField("source_prov_name_s", source_prov_name);
			doc.addField("resource_type_s", resource_type_s);
			doc.addField("days_diff_level", days_diff_level);
			doc.addField("user_created_at", user_created_at);

			doc.addField("brand_id", brand_id);
			String extend_info = generateExtendInfo(outer_color_s, inner_color_s, ticket_pack_time, car_in_area,
					take_car_area);
			doc.addField("extend_info", extend_info);
			doc.addField("filter_car_in_area", filter_car_in_area);
			doc.addField("filter_car_in_area_index", filter_car_in_area);
			doc.addField("filter_car_in_region", filter_car_in_region);
			doc.addField("filter_car_in_region_index", filter_car_in_region);
			doc.addField("car_model_id", car_model_id);
			doc.addField("base_car_id", base_car_id);
			doc.addField("not_official_resource", base_car_status ^ 1);

			/*
			 * 为资源增加年款，从而在搜索16款桑塔纳时，只去年款里去搜16款
			 */
			doc.addField("base_car_year_info", short_year);
			doc.addField("base_car_year_info", base_car_year);

			if (isValid(complete_order_count)) {
				doc.addField("user_complete_order_text", "|成交量:" + complete_order_count);
				int order_cnt = NumberUtils.toInt(complete_order_count);
				order_cnt = Math.min((order_cnt - 1) / 5 + 1, 5);
				doc.addField("niuniu_search_order_score", order_cnt);
			}

			if (expect_price != null && !expect_price.isEmpty())
				doc.addField("expect_price", expect_price);

			doc.addField("info_price_percentage", info_price_percentage);
			doc.addField("info_price_amount", info_price_amount);

			if (user_level == 2)
				user_level++;
			doc.addField("user_level", user_level);
			doc.addField("is_vip_resource", is_vip_resource);
			doc.addField("post_margin", post_margin);
			if (post_margin > 0)
				doc.addField("is_reliable_resource", 1);
			doc.addField("is_batch_resource", is_batch_resource);
			doc.addField("has_no_price", has_no_price);
			if (user_vip_level == 4) {
				doc.addField("niuniu_search_is_vip", 1);
			}
			doc.addField("niuniu_search_random_factor", generateRandomFactorForResource(id));
			doc.addField("user_deposit", user_deposit);
			if(promotion_resource_expired_at_ms!=0){
				doc.addField("promotion_resource_expired_at_ms", promotion_resource_expired_at_ms);
				doc.addField("niuniu_search_is_promotion_resource", 1);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		// 添加牛牛搜索排序特征
		Iterator<Map.Entry<String, String>> entries = search_feature_score.entrySet().iterator();
		while (entries.hasNext()) {
			Map.Entry<String, String> entry = entries.next();
			String key = entry.getKey();
			Integer value = NumberUtils.toInt(entry.getValue());
			doc.addField(key, value);
		}
		// pass it up the chain
		super.processAdd(cmd);
	}
}
