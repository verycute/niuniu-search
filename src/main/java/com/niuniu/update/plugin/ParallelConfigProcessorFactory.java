package com.niuniu.update.plugin;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.math.NumberUtils;
import org.apache.lucene.analysis.util.ResourceLoader;
import org.apache.solr.common.SolrInputDocument;
import org.apache.solr.common.util.NamedList;
import org.apache.solr.core.SolrCore;
import org.apache.solr.request.SolrQueryRequest;
import org.apache.solr.response.SolrQueryResponse;
import org.apache.solr.update.AddUpdateCommand;
import org.apache.solr.update.processor.UpdateRequestProcessor;
import org.apache.solr.update.processor.UpdateRequestProcessorFactory;
import org.apache.solr.util.plugin.SolrCoreAware;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ParallelConfigProcessorFactory extends UpdateRequestProcessorFactory implements SolrCoreAware {
	public final static Logger log = LoggerFactory.getLogger(ParallelConfigProcessorFactory.class);
	
	HashMap<Integer, HashMap<String, String>> parallelConfigMapper;
	String parallelConfigPath;
	
	@SuppressWarnings("rawtypes")
	public void init(NamedList args) {
		parallelConfigPath = args.get("parallel_config_path").toString();
		parallelConfigMapper = new HashMap<Integer, HashMap<String, String>>();
	}
	
	@Override
	public UpdateRequestProcessor getInstance(SolrQueryRequest req, SolrQueryResponse rsp,
			UpdateRequestProcessor next) {
		return new ParallelConfigProcessor(next, parallelConfigMapper);
	}
	
	public class ParallelConfigProcessor extends UpdateRequestProcessor {
		HashMap<Integer, HashMap<String, String>> parallelConfigMapper;
		
		public ParallelConfigProcessor(UpdateRequestProcessor next, HashMap<Integer, HashMap<String, String>> parallelConfigMapper) {
			super(next);
			this.parallelConfigMapper = parallelConfigMapper;
		}
		
		private int minimum(int a, int b, int c) {
			int im = a < b ? a : b;
			return im < c ? im : c;
		}

		public int getEditDistance(String s, String t) {
			int d[][]; // matrix
			int n; // length of s
			int m; // length of t
			int i; // iterates through s
			int j; // iterates through t
			char s_i; // ith character of s
			char t_j; // jth character of t
			int cost; // cost

			// Step 1
			n = s.length();
			m = t.length();
			if (n == 0) {
				return m;
			}
			if (m == 0) {
				return n;
			}
			d = new int[n + 1][m + 1];

			// Step 2
			for (i = 0; i <= n; i++) {
				d[i][0] = i;
			}
			for (j = 0; j <= m; j++) {
				d[0][j] = j;
			}

			// Step 3
			for (i = 1; i <= n; i++) {
				s_i = s.charAt(i - 1);
				// Step 4
				for (j = 1; j <= m; j++) {
					t_j = t.charAt(j - 1);
					// Step 5
					cost = (s_i == t_j) ? 0 : 1;
					// Step 6
					d[i][j] = minimum(d[i - 1][j] + 1, d[i][j - 1] + 1, d[i - 1][j - 1] + cost);
				}
			}
			// Step 7
			return d[n][m];
		}
		
		private String getStrFieldFromDocument(SolrInputDocument doc, String field) {
			Object v = doc.getFieldValue(field);
			return v == null ? null : v.toString();
		}
		
		public void processAdd(AddUpdateCommand cmd) throws IOException {
			SolrInputDocument doc = cmd.getSolrInputDocument();
			String standard_name = getStrFieldFromDocument(doc, "standard_name");
			int car_model_id = NumberUtils.toInt(getStrFieldFromDocument(doc, "car_model_id"));
			if(!standard_name.equals("国产") && !standard_name.equals("中规")){
				String configuration_remark = getStrFieldFromDocument(doc, "configuration_remark");
				String remark = getStrFieldFromDocument(doc, "remark");
				if((configuration_remark!=null && !configuration_remark.isEmpty() && configuration_remark.length()>=10) || (remark!=null && !remark.isEmpty() && remark.length()>=10)){
					ArrayList<String> ans = resolveParallelConfig(configuration_remark, remark, parallelConfigMapper.get(car_model_id));
					if(ans!=null && ans.size()>0)
						doc.addField("parallel_configs", ans);
				}
			}
			super.processAdd(cmd);
		}
		
		/*
		 * 特殊字符替换
		 */
		private String replaceSpecificChar(String configuration) {
			configuration = configuration.replaceAll("瓦", "W");//音响的功率单位
			return configuration.trim();
		}
		
		/*
		 * 剔除多余的空格
		 */
		private String removeDuplicateSpace(String configuration){
			return configuration.replaceAll(" +", " ");
		}
		
		private String preProcess(String configuration){
			return removeDuplicateSpace(replaceSpecificChar(configuration));
		}
		
		private ArrayList<String> resolveParallelConfig(String configuration, String remark, HashMap<String, String> config_guide) {
			if(config_guide==null)
				return null;
			Set<String> configSet1 = new HashSet<String>();
			Set<String> configSet2 = new HashSet<String>();
			
			if(configuration!=null && !configuration.isEmpty()){
				String line = preProcess(PluginUtils.normalize(configuration));
				line = line.substring(3).trim().toLowerCase();
				configSet1 = resolveParallelConfigBase(line, config_guide);
			}
			
			if(remark!=null && !remark.isEmpty()){
				String line = preProcess(PluginUtils.normalize(remark));
				//line = line.substring(3).trim().toLowerCase();
				configSet2 = resolveParallelConfigBase(line, config_guide);
			}
			ArrayList<String> configResult = new ArrayList<String>();
			configResult.addAll(configSet1);
			configResult.addAll(configSet2);
			return configResult;
		}
		
		private Set<String> resolveParallelConfigBase(String info, HashMap<String, String> config_guide) {
			if(config_guide==null)
				return null;
			String[] arrs = info.split("[:!()&；/,;。，、\\s\\\\t]");
			Set<String> configArray = new HashSet<String>();
			String config = null;
			for (String s : arrs) {
				if (valid(s)) {
					config = resolve(s, config_guide);
					if (config != null)
						configArray.add(config);
				}
			}
			return configArray;
		}

		private boolean valid(String str) {
			if (str.length() > 10)
				return false;
			if (str.endsWith("万"))
				return false;
			float f = NumberUtils.toFloat(str, 0f);
			if (f != 0)
				return false;

			return true;
		}

		private String resolve(String str, HashMap<String, String> config_guide) {
			if(str==null || str.length()<2)
				return null;
			Iterator<Map.Entry<String, String>> entries = config_guide.entrySet().iterator();
			while (entries.hasNext()) {
				Map.Entry<String, String> entry =  entries.next();
				String key = entry.getKey();
				String value = entry.getValue();
				boolean status = true;
				for(int i=0;i<str.length();i++){
					if(key.contains(str.substring(i,i+1)))
						continue;
					else{
						status = false;
						break;
					}
				}
				if(status)
					return value;
			}
			entries = config_guide.entrySet().iterator();
			while (entries.hasNext()) {
				Map.Entry<String, String> entry = entries.next();
				String value = entry.getValue();
				boolean status = true;
				for(int i=0;i<str.length();i++){
					if(value.contains(str.substring(i,i+1)))
						continue;
					else{
						status = false;
						break;
					}
				}
				if(status)
					return value;
			}
			entries = config_guide.entrySet().iterator();
			while (entries.hasNext()) {
				Map.Entry<String, String> entry = entries.next();
				String key = entry.getKey();
				String value = entry.getValue();
				if(str.contains(key) || str.contains(value.toLowerCase()))
					return value;
			}
			return null;
		}

	}

	//读取词典文件
	private void readParallelConfigInfo(ResourceLoader loader, String filename) {
		if (filename == null) {
			return;
		}
		InputStream is = null;
		try {
			is = loader.openResource(filename);
			if (is == null) {
				log.error("找不到平行进口配置文件", new RuntimeException(filename + " Dictionary not found!!!"));
			}
			BufferedReader br = new BufferedReader(new InputStreamReader(is, "UTF-8"), 512);
			String line = null;
			String car_model = "";
			HashMap<String, String> configs = new HashMap<String, String>();
			do {
				line = br.readLine();
				if (line == null)
					break;
				line = line.trim().toLowerCase();
				if(line.startsWith("#"))
					continue;
				if(line.startsWith("!")){
					parallelConfigMapper.put(NumberUtils.toInt(car_model), configs);
					car_model = line.replaceAll("!", "").trim();
					configs = new HashMap<String, String>();
				}else{
					String[] arrs = line.split("\t");
					if(arrs.length!=2)
						continue;
					configs.put(arrs[0], arrs[1]);
				}
			} while (line != null);
			if(configs.size()>0)
				parallelConfigMapper.put(NumberUtils.toInt(car_model), configs);
		} catch (IOException ioe) {
			log.error("平行进口配置文件读取失败", ioe);
		}
	}
	
	@Override
	public void inform(SolrCore solrCore) {
		readParallelConfigInfo(solrCore.getResourceLoader(), this.parallelConfigPath);
		log.info("[parallel_config_update_processor]\t size is : {} ",parallelConfigMapper.size());
	}

}
