package com.niuniu.update.plugin;

public class PluginUtils {
	// 全角半角转换
	private static String replace(String line) {
		char[] c = line.toCharArray();
		for (int i = 0; i < c.length; i++) {
			if (c[i] == 12288) {
				c[i] = (char) 32;
				continue;
			}
			if (c[i] > 65280 && c[i] < 65375)
				c[i] = (char) (c[i] - 65248);
		}
		return new String(c);
	}
	
	private static char[] signs = { '+', '-', '!', ':', '?', '*', '~', '^', '"', '\\', '(', ')', '[', ']', '{', '}',
	' ' };
	
	private static boolean needToConvert(char c) {
		for (int i = 0; i < signs.length; i++) {
			if (signs[i] == c)
				return true;
		}
		return false;
	}
	
	/*
	 * 全角=>半角
	 */
	public static String normalize(String query) {
		query = replace(query.trim());

		String regex = " +";
		query = query.replaceAll(regex, " ").trim();
		char[] a = query.toCharArray();
		char[] b = new char[5000];
		int i = 0;
		for (char c : a) {
			if (!needToConvert(c)) {
				b[i++] = c;
			} else {
				//b[i++] = '\\';
				b[i++] = c;
			}
		}
		String c = new String(b, 0, i);
		return c;
	}
}
