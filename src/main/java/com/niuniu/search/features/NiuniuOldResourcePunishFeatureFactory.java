package com.niuniu.search.features;

import java.util.Date;

import org.apache.commons.lang.math.NumberUtils;
import org.apache.lucene.index.NumericDocValues;
import org.apache.solr.common.util.NamedList;
import org.apache.solr.request.SolrQueryRequest;

import com.niuniu.rankings.NiuniuBaseFeature;
import com.niuniu.rankings.NiuniuBaseFeatureFactory;

public class NiuniuOldResourcePunishFeatureFactory extends NiuniuBaseFeatureFactory {
	String field_name;
	int days_diff;

	@Override
	@SuppressWarnings("rawtypes")
	public void init(NamedList args) {
		field_name = (String) args.get("fields");
		String days_diff_s = (String) args.get("days_diff");
		days_diff = NumberUtils.toInt(days_diff_s, 3);
	}

	@Override
	public NiuniuBaseFeature getInstance(String userinfo) {
		return new NiuniuOldResourcePunishFeature(field_name, days_diff);
	}

	public class NiuniuOldResourcePunishFeature extends NiuniuBaseFeature {

		Date current = null;
		long current_ms = 0;
		String field_name;
		int days_diff;
		NumericDocValues updated_at_ms = null;

		public NiuniuOldResourcePunishFeature(String field_name, int days_diff) {
			this.field_name = field_name;
			this.days_diff = days_diff;
		}

		@Override
		public void prepare(SolrQueryRequest req) {
			try {
				current = new Date();
				current_ms = current.getTime();
				updated_at_ms = req.getSearcher().getAtomicReader().getNumericDocValues(this.field_name);
			} catch (Exception e) {

			}
		}

		@Override
		public String getName() {
			return "长期未更新资源罚分";
		}

		@Override
		public int score(int docId) {
			long ms_diff = current_ms - updated_at_ms.get(docId);
			if (ms_diff >= (3600000 * 24 * days_diff))
				return 1;
			return 0;
		}
	}
}
