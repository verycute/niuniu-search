package com.niuniu.search.features;

import org.apache.lucene.index.NumericDocValues;
import org.apache.solr.common.util.NamedList;
import org.apache.solr.request.SolrQueryRequest;

import com.niuniu.rankings.NiuniuBaseFeature;
import com.niuniu.rankings.NiuniuBaseFeatureFactory;

public class NiuniuUnqualifiedResourcePunishFeatureFactory extends NiuniuBaseFeatureFactory{
	String field_name;

	@Override
	@SuppressWarnings("rawtypes")
	public void init(NamedList args) {
		field_name = (String) args.get("fields");
	}

	@Override
	public NiuniuBaseFeature getInstance(String userinfo) {
		return new NiuniuUnqualifiedResourcePunishFeature(field_name);
	}

	public class NiuniuUnqualifiedResourcePunishFeature extends NiuniuBaseFeature {

		String field_name;
		NumericDocValues userLevelDocValue = null;

		public NiuniuUnqualifiedResourcePunishFeature(String field_name) {
			this.field_name = field_name;
		}

		@Override
		public void prepare(SolrQueryRequest req) {
			try {
				userLevelDocValue = req.getSearcher().getAtomicReader().getNumericDocValues(this.field_name);
			} catch (Exception e) {
				throw new RuntimeException();
			}
		}

		@Override
		public String getName() {
			return "个人资源罚分，手机认证罚分大于身份认证";
		}

		@Override
		public int score(int docId) {
			long user_level = userLevelDocValue.get(docId);
			if(user_level==0)
				return 2;
			if(user_level==1)
				return 1;
			return 0;
		}
	}
}
