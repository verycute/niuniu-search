package com.niuniu.search.features;

import java.io.IOException;

import org.apache.lucene.index.SortedDocValues;
import org.apache.lucene.util.BytesRef;
import org.apache.solr.common.util.NamedList;
import org.apache.solr.request.SolrQueryRequest;

import com.niuniu.rankings.NiuniuBaseFeature;
import com.niuniu.rankings.NiuniuBaseFeatureFactory;

public class NiuniuGeoPreferenceFeatureFactory extends NiuniuBaseFeatureFactory {

	String province_field;
	String city_field;

	@Override
	@SuppressWarnings("rawtypes")
	public void init(NamedList args) {
		province_field = (String) args.get("province_field");
		city_field = (String) args.get("city_field");
	}

	@Override
	public NiuniuGeoPreferenceFeature getInstance(String userinfo) {
		return new NiuniuGeoPreferenceFeature(province_field, city_field, userinfo);
	}

	public class NiuniuGeoPreferenceFeature extends NiuniuBaseFeature {
		private String province_field;
		private String city_field;
		private SortedDocValues geoProvDocValue = null;
		private SortedDocValues geoCityDocValue = null;
		private String userinfo;
		private String user_province;
		private String user_city;

		public NiuniuGeoPreferenceFeature(String province_field, String city_field, String userinfo) {
			this.province_field = province_field;
			this.city_field = city_field;
			this.userinfo = userinfo;
		}

		public void parse(String userinfo) {
			if (userinfo == null)
				return;
			String[] infos = userinfo.split(";");
			for (int i = 0; i < infos.length; i++) {
				String[] tmp = infos[i].split(":");
				if (tmp.length != 2)
					continue;
				if (tmp[0].equals("user_province")) {
					user_province = tmp[1];
				}
				if (tmp[0].equals("user_city")) {
					user_city = tmp[1];
				}
			}
		}

		@Override
		public int score(int docId) {
			if ((user_province == null || user_province.isEmpty()) && (user_city == null || user_city.isEmpty()))
				return 0;
			BytesRef bytes = new BytesRef();
			if (user_city != null && !user_city.isEmpty()) {
				geoCityDocValue.get(docId, bytes);
				String city = bytes.utf8ToString();
				if (city.equals(user_city))
					return 2;
			}
			if (user_province != null && !user_province.isEmpty()) {
				geoProvDocValue.get(docId, bytes);
				String prov = bytes.utf8ToString();
				if (prov.equals(user_province))
					return 1;
			}
			return 0;
		}

		@Override
		public String getName() {
			return "地域偏好加权";
		}

		@Override
		public void prepare(SolrQueryRequest req) {
			try {
				parse(userinfo);
				geoProvDocValue = req.getSearcher().getAtomicReader().getSortedDocValues(province_field);
				geoCityDocValue = req.getSearcher().getAtomicReader().getSortedDocValues(city_field);
			} catch (IOException e) {
				throw new RuntimeException();
			}
		}

	}

}
