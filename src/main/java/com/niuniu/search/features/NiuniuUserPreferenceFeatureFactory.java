package com.niuniu.search.features;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.math.NumberUtils;
import org.apache.lucene.index.NumericDocValues;
import org.apache.solr.common.util.NamedList;
import org.apache.solr.request.SolrQueryRequest;

import com.niuniu.rankings.NiuniuBaseFeature;
import com.niuniu.rankings.NiuniuBaseFeatureFactory;

/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

public class NiuniuUserPreferenceFeatureFactory extends NiuniuBaseFeatureFactory {

	@SuppressWarnings("rawtypes")
	@Override
	public void init(NamedList args) {

	}

	@Override
	public NiuniuUserPreferenceFeature getInstance(String userinfo) {
		return new NiuniuUserPreferenceFeature("niuniu_user_preference", userinfo);
	}

	public class NiuniuUserPreferenceFeature extends NiuniuBaseFeature {

		private String feature_name;
		private NumericDocValues userIdDocValue = null;
		private String userinfo;
		private Map<Long, Integer> userPrefWeight = null;

		public NiuniuUserPreferenceFeature(String name, String userinfo) {
			this.feature_name = name;
			this.userinfo = userinfo;
		}

		public void parse(String userinfo) {
			String[] infos = userinfo.split(";");
			if (infos.length > 0)
				userPrefWeight = new HashMap<Long, Integer>();
			for (int i = 0; i < infos.length; i++) {
				String[] tmp = infos[i].split(",");
				if (tmp.length != 2)
					continue;
				if (tmp[0].equals("user_prefenrence_info")) {
					String[] tmp_infos = tmp[1].split("\\|");
					for (int j = 0; j < tmp_infos.length; j++) {
						String[] id_status = tmp_infos[j].split("\\^");
						if (id_status.length != 2)
							continue;
						userPrefWeight.put(NumberUtils.createLong(id_status[0]),
								NumberUtils.createInteger(id_status[1]));
					}
				}
			}
		}

		@Override
		public int score(int docId) {
			long user_id = userIdDocValue.get(docId);
			if (userPrefWeight.containsKey(user_id)) {
				return 10 * userPrefWeight.get(user_id);
			}
			return 0;
		}

		@Override
		public String getName() {
			return feature_name;
		}

		@Override
		public void prepare(SolrQueryRequest req) {
			try {
				parse(userinfo);
				userIdDocValue = req.getSearcher().getAtomicReader().getNumericDocValues("user_id");
			} catch (IOException e) {
				throw new RuntimeException();
			}
		}

	}

}
