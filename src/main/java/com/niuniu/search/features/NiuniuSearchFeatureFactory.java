package com.niuniu.search.features;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.math.NumberUtils;
import org.apache.lucene.index.NumericDocValues;
import org.apache.solr.common.util.NamedList;
import org.apache.solr.request.SolrQueryRequest;

import com.niuniu.rankings.NiuniuBaseFeature;
import com.niuniu.rankings.NiuniuBaseFeatureFactory;

/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

public class NiuniuSearchFeatureFactory extends NiuniuBaseFeatureFactory{
	  
	  //Specify the Doc Values for reranking 
	  List<String> fields;
	  List<Integer> weights;
	  public void parseUserInfo(String uinfo){
	    
	  }
	  
	  @SuppressWarnings("rawtypes")
	  @Override
	  public void init(NamedList args) {
	    fields = new ArrayList<String>();
	    weights = new ArrayList<Integer>();
	    String fString = (String)args.get("fields");
	    String fWeight = (String)args.get("weights");
	    String[] field_arr = fString.split(",");
	    for(String field:field_arr){
	      fields.add(field.trim());
	    }
	    String[] weight_arr = fWeight.split(",");
	    for(String weight:weight_arr){
	      weights.add(NumberUtils.createInteger(weight.trim()));
	    }
	  }
	  
	  @Override
	  public NiuniuSearchFeature getInstance(String userinfo) {
	    return new NiuniuSearchFeature(fields, weights);
	  }
	  
	  
	  public class NiuniuSearchFeature extends NiuniuBaseFeature{
	    
	    private List<String> fields;
	    private List<Integer> weights;
	    Map<NumericDocValues,Integer> searchDocValues = null;
	    
	    public NiuniuSearchFeature(List<String> fields, List<Integer> weights){
	      this.fields = fields;
	      this.weights = weights;
	    }
	    
	    public void parse(String userinfo){
	      
	    }

	    @SuppressWarnings("rawtypes")
	    @Override
	    public int score(int docId) {
	      int sum = 0;
	      Iterator entries = searchDocValues.entrySet().iterator();
	      while(entries.hasNext()){
	        Map.Entry entry = (Map.Entry) entries.next();
	        NumericDocValues key = (NumericDocValues)entry.getKey();
	        Integer value = (Integer)entry.getValue();
	        sum += value * ((int)key.get(docId));
	      }
	      return sum;
	    }

	    @Override
	    public String getName() {
	      //return fields.toString();
	      return "业务逻辑(置顶资源，认证等级，靠谱资源，自定义资源，批量资源，电议资源，商家资质，会员资源，会员订单)分";
	    }

	    
	    /*
	     * parse user info 
	     */
	    @Override
	    public void prepare(SolrQueryRequest req) {
	      try{
	        searchDocValues = new HashMap<NumericDocValues, Integer>();
	        parse(null);
	        for(int i=0;i<fields.size();i++){
	          searchDocValues.put(req.getSearcher().getAtomicReader().getNumericDocValues(fields.get(i)), weights.get(i));
	        }
	      }catch(IOException e){
	        e.printStackTrace();
	      }
	    }
	  }

	}
