package com.niuniu.search.features;

import java.util.Date;

import org.apache.lucene.index.NumericDocValues;
import org.apache.solr.common.util.NamedList;
import org.apache.solr.request.SolrQueryRequest;

import com.niuniu.rankings.NiuniuBaseFeature;
import com.niuniu.rankings.NiuniuBaseFeatureFactory;

public class NiuniuPromotionResourceFeatureFactory extends NiuniuBaseFeatureFactory {
	String field_name;

	@SuppressWarnings("rawtypes")
	@Override
	public void init(NamedList args) {
		field_name = (String) args.get("fields");
	}

	@Override
	public NiuniuBaseFeature getInstance(String userinfo) {
		return new NiuniuPromotionResourceFeature(field_name);
	}

	public class NiuniuPromotionResourceFeature extends NiuniuBaseFeature {

		Date current = null;
		long current_ms = 0;
		String field_name;
		NumericDocValues promotion_resource_expired_at_ms = null;

		public NiuniuPromotionResourceFeature(String field_name) {
			this.field_name = field_name;
		}

		@Override
		public void prepare(SolrQueryRequest req) {
			try {
				current = new Date();
				current_ms = current.getTime();
				promotion_resource_expired_at_ms = req.getSearcher().getAtomicReader()
						.getNumericDocValues(this.field_name);
			} catch (Exception e) {

			}
		}

		@Override
		public String getName() {
			return "保证金资源实时加分";
		}

		@Override
		public int score(int docId) {
			long ms_diff = promotion_resource_expired_at_ms.get(docId);
			if (ms_diff >= current_ms)
				return 1;
			return 0;
		}
	}
}
