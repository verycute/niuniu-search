package com.niuniu.search.features;

import java.util.Date;

import org.apache.lucene.index.NumericDocValues;
import org.apache.solr.common.util.NamedList;
import org.apache.solr.request.SolrQueryRequest;

import com.niuniu.rankings.NiuniuBaseFeature;
import com.niuniu.rankings.NiuniuBaseFeatureFactory;

/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

public class NiuniuNewResourceFeatureFactory extends NiuniuBaseFeatureFactory {
	String field_name;

	@Override
	@SuppressWarnings("rawtypes")
	public void init(NamedList args) {
		field_name = (String) args.get("fields");
	}

	@Override
	public NiuniuBaseFeature getInstance(String userinfo) {
		return new NiuniuNewResourceFeature(field_name);
	}

	public class NiuniuNewResourceFeature extends NiuniuBaseFeature {

		Date current = null;
		long current_ms = 0;
		String field_name;
		NumericDocValues created_at_ms = null;

		public NiuniuNewResourceFeature(String field_name) {
			this.field_name = field_name;
		}

		@Override
		public void prepare(SolrQueryRequest req) {
			try {
				current = new Date();
				current_ms = current.getTime();
				created_at_ms = req.getSearcher().getAtomicReader().getNumericDocValues(this.field_name);
			} catch (Exception e) {

			}
		}

		@Override
		public String getName() {
			return "新发资源加分";
		}

		@Override
		public int score(int docId) {
			long ms_diff = current_ms - created_at_ms.get(docId);
			if (ms_diff >= (7200000 * 24) || ms_diff < 0)
				return 0;
			if (ms_diff >= (3600000 * 24))
				return 1;
			return 2;
		}

	}

}
