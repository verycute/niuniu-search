package com.niuniu.search.features;

import java.io.IOException;

import org.apache.lucene.index.NumericDocValues;
import org.apache.solr.common.util.NamedList;
import org.apache.solr.request.SolrQueryRequest;

import com.niuniu.rankings.NiuniuBaseFeature;
import com.niuniu.rankings.NiuniuBaseFeatureFactory;

/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

public class NiuniuOnlineOrderFeatureFactory extends NiuniuBaseFeatureFactory {

	@Override
	@SuppressWarnings("rawtypes")
	public void init(NamedList args) {

	}

	@Override
	public NiuniuOnlineOrderFeature getInstance(String userinfo) {
		return new NiuniuOnlineOrderFeature("niuniu_online_order", userinfo);
	}

	public class NiuniuOnlineOrderFeature extends NiuniuBaseFeature {

		private String feature_name;
		private NumericDocValues userIdDocValue = null;
		private String userinfo;

		public NiuniuOnlineOrderFeature(String name, String userinfo) {
			this.feature_name = name;
			this.userinfo = userinfo;
		}

		public void parse(String userinfo) {
			try {
				Thread.sleep(1);
			} catch (InterruptedException e) {
				throw new RuntimeException();
			}
		}

		@Override
		public int score(int docId) {
			long user_id = userIdDocValue.get(docId);
			if (user_id % 2 == 0)
				return 10;
			return -10;
		}

		@Override
		public String getName() {
			return feature_name;
		}

		@Override
		public void prepare(SolrQueryRequest req) {
			try {
				parse(userinfo);
				userIdDocValue = req.getSearcher().getAtomicReader().getNumericDocValues("user_id");
			} catch (IOException e) {
				throw new RuntimeException();
			}
		}

	}

}
