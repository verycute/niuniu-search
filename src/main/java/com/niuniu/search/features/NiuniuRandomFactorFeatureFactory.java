package com.niuniu.search.features;

import org.apache.lucene.index.NumericDocValues;
import org.apache.solr.common.util.NamedList;
import org.apache.solr.request.SolrQueryRequest;

import com.niuniu.rankings.NiuniuBaseFeature;
import com.niuniu.rankings.NiuniuBaseFeatureFactory;

public class NiuniuRandomFactorFeatureFactory extends NiuniuBaseFeatureFactory {
	String field_name;

	public class NiuniuRandomFactorFeature extends NiuniuBaseFeature {

		String field_name;
		NumericDocValues random_values = null;

		public NiuniuRandomFactorFeature(String field_name) {
			this.field_name = field_name;
		}

		@Override
		public void prepare(SolrQueryRequest req) {
			try {
				random_values = req.getSearcher().getAtomicReader().getNumericDocValues(this.field_name);
			} catch (Exception e) {

			}
		}

		@Override
		public String getName() {
			return "随机因子加分";
		}

		@Override
		public int score(int docId) {
			int value = (int) (random_values.get(docId));
			return value;
		}
	}

	@Override
	public NiuniuBaseFeature getInstance(String arg0) {
		return new NiuniuRandomFactorFeature(field_name);
	}

	@SuppressWarnings("rawtypes")
	public void init(NamedList args) {
		field_name = (String) args.get("fields");
	}
}
