package com.niuniu.utils;

import java.util.HashMap;
import java.util.Map;

import net.sourceforge.pinyin4j.PinyinHelper;
import net.sourceforge.pinyin4j.format.HanyuPinyinCaseType;
import net.sourceforge.pinyin4j.format.HanyuPinyinOutputFormat;
import net.sourceforge.pinyin4j.format.HanyuPinyinToneType;
import net.sourceforge.pinyin4j.format.HanyuPinyinVCharType;
import net.sourceforge.pinyin4j.format.exception.BadHanyuPinyinOutputFormatCombination;

public class NiuniuUtils {
	private static String[] multiPhoneticChar = { "丁", "乐", "仔", "传", "伽", "俊", "兹", "劲", "勒", "南", "卡", "员", "地", "塔",
			"塞", "壳", "夏", "大", "奇", "娜", "家", "广", "度", "思", "拓", "摩", "景", "曜", "杉", "派", "盛", "盾", "系", "红", "育",
			"艾", "蔚", "虫", "行", "车", "长", "阿", "陆", "顿", "鸟" };
	private static String[] multiPhonetic = { "ding", "le", "zai", "chuan", "ga", "jun", "zi", "jin", "le", "nan", "ka",
			"yuan", "di", "ta", "sai", "ke", "xia", "da", "qi", "na", "jia", "guang", "du", "si", "tuo", "mo", "jing",
			"yao", "shan", "pai", "sheng", "dun", "xi", "hong", "yu", "ai", "wei", "chong", "xing", "che", "chang", "a",
			"lu", "dun", "niao" };
	private static Map<String, String> phoneticMapping = null;

	static {
		phoneticMapping = new HashMap<String, String>();
		for (int i = 0; i < multiPhoneticChar.length; i++) {
			phoneticMapping.put(multiPhoneticChar[i], multiPhonetic[i]);
		}
	}

	public static String toPinyin(String input, String delim) {
		char[] chars = input.toCharArray();
		int len = chars.length;
		// cut the string

		String[] t2 = new String[chars.length];
		HanyuPinyinOutputFormat outputFormat = new HanyuPinyinOutputFormat();
		outputFormat.setCaseType(HanyuPinyinCaseType.LOWERCASE);
		outputFormat.setToneType(HanyuPinyinToneType.WITHOUT_TONE);
		outputFormat.setVCharType(HanyuPinyinVCharType.WITH_V);
		String target = "";
		int latest_status = -1;// 1 means Chinese characters, 0 means
								// non-Chinese characters
		int status = -1;
		String res = "";
		try {
			for (int i = 0; i < len; i++) {
				// 判断是否为汉字字符
				String cur = java.lang.Character.toString(chars[i]);
				if (cur.matches("[\\一-\\龥]+")) {
					if (phoneticMapping.containsKey(cur)) {
						res = phoneticMapping.get(cur);
					} else {
						t2 = PinyinHelper.toHanyuPinyinStringArray(chars[i], outputFormat);
						if (t2 == null)
							res = "";
						else
							res = t2[0];
					}
					status = 1;
					if (latest_status == status) {
						target += res;
					} else {
						target += delim + res;
						latest_status = 1;
					}
				} else {
					status = 0;
					if (cur.equals(" ")) {
						latest_status = -1;
						continue;
					}
					if (status == latest_status) {
						target += Character.toString(chars[i]);
					} else {
						target += delim + cur;
						latest_status = status;
					}
				}
			}
		} catch (BadHanyuPinyinOutputFormatCombination e1) {
			e1.printStackTrace();
		}
		return target.trim().toLowerCase();
	}
}
