package com.niuniu.queryparser;

import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.WildcardQuery;
import org.apache.lucene.search.BooleanClause.Occur;
import org.apache.solr.SolrTestCaseJ4;
import org.apache.solr.request.SolrQueryRequest;
import org.apache.solr.search.QParser;
import org.junit.BeforeClass;
import org.junit.Test;

public class TestNiuniuPrefixQueryParser extends SolrTestCaseJ4 {
	@BeforeClass
	public static void beforeClass() throws Exception {
		initCore("solrconfig-prefix.xml", "schema-prefix.xml");
	}

	@Test
	public void testPrefixQueryParser() throws Exception {
		{
			SolrQueryRequest req = req();
			QParser parser = null;
			String qstr = "长安";
			try {
				parser = QParser.getParser(qstr, "prefixparser", req);
			} catch (Exception e) {
				throw new RuntimeException("getParser excep using defType=" + "prefixparser with qstr=" + qstr, e);
			}
			Query q = parser.parse();
			assertNotNull("expected query", q);
			BooleanQuery query = new BooleanQuery();
			{
				Query q1 = new WildcardQuery(new Term("query", "长安*"));
				Query q2 = new WildcardQuery(new Term("query_str", "长安*"));
				Query q3 = new WildcardQuery(new Term("query_pinyin", "changan*"));
				Query q4 = new WildcardQuery(new Term("query_pinyin_abbrev", "长安*"));
				
				BooleanQuery qq = new BooleanQuery();
				qq.add(q1, Occur.SHOULD);
				qq.add(q2, Occur.SHOULD);
				qq.add(q3, Occur.SHOULD);
				qq.add(q4, Occur.SHOULD);
				query.add(qq, Occur.MUST);
			}
			
			assertTrue(q.equals(query));
		}
	}
	
	@Test
	public void testPrefixQueryParserWithSpace() throws Exception {
		{
			SolrQueryRequest req = req();
			QParser parser = null;
			String qstr = "大众 帕萨";
			try {
				parser = QParser.getParser(qstr, "prefixparser", req);
			} catch (Exception e) {
				throw new RuntimeException("getParser excep using defType=" + "prefixparser with qstr=" + qstr, e);
			}
			Query q = parser.parse();
			assertNotNull("expected query", q);
			BooleanQuery query = new BooleanQuery();
			{
				BooleanQuery tq = new BooleanQuery();
				tq.add(new TermQuery(new Term("query","大众")), BooleanClause.Occur.SHOULD);
				tq.add(new TermQuery(new Term("query_str","大众")), BooleanClause.Occur.SHOULD);
				tq.add(new TermQuery(new Term("query_pinyin","dazhong")), BooleanClause.Occur.SHOULD);
				tq.add(new TermQuery(new Term("query_pinyin_abbrev","大众")), BooleanClause.Occur.SHOULD);
				query.add(tq, Occur.MUST);
				
				Query q1 = new WildcardQuery(new Term("query", "帕萨*"));
				Query q2 = new WildcardQuery(new Term("query_str", "帕萨*"));
				Query q3 = new WildcardQuery(new Term("query_pinyin", "pasa*"));
				Query q4 = new WildcardQuery(new Term("query_pinyin_abbrev", "帕萨*"));
				
				BooleanQuery qq = new BooleanQuery();
				qq.add(q1, Occur.SHOULD);
				qq.add(q2, Occur.SHOULD);
				qq.add(q3, Occur.SHOULD);
				qq.add(q4, Occur.SHOULD);
				query.add(qq, Occur.MUST);
			}
			
			assertTrue(q.equals(query));
		}
	}
	
	@Test
	public void testPrefixQueryParserHybrid() throws Exception {
		{
			SolrQueryRequest req = req();
			QParser parser = null;
			String qstr = "宝ma";
			try {
				parser = QParser.getParser(qstr, "prefixparser", req);
			} catch (Exception e) {
				throw new RuntimeException("getParser excep using defType=" + "prefixparser with qstr=" + qstr, e);
			}
			Query q = parser.parse();
			assertNotNull("expected query", q);
			BooleanQuery query = new BooleanQuery();
			{
				Query q1 = new WildcardQuery(new Term("query", "宝ma*"));
				Query q2 = new WildcardQuery(new Term("query_str", "宝ma*"));
				Query q3 = new WildcardQuery(new Term("query_pinyin", "baoma*"));
				Query q4 = new WildcardQuery(new Term("query_pinyin_abbrev", "宝ma*"));
				
				BooleanQuery qq = new BooleanQuery();
				qq.add(q1, Occur.SHOULD);
				qq.add(q2, Occur.SHOULD);
				qq.add(q3, Occur.SHOULD);
				qq.add(q4, Occur.SHOULD);
				query.add(qq, Occur.MUST);
			}
			
			assertTrue(q.equals(query));
		}
	}
	
	@Test
	public void testPrefixQueryParserIllegal() throws Exception {
		{
			SolrQueryRequest req = req();
			QParser parser = null;
			String qstr = "";
			try {
				parser = QParser.getParser(qstr, "prefixparser", req);
			} catch (Exception e) {
				throw new RuntimeException("getParser excep using defType=" + "prefixparser with qstr=" + qstr, e);
			}
			Query q = parser.parse();
			assertNotNull("expected query", q);
			BooleanQuery query = new BooleanQuery();
			{
				Query q1 = new WildcardQuery(new Term("query", "*"));
				Query q2 = new WildcardQuery(new Term("query_str", "*"));
				Query q3 = new WildcardQuery(new Term("query_pinyin", "*"));
				Query q4 = new WildcardQuery(new Term("query_pinyin_abbrev", "*"));
				
				BooleanQuery qq = new BooleanQuery();
				qq.add(q1, Occur.SHOULD);
				qq.add(q2, Occur.SHOULD);
				qq.add(q3, Occur.SHOULD);
				qq.add(q4, Occur.SHOULD);
				query.add(qq, Occur.MUST);
			}
			
			assertTrue(q.equals(query));
		}
		{
			SolrQueryRequest req = req();
			QParser parser = null;
			String qstr = " ";
			try {
				parser = QParser.getParser(qstr, "prefixparser", req);
			} catch (Exception e) {
				throw new RuntimeException("getParser excep using defType=" + "prefixparser with qstr=" + qstr, e);
			}
			Query q = parser.parse();
			assertNotNull("expected query", q);
			BooleanQuery query = new BooleanQuery();
			{
				Query q1 = new WildcardQuery(new Term("query", "*"));
				Query q2 = new WildcardQuery(new Term("query_str", "*"));
				Query q3 = new WildcardQuery(new Term("query_pinyin", "*"));
				Query q4 = new WildcardQuery(new Term("query_pinyin_abbrev", "*"));
				
				BooleanQuery qq = new BooleanQuery();
				qq.add(q1, Occur.SHOULD);
				qq.add(q2, Occur.SHOULD);
				qq.add(q3, Occur.SHOULD);
				qq.add(q4, Occur.SHOULD);
				query.add(qq, Occur.MUST);
			}
			
			assertTrue(q.equals(query));
		}
		
	}
	
}
