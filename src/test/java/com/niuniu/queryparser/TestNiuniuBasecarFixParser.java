package com.niuniu.queryparser;

import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.WildcardQuery;
import org.apache.lucene.search.BooleanClause.Occur;
import org.apache.solr.SolrTestCaseJ4;
import org.apache.solr.request.SolrQueryRequest;
import org.apache.solr.search.QParser;
import org.junit.BeforeClass;
import org.junit.Test;

public class TestNiuniuBasecarFixParser extends SolrTestCaseJ4 {
	@BeforeClass
	public static void beforeClass() throws Exception {
		initCore("solrconfig-basecarfix.xml", "schema-basecarfix.xml");
	}

	@Test
	public void testQueryParser() throws Exception {
		{
			SolrQueryRequest req = req();
			QParser parser = null;
			String qstr = "16款奥迪A4l 298";
			try {
				parser = QParser.getParser(qstr, "basecarfixparser", req);
			} catch (Exception e) {
				throw new RuntimeException("getParser excep using defType=" + "basecarfixparser with qstr=" + qstr, e);
			}
			Query q = parser.parse();
			assertNotNull("expected query", q);
			BooleanQuery query = new BooleanQuery();
			
			{
				BooleanQuery char_query = new BooleanQuery();
				//char_query.add(new TermQuery(new Term("base_car_style", "款")), Occur.SHOULD);//停止此过滤
				char_query.add(new TermQuery(new Term("base_car_style", "奥")), Occur.SHOULD);
				char_query.add(new TermQuery(new Term("base_car_style", "迪")), Occur.SHOULD);
				char_query.add(new TermQuery(new Term("base_car_style", "a4l")), Occur.SHOULD);
				char_query.add(new TermQuery(new Term("base_car_style", "款奥")), Occur.SHOULD);
				char_query.add(new TermQuery(new Term("base_car_style", "奥迪")), Occur.SHOULD);
				char_query.add(new TermQuery(new Term("base_car_style", "款奥迪")), Occur.SHOULD);
				query.add(char_query, Occur.MUST);
			}
			{
				BooleanQuery num_query = new BooleanQuery();
				num_query.add(new TermQuery(new Term("base_car_style", "16")), Occur.SHOULD);
				num_query.add(new TermQuery(new Term("base_car_NO", "16")), Occur.SHOULD);
				num_query.add(new TermQuery(new Term("year", "16")), Occur.SHOULD);
				query.add(num_query, Occur.MUST);
			}
			{
				BooleanQuery last_query = new BooleanQuery();
				last_query.add(new WildcardQuery(new Term("base_car_style", "298*")), Occur.SHOULD);
				last_query.add(new WildcardQuery(new Term("base_car_NO", "298*")), Occur.SHOULD);
				last_query.add(new WildcardQuery(new Term("year", "298*")), Occur.SHOULD);
				query.add(last_query, Occur.MUST);
			}
			assertTrue(q.equals(query));
			
		}
	}
	
	@Test
	public void testNoLastQueryParser() throws Exception {
		{
			SolrQueryRequest req = req();
			QParser parser = null;
			String qstr = "16款奥迪A4l";
			try {
				parser = QParser.getParser(qstr, "basecarfixparser", req);
			} catch (Exception e) {
				throw new RuntimeException("getParser excep using defType=" + "basecarfixparser with qstr=" + qstr, e);
			}
			Query q = parser.parse();
			assertNotNull("expected query", q);
			BooleanQuery query = new BooleanQuery();			
			{
				BooleanQuery char_query = new BooleanQuery();
				char_query.add(new TermQuery(new Term("base_car_style", "奥")), Occur.SHOULD);
				char_query.add(new TermQuery(new Term("base_car_style", "迪")), Occur.SHOULD);
				char_query.add(new TermQuery(new Term("base_car_style", "款奥")), Occur.SHOULD);
				char_query.add(new TermQuery(new Term("base_car_style", "奥迪")), Occur.SHOULD);
				char_query.add(new TermQuery(new Term("base_car_style", "款奥迪")), Occur.SHOULD);
				query.add(char_query, Occur.MUST);
			}
			{
				BooleanQuery num_query = new BooleanQuery();
				num_query.add(new TermQuery(new Term("base_car_style", "16")), Occur.SHOULD);
				num_query.add(new TermQuery(new Term("base_car_NO", "16")), Occur.SHOULD);
				num_query.add(new TermQuery(new Term("year", "16")), Occur.SHOULD);
				query.add(num_query, Occur.MUST);
			}
			{
				BooleanQuery last_query = new BooleanQuery();
				last_query.add(new WildcardQuery(new Term("base_car_style", "a4l*")), Occur.SHOULD);
				query.add(last_query, Occur.MUST);
			}
			assertTrue(q.equals(query));
		}
		
		{
			SolrQueryRequest req = req();
			QParser parser = null;
			String qstr = "tfsi";
			try {
				parser = QParser.getParser(qstr, "basecarfixparser", req);
			} catch (Exception e) {
				throw new RuntimeException("getParser excep using defType=" + "basecarfixparser with qstr=" + qstr, e);
			}
			Query q = parser.parse();
			assertNotNull("expected query", q);
			//wildCardQuery的disableCoord默认要设置为true，因为query只有1个，但是有可能命中多个term，那岂不是coord>1了。。
			BooleanQuery query = new BooleanQuery(true);
			{
				BooleanQuery last_query = new BooleanQuery();
				last_query.add(new WildcardQuery(new Term("base_car_style", "tfsi*")), Occur.SHOULD);
				query.add(last_query, Occur.MUST);
			}
			assertTrue(q.equals(query));
		}
	}
}
