package com.niuniu.queryparser;

import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.BooleanClause.Occur;
import org.apache.solr.SolrTestCaseJ4;
import org.apache.solr.request.SolrQueryRequest;
import org.apache.solr.search.QParser;
import org.junit.BeforeClass;
import org.junit.Test;

public class TestNiuniuBasecarParser extends SolrTestCaseJ4 {
	@BeforeClass
	public static void beforeClass() throws Exception {
		initCore("solrconfig-basecar.xml", "schema-basecar.xml");
	}

	@Test
	public void testQueryParser() throws Exception {
		{
			SolrQueryRequest req = req();
			QParser parser = null;
			String qstr = "RX450H白车棕内,黄水晶棕内869";
			try {
				parser = QParser.getParser(qstr, "basecarparser", req);
			} catch (Exception e) {
				throw new RuntimeException("getParser excep using defType=" + "niuniuparser with qstr=" + qstr, e);
			}
			Query q = parser.parse();
			assertNotNull("expected query", q);
			BooleanQuery query = new BooleanQuery();
			
			{
				BooleanQuery price_query = new BooleanQuery();
				Query tmp = new TermQuery(new Term("base_car_NO", "869"));
				tmp.setBoost(0.3f);
				price_query.add(tmp, Occur.SHOULD);
				
				tmp = new TermQuery(new Term("base_car_style", "869"));
				tmp.setBoost(0.2f);
				price_query.add(tmp, Occur.SHOULD);
				
				query.add(price_query, Occur.MUST);
			}
			
			{
				Query tmp = new TermQuery(new Term("car_model_name", "rx"));
				tmp.setBoost(0.5f);
				BooleanQuery model_query = new BooleanQuery();
				model_query.add(tmp, Occur.MUST);
				query.add(model_query, Occur.MUST);
			}
			
			{
				Query tmp = new TermQuery(new Term("base_car_style", "450h"));
				tmp.setBoost(0.2f);
				BooleanQuery model_query = new BooleanQuery();
				model_query.add(tmp, Occur.SHOULD);
				query.add(model_query, Occur.MUST);
			}
			
			assertTrue(q.equals(query));
		}
		
		{
			SolrQueryRequest req = req();
			QParser parser = null;
			String qstr = "";
			try {
				parser = QParser.getParser(qstr, "basecarparser", req);
			} catch (Exception e) {
				throw new RuntimeException("getParser excep using defType=" + "niuniuparser with qstr=" + qstr, e);
			}
			Query q = parser.parse();
			assertNull("expected query", q);
		}
		
		{
			SolrQueryRequest req = req();
			QParser parser = null;
			String qstr = "白黑";
			try {
				parser = QParser.getParser(qstr, "basecarparser", req);
			} catch (Exception e) {
				throw new RuntimeException("getParser excep using defType=" + "niuniuparser with qstr=" + qstr, e);
			}
			Query q = parser.parse();
			assertNull("expected query", q);
		}
		
		{
			SolrQueryRequest req = req();
			QParser parser = null;
			String qstr = "1.奔驰";
			try {
				parser = QParser.getParser(qstr, "basecarparser", req);
			} catch (Exception e) {
				throw new RuntimeException("getParser excep using defType=" + "niuniuparser with qstr=" + qstr, e);
			}
			Query q = parser.parse();
			assertNotNull("expected query", q);
			
			BooleanQuery query = new BooleanQuery();
			{
				BooleanQuery brand_query = new BooleanQuery();
				Query tmp = new TermQuery(new Term("brand_name", "奔驰"));
				brand_query.add(tmp, Occur.SHOULD);
				query.add(brand_query, Occur.MUST);
			}
			
			assertEquals(query, q);
		}
	}
}
