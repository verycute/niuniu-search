package com.niuniu.queryparser;

import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.WildcardQuery;
import org.apache.lucene.search.BooleanClause.Occur;
import org.apache.solr.SolrTestCaseJ4;
import org.apache.solr.common.params.SolrParams;
import org.apache.solr.common.util.NamedList;
import org.apache.solr.request.SolrQueryRequest;
import org.apache.solr.search.QParser;
import org.junit.BeforeClass;
import org.junit.Test;

public class TestNiuniuQueryParser extends SolrTestCaseJ4 {
	@BeforeClass
	public static void beforeClass() throws Exception {
		initCore("solrconfig-niuniu.xml", "schema-niuniu.xml");
	}

	@Test
	public void testQueryParser() throws Exception {
		{
			SolrQueryRequest req = req();
			QParser parser = null;
			String qstr = "奔驰c200";
			try {
				parser = QParser.getParser(qstr, "niuniuparser", req);
			} catch (Exception e) {
				throw new RuntimeException("getParser excep using defType=" + "niuniuparser with qstr=" + qstr, e);
			}
			Query q = parser.parse();
			assertNotNull("expected query", q);
			BooleanQuery query = new BooleanQuery();
			{
				BooleanQuery brand_query = new BooleanQuery();
				brand_query.add(new TermQuery(new Term("brand_name", "奔驰")), Occur.SHOULD);
				query.add(brand_query, Occur.MUST);
			}
			{
				Query tmp = new TermQuery(new Term("base_car_style", "c200"));
				tmp.setBoost(0.5f);
				BooleanQuery model_query = new BooleanQuery();
				model_query.add(tmp, Occur.SHOULD);
				query.add(model_query, Occur.MUST);
			}
			assertTrue(q.equals(query));
		}
		
		{
			SolrQueryRequest req = req();
			QParser parser = null;
			String qstr = "奔驰c200 3538";
			try {
				parser = QParser.getParser(qstr, "niuniuparser", req);
			} catch (Exception e) {
				throw new RuntimeException("getParser excep using defType=" + "niuniuparser with qstr=" + qstr, e);
			}
			Query q = parser.parse();
			assertNotNull("expected query", q);
			BooleanQuery query = new BooleanQuery();
			{
				BooleanQuery price_query = new BooleanQuery();
				Query tmp = new TermQuery(new Term("base_car_NO", "3538"));
				tmp.setBoost(0.5f);
				price_query.add(tmp, Occur.SHOULD);
				
				tmp = new TermQuery(new Term("last_price_NO", "3538"));
				tmp.setBoost(0.5f);
				price_query.add(tmp, Occur.SHOULD);
				
				tmp = new TermQuery(new Term("parent_price_NO", "3538"));
				tmp.setBoost(0.5f);
				price_query.add(tmp, Occur.SHOULD);
				
				tmp = new TermQuery(new Term("base_car_style", "3538"));
				tmp.setBoost(0.5f);
				price_query.add(tmp, Occur.SHOULD);
				
				query.add(price_query, Occur.MUST);
			}
			{
				BooleanQuery brand_query = new BooleanQuery();
				brand_query.add(new TermQuery(new Term("brand_name", "奔驰")), Occur.SHOULD);
				query.add(brand_query, Occur.MUST);
			}
			{
				Query tmp = new TermQuery(new Term("base_car_style", "c200"));
				tmp.setBoost(0.5f);
				BooleanQuery model_query = new BooleanQuery();
				model_query.add(tmp, Occur.SHOULD);
				query.add(model_query, Occur.MUST);
			}
			
			assertTrue(q.equals(query));
		}

		{
			SolrQueryRequest req = req();
			QParser parser = null;
			String qstr = "17款帕萨特大众手动";
			try {
				parser = QParser.getParser(qstr, "niuniuparser", req);
			} catch (Exception e) {
				throw new RuntimeException("getParser excep using defType=" + "niuniuparser with qstr=" + qstr, e);
			}
			Query q = parser.parse();
			assertNotNull("expected query", q);
			BooleanQuery query = new BooleanQuery();
			{
				BooleanQuery brand_query = new BooleanQuery();
				brand_query.add(new TermQuery(new Term("brand_name", "大众")), Occur.SHOULD);
				query.add(brand_query, Occur.MUST);
			}
			{
				BooleanQuery model_query = new BooleanQuery();
				model_query.add(new TermQuery(new Term("car_model_name", "帕萨特")), Occur.MUST);
				query.add(model_query, Occur.MUST);
			}
			{
				Query tmp = new TermQuery(new Term("base_car_style", "手动"));
				tmp.setBoost(0.5f);
				BooleanQuery model_query = new BooleanQuery();
				model_query.add(tmp, Occur.SHOULD);
				query.add(model_query, Occur.MUST);
			}
			{
				Query tmp = new TermQuery(new Term("base_car_year_info", "17款"));
				tmp.setBoost(0.1f);
				BooleanQuery year_query = new BooleanQuery();
				year_query.add(tmp, Occur.SHOULD);
				query.add(year_query, Occur.MUST);
			}
			assertTrue(q.equals(query));
		}
		
		{
			SolrQueryRequest req = req();
			QParser parser = null;
			String qstr = "奔驰c200红色";
			try {
				parser = QParser.getParser(qstr, "niuniuparser", req);
			} catch (Exception e) {
				throw new RuntimeException("getParser excep using defType=" + "niuniuparser with qstr=" + qstr, e);
			}
			Query q = parser.parse();
			assertNotNull("expected query", q);
			BooleanQuery query = new BooleanQuery();
			{
				BooleanQuery brand_query = new BooleanQuery();
				brand_query.add(new TermQuery(new Term("brand_name", "奔驰")), Occur.SHOULD);
				query.add(brand_query, Occur.MUST);
			}
			{
				Query tmp = new TermQuery(new Term("base_car_style", "c200"));
				tmp.setBoost(0.5f);
				BooleanQuery model_query = new BooleanQuery();
				model_query.add(tmp, Occur.SHOULD);
				query.add(model_query, Occur.MUST);
			}
			{
				Query tmp = new TermQuery(new Term("base_car_style", "红色"));
				tmp.setBoost(0.01f);
				BooleanQuery model_query = new BooleanQuery();
				model_query.add(tmp, Occur.SHOULD);
				
				tmp = new TermQuery(new Term("remark", "红色"));
				tmp.setBoost(0.001f);
				model_query.add(tmp, Occur.SHOULD);
				
				tmp = new TermQuery(new Term("configuration_remark", "红色"));
				tmp.setBoost(0.001f);
				model_query.add(tmp, Occur.SHOULD);
				
				QParser parser2 = QParser.getParser("outer_color:红色 OR inner_color:红色","lucene", req);
				Query cq = parser2.getQuery();
				cq.setBoost(0.02f);
				model_query.add(cq, Occur.SHOULD);
				
				query.add(model_query, Occur.MUST);
			}
			assertTrue(q.equals(query));
		}
	}

	@Test
	public void testQueryParserIllegalQuery() throws Exception {
		{
			SolrQueryRequest req = req();
			QParser parser = null;
			String qstr = "";
			try {
				parser = QParser.getParser(qstr, "niuniuparser", req);
			} catch (Exception e) {
				throw new RuntimeException("getParser excep using defType=" + "niuniuparser with qstr=" + qstr, e);
			}
			Query q = parser.parse();
			assertNull("expected query is null", q);
		}
	}

	@Test
	public void testQueryParserDuplicateToken() throws Exception {
		{
			SolrQueryRequest req = req();
			QParser parser = null;
			String qstr = "宝马宝马宝马";
			try {
				parser = QParser.getParser(qstr, "niuniuparser", req);
			} catch (Exception e) {
				throw new RuntimeException("getParser excep using defType=" + "niuniuparser with qstr=" + qstr, e);
			}
			Query q = parser.parse();
			assertNotNull("expected query", q);
			BooleanQuery query = new BooleanQuery();
			{
				BooleanQuery brand_query = new BooleanQuery();
				brand_query.add(new TermQuery(new Term("brand_name", "宝马")), Occur.SHOULD);
				query.add(brand_query, Occur.MUST);
			}
			assertTrue(q.equals(query));
		}

		{
			SolrQueryRequest req = req();
			QParser parser = null;
			String qstr = "2017款 17款帕萨特大众手动";
			try {
				parser = QParser.getParser(qstr, "niuniuparser", req);
			} catch (Exception e) {
				throw new RuntimeException("getParser excep using defType=" + "niuniuparser with qstr=" + qstr, e);
			}
			Query q = parser.parse();
			assertNotNull("expected query", q);
			BooleanQuery query = new BooleanQuery();
			{
				BooleanQuery brand_query = new BooleanQuery();
				brand_query.add(new TermQuery(new Term("brand_name", "大众")), Occur.SHOULD);
				query.add(brand_query, Occur.MUST);
			}
			{
				BooleanQuery model_query = new BooleanQuery();
				model_query.add(new TermQuery(new Term("car_model_name", "帕萨特")), Occur.MUST);
				query.add(model_query, Occur.MUST);
			}
			{
				Query tmp = new TermQuery(new Term("base_car_style", "手动"));
				tmp.setBoost(0.5f);
				BooleanQuery model_query = new BooleanQuery();
				model_query.add(tmp, Occur.SHOULD);
				query.add(model_query, Occur.MUST);
			}
			{
				Query tmp = new TermQuery(new Term("base_car_year_info", "17款"));
				tmp.setBoost(0.1f);
				BooleanQuery year_query = new BooleanQuery();
				year_query.add(tmp, Occur.SHOULD);
				query.add(year_query, Occur.MUST);
			}
			assertTrue(q.equals(query));
		}

		{
			SolrQueryRequest req = req();
			QParser parser = null;
			String qstr = "美规奥迪A4";
			try {
				parser = QParser.getParser(qstr, "niuniuparser", req);
			} catch (Exception e) {
				throw new RuntimeException("getParser excep using defType=" + "niuniuparser with qstr=" + qstr, e);
			}
			Query q = parser.parse();
			assertNotNull("expected query", q);
			BooleanQuery query = new BooleanQuery();
			{
				BooleanQuery brand_query = new BooleanQuery();
				brand_query.add(new TermQuery(new Term("brand_name", "奥迪")), Occur.SHOULD);
				query.add(brand_query, Occur.MUST);
			}
			{
				BooleanQuery standard_query = new BooleanQuery();
				standard_query.add(new TermQuery(new Term("standard_name", "美规")), Occur.SHOULD);
				query.add(standard_query, Occur.MUST);
			}
			{
				BooleanQuery model_query = new BooleanQuery();
				model_query.add(new TermQuery(new Term("car_model_name", "a4")), Occur.SHOULD);
				model_query.add(new TermQuery(new Term("car_model_name", "a4l")), Occur.SHOULD);
				query.add(model_query, Occur.MUST);
			}
			assertTrue(q.equals(query));
		}
	}

	@Test
	public void testQueryParserWildcardQuery() throws Exception {
		{
			SolrQueryRequest req = req();
			QParser parser = null;
			String qstr = "*:*";
			try {
				parser = QParser.getParser(qstr, "niuniuparser", req);
			} catch (Exception e) {
				throw new RuntimeException("getParser excep using defType=" + "niuniuparser with qstr=" + qstr, e);
			}
			Query q = parser.parse();
			assertNotNull("expected query", q);
			Query expected = new WildcardQuery(new Term(req.getSchema().getDefaultSearchFieldName(), "*"));
			assertTrue(expected.equals(q));
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Test
	public void testQueryParserLevel() throws Exception {
		{
			NamedList params = new NamedList<>();
			params.add("search_level", "low");
			SolrParams solrParams = SolrParams.toSolrParams(params);
			SolrQueryRequest req = req();
			req.setParams(solrParams);
			QParser parser = null;
			String qstr = "奔驰c200";
			try {
				parser = QParser.getParser(qstr, "niuniuparser", req);
			} catch (Exception e) {
				throw new RuntimeException("getParser excep using defType=" + "niuniuparser with qstr=" + qstr, e);
			}
			Query q = parser.parse();
			assertNotNull("expected query", q);
			BooleanQuery query = new BooleanQuery();
			{
				BooleanQuery brand_query = new BooleanQuery();
				brand_query.add(new TermQuery(new Term("brand_name", "奔驰")), Occur.SHOULD);
				query.add(brand_query, Occur.MUST);
			}
			{
				Query tmp = new TermQuery(new Term("base_car_style", "c200"));
				tmp.setBoost(0.5f);
				BooleanQuery model_query = new BooleanQuery();
				model_query.add(tmp, Occur.SHOULD);
				query.add(model_query, Occur.SHOULD);
			}
			assertTrue(q.equals(query));
		}
		{
			NamedList params = new NamedList<>();
			params.add("search_level", "high");
			SolrParams solrParams = SolrParams.toSolrParams(params);
			SolrQueryRequest req = req();
			req.setParams(solrParams);
			QParser parser = null;
			String qstr = "奔驰c200";
			try {
				parser = QParser.getParser(qstr, "niuniuparser", req);
			} catch (Exception e) {
				throw new RuntimeException("getParser excep using defType=" + "niuniuparser with qstr=" + qstr, e);
			}
			Query q = parser.parse();
			assertNotNull("expected query", q);
			BooleanQuery query = new BooleanQuery();
			{
				BooleanQuery brand_query = new BooleanQuery();
				brand_query.add(new TermQuery(new Term("brand_name", "奔驰")), Occur.SHOULD);
				query.add(brand_query, Occur.MUST);
			}
			{
				Query tmp = new TermQuery(new Term("base_car_style", "c200"));
				tmp.setBoost(0.5f);
				BooleanQuery model_query = new BooleanQuery();
				model_query.add(tmp, Occur.SHOULD);
				query.add(model_query, Occur.SHOULD);
			}
			assertFalse(q.equals(query));
		}
	}

	@Test
	public void testQueryParserSynonym() throws Exception {
		{
			SolrQueryRequest req = req();
			QParser parser = null;
			String qstr = "奥迪A4";
			try {
				parser = QParser.getParser(qstr, "niuniuparser", req);
			} catch (Exception e) {
				throw new RuntimeException("getParser excep using defType=" + "niuniuparser with qstr=" + qstr, e);
			}
			Query q = parser.parse();
			assertNotNull("expected query", q);
			BooleanQuery query = new BooleanQuery();
			{
				BooleanQuery brand_query = new BooleanQuery();
				brand_query.add(new TermQuery(new Term("brand_name", "奥迪")), Occur.SHOULD);
				query.add(brand_query, Occur.MUST);
			}
			{
				Query tmp = new TermQuery(new Term("car_model_name", "a4"));
				BooleanQuery model_query = new BooleanQuery();
				model_query.add(tmp, Occur.SHOULD);
				tmp = new TermQuery(new Term("car_model_name", "a4l"));
				model_query.add(tmp, Occur.SHOULD);
				query.add(model_query, Occur.MUST);
			}
			assertTrue(q.equals(query));
		}

		{
			SolrQueryRequest req = req();
			QParser parser = null;
			String qstr = "路虎揽运 揽胜";
			try {
				parser = QParser.getParser(qstr, "niuniuparser", req);
			} catch (Exception e) {
				throw new RuntimeException("getParser excep using defType=" + "niuniuparser with qstr=" + qstr, e);
			}
			Query q = parser.parse();
			assertNotNull("expected query", q);
			BooleanQuery query = new BooleanQuery();
			{
				BooleanQuery brand_query = new BooleanQuery();
				brand_query.add(new TermQuery(new Term("brand_name", "路虎")), Occur.SHOULD);
				query.add(brand_query, Occur.MUST);
			}
			{
				Query tmp = new TermQuery(new Term("car_model_name", "揽胜"));
				BooleanQuery model_query = new BooleanQuery();
				model_query.add(tmp, Occur.MUST);
				tmp = new TermQuery(new Term("car_model_name", "运动"));
				model_query.add(tmp, Occur.MUST);
				query.add(model_query, Occur.MUST);
			}
			assertTrue(q.equals(query));
		}
	}

	@SuppressWarnings("rawtypes")
	@Test
	public void testQuerySpecialCharacter() throws Exception {
		{
			NamedList params = new NamedList<>();
			SolrParams solrParams = SolrParams.toSolrParams(params);
			SolrQueryRequest req = req();
			req.setParams(solrParams);
			QParser parser = null;
			String qstr = "奔驰c级";
			try {
				parser = QParser.getParser(qstr, "niuniuparser", req);
			} catch (Exception e) {
				throw new RuntimeException("getParser excep using defType=" + "niuniuparser with qstr=" + qstr, e);
			}
			Query q = parser.parse();
			assertNotNull("expected query", q);
			BooleanQuery query = new BooleanQuery();
			{
				BooleanQuery brand_query = new BooleanQuery();
				brand_query.add(new TermQuery(new Term("brand_name", "奔驰")), Occur.SHOULD);
				query.add(brand_query, Occur.MUST);
			}
			{
				Query tmp = new TermQuery(new Term("car_model_name", "c"));
				BooleanQuery model_query = new BooleanQuery();
				model_query.add(tmp, Occur.MUST);
				query.add(model_query, Occur.MUST);
			}
			assertTrue(q.equals(query));
		}

		{
			NamedList params = new NamedList<>();
			SolrParams solrParams = SolrParams.toSolrParams(params);
			SolrQueryRequest req = req();
			req.setParams(solrParams);
			QParser parser = null;
			String qstr = "奔驰s级";
			try {
				parser = QParser.getParser(qstr, "niuniuparser", req);
			} catch (Exception e) {
				throw new RuntimeException("getParser excep using defType=" + "niuniuparser with qstr=" + qstr, e);
			}
			Query q = parser.parse();
			assertNotNull("expected query", q);
			BooleanQuery query = new BooleanQuery();
			{
				BooleanQuery model_style_query = new BooleanQuery();
				Query tmp = new TermQuery(new Term("base_car_style", "s"));
				tmp.setBoost(0.5f);
				model_style_query.add(tmp, Occur.SHOULD);
				tmp = new TermQuery(new Term("car_model_name", "s"));
				model_style_query.add(tmp, Occur.SHOULD);
				query.add(model_style_query, Occur.MUST);
			}
			{
				BooleanQuery brand_query = new BooleanQuery();
				brand_query.add(new TermQuery(new Term("brand_name", "奔驰")), Occur.SHOULD);
				query.add(brand_query, Occur.MUST);
			}
			assertTrue(q.equals(query));
		}
	}
	
	@SuppressWarnings("rawtypes")
	@Test
	public void testQueryNewBasecarArchitecture() throws Exception {
		{
			NamedList params = new NamedList<>();
			SolrParams solrParams = SolrParams.toSolrParams(params);
			SolrQueryRequest req = req();
			req.setParams(solrParams);
			QParser parser = null;
			String qstr = "标致408 1297";
			try {
				parser = QParser.getParser(qstr, "niuniuparser", req);
			} catch (Exception e) {
				throw new RuntimeException("getParser excep using defType=" + "niuniuparser with qstr=" + qstr, e);
			}
			Query q = parser.parse();
			assertNotNull("expected query", q);
			BooleanQuery query = new BooleanQuery();
			{
				BooleanQuery price_query = new BooleanQuery();
				Query tmp = new TermQuery(new Term("base_car_NO", "408"));
				price_query.add(tmp, Occur.SHOULD);
				
				tmp = new TermQuery(new Term("last_price_NO", "408"));
				price_query.add(tmp, Occur.SHOULD);
				
				tmp = new TermQuery(new Term("parent_price_NO", "408"));
				price_query.add(tmp, Occur.SHOULD);
				
				tmp = new TermQuery(new Term("car_model_name", "408"));
				price_query.add(tmp, Occur.SHOULD);
				
				query.add(price_query, Occur.MUST);
			}
			{
				BooleanQuery price_query = new BooleanQuery();
				Query tmp = new TermQuery(new Term("base_car_NO", "1297"));
				tmp.setBoost(0.5f);
				price_query.add(tmp, Occur.SHOULD);
				
				tmp = new TermQuery(new Term("last_price_NO", "1297"));
				tmp.setBoost(0.5f);
				price_query.add(tmp, Occur.SHOULD);
				
				tmp = new TermQuery(new Term("parent_price_NO", "1297"));
				tmp.setBoost(0.5f);
				price_query.add(tmp, Occur.SHOULD);
				
				tmp = new TermQuery(new Term("base_car_style", "1297"));
				tmp.setBoost(0.5f);
				price_query.add(tmp, Occur.SHOULD);
				
				query.add(price_query, Occur.MUST);
			}
			{
				BooleanQuery brand_query = new BooleanQuery();
				brand_query.add(new TermQuery(new Term("brand_name", "标致")), Occur.SHOULD);
				query.add(brand_query, Occur.MUST);
			}
			
			assertTrue(q.equals(query));
		}
	}
	
	@SuppressWarnings("rawtypes")
	@Test
	public void testQuerySpecialHeader() throws Exception {
		{
			NamedList params = new NamedList<>();
			SolrParams solrParams = SolrParams.toSolrParams(params);
			SolrQueryRequest req = req();
			req.setParams(solrParams);
			QParser parser = null;
			String qstr = "1.奔驰c级";
			try {
				parser = QParser.getParser(qstr, "niuniuparser", req);
			} catch (Exception e) {
				throw new RuntimeException("getParser excep using defType=" + "niuniuparser with qstr=" + qstr, e);
			}
			Query q = parser.parse();
			assertNotNull("expected query", q);
			BooleanQuery query = new BooleanQuery();
			{
				BooleanQuery brand_query = new BooleanQuery();
				brand_query.add(new TermQuery(new Term("brand_name", "奔驰")), Occur.SHOULD);
				query.add(brand_query, Occur.MUST);
			}
			{
				Query tmp = new TermQuery(new Term("car_model_name", "c"));
				BooleanQuery model_query = new BooleanQuery();
				model_query.add(tmp, Occur.MUST);
				query.add(model_query, Occur.MUST);
			}
			assertTrue(q.equals(query));
		}
	}
	
	@SuppressWarnings("rawtypes")
	@Test
	public void testQueryWithDash() throws Exception {
		{
			NamedList params = new NamedList<>();
			SolrParams solrParams = SolrParams.toSolrParams(params);
			SolrQueryRequest req = req();
			req.setParams(solrParams);
			QParser parser = null;
			String qstr = "雪铁龙C3-XR";
			try {
				parser = QParser.getParser(qstr, "niuniuparser", req);
			} catch (Exception e) {
				throw new RuntimeException("getParser excep using defType=" + "niuniuparser with qstr=" + qstr, e);
			}
			Query q = parser.parse();
			assertNotNull("expected query", q);
			BooleanQuery query = new BooleanQuery();
			{
				BooleanQuery brand_query = new BooleanQuery();
				brand_query.add(new TermQuery(new Term("brand_name", "雪铁龙")), Occur.SHOULD);
				query.add(brand_query, Occur.MUST);
			}
			{
				BooleanQuery model_query = new BooleanQuery();
				Query tmp = new TermQuery(new Term("car_model_name", "c3"));
				model_query.add(tmp, Occur.MUST);
				tmp = new TermQuery(new Term("car_model_name", "c3xr"));
				model_query.add(tmp, Occur.MUST);
				query.add(model_query, Occur.MUST);
			}
			assertTrue(q.equals(query));
		}
	}
}
