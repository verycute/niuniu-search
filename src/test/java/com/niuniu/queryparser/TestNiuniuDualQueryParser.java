package com.niuniu.queryparser;

import org.apache.lucene.search.Query;
import org.apache.solr.SolrTestCaseJ4;
import org.apache.solr.request.SolrQueryRequest;
import org.apache.solr.search.QParser;
import org.junit.BeforeClass;
import org.junit.Test;

public class TestNiuniuDualQueryParser extends SolrTestCaseJ4{
	@BeforeClass
	public static void beforeClass() throws Exception {
		initCore("solrconfig-niuniu.xml", "schema-niuniu.xml");
	}
	
	@Test
	public void testQueryParser() throws Exception {
		{
			SolrQueryRequest req = req();
			QParser parser = null;
			String qstr = "奔驰斯玛特 OR smart";
			try {
				parser = QParser.getParser(qstr, "macroparser", req);
			} catch (Exception e) {
				throw new RuntimeException("getParser excep using defType=" + "macroparser with qstr=" + qstr, e);
			}
			Query q = parser.parse();
			assertNotNull("expected query", q);
		}
	}
}
