package com.niuniu.update.plugin;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.solr.SolrTestCaseJ4;
import org.apache.solr.common.SolrInputDocument;
import org.apache.solr.common.SolrInputField;
import org.apache.solr.common.params.ModifiableSolrParams;
import org.apache.solr.common.params.MultiMapSolrParams;
import org.apache.solr.common.params.SolrParams;
import org.apache.solr.common.params.UpdateParams;
import org.apache.solr.common.util.ContentStream;
import org.apache.solr.common.util.ContentStreamBase;
import org.apache.solr.core.SolrCore;
import org.apache.solr.handler.UpdateRequestHandler;
import org.apache.solr.request.LocalSolrQueryRequest;
import org.apache.solr.request.SolrQueryRequest;
import org.apache.solr.request.SolrQueryRequestBase;
import org.apache.solr.request.SolrRequestInfo;
import org.apache.solr.response.SolrQueryResponse;
import org.apache.solr.update.AddUpdateCommand;
import org.apache.solr.update.processor.UpdateRequestProcessor;
import org.apache.solr.update.processor.UpdateRequestProcessorChain;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import org.apache.lucene.util.LuceneTestCase.SuppressCodecs;

@SuppressCodecs({"Lucene3x","Lucene41", "Lucene40","Appending"})
public class TestNiuniuProcessor extends SolrTestCaseJ4{
	@BeforeClass
	public static void beforeClass() throws Exception {
		initCore("solrconfig-niuniu.xml", "schema-niuniu.xml");
	}

	@Override
	@Before
	public void setUp() throws Exception {
		super.setUp();
		clearIndex();
		assertU(commit());
	}
	
	@Test
	public void testNiuniuProcessorWork() throws Exception {
		SolrCore core = h.getCore();
		UpdateRequestProcessorChain chained = core.getUpdateProcessingChain("niuniu");
		assertNotNull(chained);

		addDoc(adoc("id", "254823", 
	                "post_status", "1", 
	                "base_car_NO", "848", 
	                "inner_color", "米色",
	                "outer_color", "风雅金",
	                "user_id", "37947",
	                "company_owner_id", null,
	                "user_name", "段磊",
	                "firm_name", "上汽大众",
	                "post_type", "0",
	                "guiding_price", "8.48",
	                "resource_type", "0",
	                "standard", "国产",
	                "base_car_year", "2015",
	                "configuration_remark", null,
	                "base_car_status", "1",
	                "base_car_style", "330TSI 自动舒适版",
	                "engine_capacity", "1.8T",
	                "brand", "大众",
	                "car_model_name", "朗逸",
	                "user_city_name", "上海",
	                "user_prov_name", "上海",
	                "remark", "呵呵哒",
	                "user_vip_level", "4",
	                "created_at", "2017-06-20T23:06:46.781541Z",
	                "updated_at", "2017-06-20T23:06:46.803441Z",
	                "maintained_at", "2017-06-20T15:06:46.780828Z",
	                "expired_at", "2017-07-20T23:06:46.78107Z",
	                "user_created_at", "2016-05-17T22:04:58.398932Z",
	                "expect_price", "11.19",
	                "discount_way", "2",
	                "discount_content", "2.50",
	                "user_level", "3",
	                "post_margin", "1000",
	                "base_price", "8.48",
	                "aasm_state", null,
	                "source_prov_name", null,
	                "short_name", null,
	                "photo_image", "3fb435b2c7.jpg",
	                "non_member_price", null,
	                "configuration_price", "0.00",
	                "frame_number", null,
	                "standard_id", "1",
	                "car_model_status", "1",
	                "niuniu_search_resource_orders", "0",
	                "brand_id", "15",
	                "brand_pinyin", "da zhong",
	                "car_model_id", "99",
	                "base_car_id", "9942",
	                "car_in_area", "东区",
	                "take_car_area", "东区",
	                "ticket_pack_time", "手续3-7天",
	                "complete_order_count", "42",
	                "is_vip_resource", "0",
	                "is_batch_resource", "0",
	                "niuniu_search_is_unqualified_seller", "0",
	                "niuniu_search_company_id", "2205"
	                ));
	    assertU(commit());
	    checkNumDocs(1);
	}
	
	@Test
	public void testNiuniuProcessorCorrect() throws Exception {
		{
			SolrInputDocument d = null;
	
		    // get all defaults
		    d = processAdd("niuniu",doc(f("id", "254823"),
	                f("post_status", "1"),
	                f("base_car_NO", "15"),
	                f("inner_color", "米色"),
	                f("outer_color", "风雅金"),
	                f("user_id", "37947"),
	                f("company_owner_id", ""),
	                f("user_name", "段磊"),
	                f("firm_name", "上汽大众"),
	                f("post_type", "0"),
	                f("guiding_price", "15.00"),
	                f("resource_type", "0"),
	                f("standard", "国产"),
	                f("base_car_year", "2015"),
	                f("configuration_remark", ""),
	                f("base_car_status", "1"),
	                f("base_car_style", "1.6L 自动舒适版"),
	                f("engine_capacity", "1.8T"),
	                f("brand", "大众"),
	                f("car_model_name", "朗逸"),
	                f("user_city_name", "上海"),
	                f("user_prov_name", "上海"),
	                f("remark", "呵呵哒"),
	                f("user_vip_level", "4"),
	                f("created_at", "2017-09-27T08:05:54.325123Z"),
	                f("updated_at", "2017-09-27T08:05:54.325123Z"),
	                f("maintained_at", "2017-09-27T08:05:54.325123Z"),
	                f("expired_at", "2017-07-20T07:06:46.78107Z"),
	                f("user_created_at", "2016-05-17T14:04:58.398932Z"),
	                f("expect_price", "12.50"),
	                f("discount_way", "2"),
	                f("discount_content", "2.50"),
	                f("user_level", "3"),
	                f("post_margin", "1000"),
	                f("base_price", "15.00"),
	                f("aasm_state", ""),
	                f("source_prov_name", ""),
	                f("short_name", ""),
	                f("photo_image", "3fb435b2c7.jpg"),
	                f("non_member_price", ""),
	                f("configuration_price", "0.00"),
	                f("frame_number", ""),
	                f("standard_id", "1"),
	                f("car_model_status", "1"),
	                f("niuniu_search_resource_orders", "0"),
	                f("brand_id", "15"),
	                f("brand_pinyin", "da zhong"),
	                f("car_model_id", "99"),
	                f("base_car_id", "9942"),
	                f("car_in_area", "东区"),
	                f("take_car_area", "东区"),
	                f("ticket_pack_time", "手续3-7天"),
	                f("complete_order_count", "42"),
	                f("is_vip_resource", "0"),
	                f("is_batch_resource", "0"),
	                f("niuniu_search_is_unqualified_seller", "0"),
	                f("niuniu_search_company_id", "2205")));
		    
		    assertEquals("备注: 呵呵哒", d.getFieldValue("remark"));
		    assertEquals("2017-09-27T08:05:54.325Z", d.getFieldValue("created_at"));
		    assertEquals("2017-09-27T08:05:54.325Z", d.getFieldValue("updated_at"));
		    //assertEquals(1506528354325l, d.getFieldValue("created_at_ms"));
		    assertEquals(1506499554325l, d.getFieldValue("created_at_ms"));
		    assertEquals("段磊(上海)", d.getFieldValue("user_name"));
		    assertEquals("认证综展", d.getFieldValue("user_level_name"));
		    assertEquals("朗逸 15款 1.6L 自动舒适版", d.getFieldValue("title"));
		    assertEquals("指导价: 15万/下2.5万", d.getFieldValue("info_price"));
		    assertEquals("1 % 1.6L 自动舒适版 % 15.00万", d.getFieldValue("base_car_style_full_name_s"));
		    assertEquals("da zhong,1,上汽大众", d.getFieldValue("brand_firm_standard_s"));
		    assertEquals("风雅金/米色|手续3-7天|销东区|车在东区", d.getFieldValue("extend_info"));
		    assertEquals("|成交量:42", d.getFieldValue("user_complete_order_text"));
		    assertEquals("指导价: 15万/下16.67点", d.getFieldValue("info_price_percentage"));
		    assertEquals("指导价: 15万/下2.5万", d.getFieldValue("info_price_amount"));
		    assertEquals("现车", d.getFieldValue("resource_type_s"));
		    assertEquals("国产/现车", d.getFieldValue("subtitle"));
		    assertEquals("15款 1.6L 自动舒适版 1.8T", d.getFieldValue("base_car_style"));
		    assertEquals("3", d.getFieldValue("days_diff_level"));
		    
		    Collection<Object> content = d.getFieldValues("base_car_year_info");
		    ArrayList<String> expected = new ArrayList<>();
		    expected.add("15款");
		    expected.add("2015款");
		    assertEquals(expected, content);
		    
		    assertEquals(5, d.getFieldValue("niuniu_search_order_score"));
		    assertEquals(2205, d.getFieldValue("niuniu_search_company_id"));
		    assertEquals(0, d.getFieldValue("niuniu_search_is_unqualified_seller"));
		    assertEquals("省份未填", d.getFieldValue("filter_car_in_area"));
		}
		{
			SolrInputDocument d = null;
	
		    // get all defaults
		    d = processAdd("niuniu",doc(f("id", "254823"),
	                f("post_status", "1"),
	                f("base_car_NO", "1369"),
	                f("inner_color", "米色"),
	                f("outer_color", "风雅金"),
	                f("user_id", "37947"),
	                f("company_owner_id", ""),
	                f("user_name", "段磊"),
	                f("firm_name", "上汽大众"),
	                f("post_type", "0"),
	                f("guiding_price", "13.69"),
	                f("resource_type", "0"),
	                f("standard", "国产"),
	                f("base_car_year", "2015"),
	                f("configuration_remark", ""),
	                f("base_car_status", "1"),
	                f("base_car_style", "1.6L 自动舒适版"),
	                f("engine_capacity", "1.8T"),
	                f("brand", "大众"),
	                f("car_model_name", "朗逸"),
	                f("user_city_name", "上海"),
	                f("user_prov_name", "上海"),
	                f("remark", "呵呵哒"),
	                f("user_vip_level", "4"),
	                f("created_at", "2017-06-20T15:06:46.781541Z"),
	                f("updated_at", "2017-06-20T15:06:46.803441Z"),
	                f("maintained_at", "2017-06-20T15:06:46.780828Z"),
	                f("expired_at", "2017-07-20T15:06:46.78107Z"),
	                f("user_created_at", "2016-05-17T22:04:58.398932Z"),
	                f("expect_price", "11.19"),
	                f("discount_way", "2"),
	                f("discount_content", "2.50"),
	                f("user_level", "3"),
	                f("post_margin", "1000"),
	                f("base_price", "13.69"),
	                f("last_base_price", "0.00"),
	                f("parent_base_price", "0.00"),
	                f("aasm_state", ""),
	                f("source_prov_name", ""),
	                f("short_name", ""),
	                f("photo_image", "3fb435b2c7.jpg"),
	                f("non_member_price", ""),
	                f("configuration_price", "0.00"),
	                f("frame_number", ""),
	                f("standard_id", "1"),
	                f("car_model_status", "1"),
	                f("niuniu_search_resource_orders", "0"),
	                f("brand_id", "15"),
	                f("brand_pinyin", "da zhong"),
	                f("car_model_id", "99"),
	                f("base_car_id", "9942"),
	                f("car_in_area", "东区"),
	                f("take_car_area", "东区"),
	                f("ticket_pack_time", "手续3-7天"),
	                f("complete_order_count", "42"),
	                f("is_vip_resource", "0"),
	                f("is_batch_resource", "0"),
	                f("niuniu_search_is_unqualified_seller", "0"),
	                f("niuniu_search_company_id", "2205")));
		    
		    assertEquals("备注: 呵呵哒", d.getFieldValue("remark"));
		    assertEquals("2017-06-20T15:06:46.781Z", d.getFieldValue("created_at"));
		    assertEquals("段磊(上海)", d.getFieldValue("user_name"));
		    assertEquals("认证综展", d.getFieldValue("user_level_name"));
		    assertEquals("朗逸 15款 1.6L 自动舒适版", d.getFieldValue("title"));
		    assertEquals("指导价: 13.69万/下2.5万", d.getFieldValue("info_price"));
		    assertEquals("1 % 1.6L 自动舒适版 % 13.69万", d.getFieldValue("base_car_style_full_name_s"));
		    assertEquals("da zhong,1,上汽大众", d.getFieldValue("brand_firm_standard_s"));
		    assertEquals("风雅金/米色|手续3-7天|销东区|车在东区", d.getFieldValue("extend_info"));
		    assertEquals("|成交量:42", d.getFieldValue("user_complete_order_text"));
		    assertEquals("指导价: 13.69万/下18.26点", d.getFieldValue("info_price_percentage"));
		    assertEquals("指导价: 13.69万/下2.5万", d.getFieldValue("info_price_amount"));
		    assertEquals("现车", d.getFieldValue("resource_type_s"));
		    assertEquals("国产/现车", d.getFieldValue("subtitle"));
		    assertEquals("15款 1.6L 自动舒适版 1.8T", d.getFieldValue("base_car_style"));
		    
		    Collection<Object> base_car_NO = d.getFieldValues("base_car_NO");
		    ArrayList<Integer> expected = new ArrayList<>();
		    expected.add(1369);
		    expected.add(136900);
		    assertEquals(expected, base_car_NO);
		    
		    Collection<Object> last_price_NO = d.getFieldValues("last_price_NO");
		    expected = new ArrayList<>();
		    expected.add(0);
		    assertEquals(expected, last_price_NO);
		    
		    Collection<Object> parent_price_NO = d.getFieldValues("parent_price_NO");
		    expected = new ArrayList<>();
		    expected.add(0);
		    assertEquals(expected, parent_price_NO);
		    
		    Collection<Object> content = d.getFieldValues("base_car_year_info");
		    ArrayList<String> expected2 = new ArrayList<>();
		    expected2.add("15款");
		    expected2.add("2015款");
		    assertEquals(expected2, content);
		    
		    assertEquals(5, d.getFieldValue("niuniu_search_order_score"));
		    assertEquals(2205, d.getFieldValue("niuniu_search_company_id"));
		    assertEquals(0, d.getFieldValue("niuniu_search_is_unqualified_seller"));
		}
		
		{
			SolrInputDocument d = null;
	
		    // get all defaults
		    d = processAdd("niuniu",doc(f("id", "254823"),
	                f("post_status", "1"),
	                f("base_car_NO", "135"),
	                f("inner_color", "米色"),
	                f("outer_color", "风雅金"),
	                f("user_id", "37947"),
	                f("company_owner_id", ""),
	                f("user_name", "段磊"),
	                f("firm_name", "上汽大众"),
	                f("post_type", "0"),
	                f("guiding_price", "13.50"),
	                f("resource_type", "0"),
	                f("standard", "国产"),
	                f("base_car_year", "2015"),
	                f("configuration_remark", ""),
	                f("base_car_status", "1"),
	                f("base_car_style", "1.6L 自动舒适版"),
	                f("engine_capacity", "1.8T"),
	                f("brand", "大众"),
	                f("car_model_name", "朗逸"),
	                f("user_city_name", "上海"),
	                f("user_prov_name", "上海"),
	                f("remark", "呵呵哒"),
	                f("user_vip_level", "4"),
	                f("created_at", "2017-06-20T15:06:46.781541Z"),
	                f("updated_at", "2017-06-20T15:06:46.803441Z"),
	                f("maintained_at", "2017-06-20T15:06:46.780828Z"),
	                f("expired_at", "2017-07-20T15:06:46.78107Z"),
	                f("user_created_at", "2016-05-17T22:04:58.398932Z"),
	                f("expect_price", "11.00"),
	                f("discount_way", "2"),
	                f("discount_content", "2.50"),
	                f("user_level", "3"),
	                f("post_margin", "1000"),
	                f("base_price", "13.69"),
	                f("last_base_price", "13.59"),
	                f("parent_base_price", "13.49"),
	                f("aasm_state", ""),
	                f("source_prov_name", ""),
	                f("short_name", ""),
	                f("photo_image", "3fb435b2c7.jpg"),
	                f("non_member_price", ""),
	                f("configuration_price", "0.00"),
	                f("frame_number", ""),
	                f("standard_id", "1"),
	                f("car_model_status", "1"),
	                f("niuniu_search_resource_orders", "0"),
	                f("brand_id", "15"),
	                f("brand_pinyin", "da zhong"),
	                f("car_model_id", "99"),
	                f("base_car_id", "9942"),
	                f("car_in_area", "东区"),
	                f("take_car_area", "东区"),
	                f("ticket_pack_time", "手续3-7天"),
	                f("complete_order_count", "42"),
	                f("is_vip_resource", "0"),
	                f("is_batch_resource", "0"),
	                f("niuniu_search_is_unqualified_seller", "0"),
	                f("niuniu_search_company_id", "2205")));
		    
		    assertEquals("备注: 呵呵哒", d.getFieldValue("remark"));
		    assertEquals("2017-06-20T15:06:46.781Z", d.getFieldValue("created_at"));
		    assertEquals("段磊(上海)", d.getFieldValue("user_name"));
		    assertEquals("认证综展", d.getFieldValue("user_level_name"));
		    assertEquals("朗逸 15款 1.6L 自动舒适版", d.getFieldValue("title"));
		    assertEquals("指导价: 13.5万/下2.5万", d.getFieldValue("info_price"));
		    assertEquals("1 % 1.6L 自动舒适版 % 13.69万", d.getFieldValue("base_car_style_full_name_s"));
		    assertEquals("da zhong,1,上汽大众", d.getFieldValue("brand_firm_standard_s"));
		    assertEquals("风雅金/米色|手续3-7天|销东区|车在东区", d.getFieldValue("extend_info"));
		    assertEquals("|成交量:42", d.getFieldValue("user_complete_order_text"));
		    assertEquals("指导价: 13.5万/下18.52点", d.getFieldValue("info_price_percentage"));
		    assertEquals("指导价: 13.5万/下2.5万", d.getFieldValue("info_price_amount"));
		    assertEquals("现车", d.getFieldValue("resource_type_s"));
		    assertEquals("国产/现车", d.getFieldValue("subtitle"));
		    assertEquals("15款 1.6L 自动舒适版 1.8T", d.getFieldValue("base_car_style"));
		    
		    Collection<Object> base_car_NO = d.getFieldValues("base_car_NO");
		    ArrayList<Integer> expected = new ArrayList<>();
		    expected.add(1369);
		    expected.add(136900);
		    assertEquals(expected, base_car_NO);
		    
		    Collection<Object> last_price_NO = d.getFieldValues("last_price_NO");
		    expected = new ArrayList<>();
		    expected.add(1359);
		    expected.add(135900);
		    assertEquals(expected, last_price_NO);
		    
		    Collection<Object> parent_price_NO = d.getFieldValues("parent_price_NO");
		    expected = new ArrayList<>();
		    expected.add(1349);
		    expected.add(134900);
		    assertEquals(expected, parent_price_NO);
		    
		    Collection<Object> content = d.getFieldValues("base_car_year_info");
		    ArrayList<String> expected2 = new ArrayList<>();
		    expected2.add("15款");
		    expected2.add("2015款");
		    assertEquals(expected2, content);
		    
		    assertEquals(5, d.getFieldValue("niuniu_search_order_score"));
		    assertEquals(2205, d.getFieldValue("niuniu_search_company_id"));
		    assertEquals(0, d.getFieldValue("niuniu_search_is_unqualified_seller"));
		}
		
		{
			SolrInputDocument d = null;
	
		    // get all defaults
		    d = processAdd("niuniu",doc(f("id", "254823"),
	                f("post_status", "1"),
	                f("base_car_NO", "135"),
	                f("inner_color", "米色"),
	                f("outer_color", "风雅金"),
	                f("user_id", "37947"),
	                f("company_owner_id", ""),
	                f("user_name", "段磊"),
	                f("firm_name", "上汽大众"),
	                f("post_type", "0"),
	                f("guiding_price", "13.50"),
	                f("resource_type", "0"),
	                f("standard", "国产"),
	                f("base_car_year", "2015"),
	                f("configuration_remark", ""),
	                f("base_car_status", "1"),
	                f("base_car_style", "1.6L 自动舒适版"),
	                f("engine_capacity", "1.8T"),
	                f("brand", "大众"),
	                f("car_model_name", "朗逸"),
	                f("user_city_name", "上海"),
	                f("user_prov_name", "上海"),
	                f("remark", "呵呵哒"),
	                f("user_vip_level", "4"),
	                f("created_at", "2017-06-20T15:06:46.781541Z"),
	                f("updated_at", "2017-06-20T15:06:46.803441Z"),
	                f("maintained_at", "2017-06-20T15:06:46.780828Z"),
	                f("expired_at", "2017-07-20T15:06:46.78107Z"),
	                f("user_created_at", "2016-05-17T22:04:58.398932Z"),
	                f("expect_price", "11.00"),
	                f("discount_way", "2"),
	                f("discount_content", "2.50"),
	                f("user_level", "3"),
	                f("post_margin", "1000"),
	                f("base_price", "13.50"),
	                f("parent_base_price", "13.00"),
	                f("last_base_price", "13.45"),
	                f("aasm_state", ""),
	                f("source_prov_name", ""),
	                f("short_name", ""),
	                f("photo_image", "3fb435b2c7.jpg"),
	                f("non_member_price", ""),
	                f("configuration_price", "0.00"),
	                f("frame_number", ""),
	                f("standard_id", "1"),
	                f("car_model_status", "1"),
	                f("niuniu_search_resource_orders", "0"),
	                f("brand_id", "15"),
	                f("brand_pinyin", "da zhong"),
	                f("car_model_id", "99"),
	                f("base_car_id", "9942"),
	                f("car_in_area", "东区"),
	                f("take_car_area", "东区"),
	                f("ticket_pack_time", "手续3-7天"),
	                f("complete_order_count", "42"),
	                f("is_vip_resource", "0"),
	                f("is_batch_resource", "0"),
	                f("niuniu_search_is_unqualified_seller", "0"),
	                f("niuniu_search_company_id", "2205")));
		    
		    assertEquals("备注: 呵呵哒", d.getFieldValue("remark"));
		    assertEquals("2017-06-20T15:06:46.781Z", d.getFieldValue("created_at"));
		    assertEquals("段磊(上海)", d.getFieldValue("user_name"));
		    assertEquals("认证综展", d.getFieldValue("user_level_name"));
		    assertEquals("朗逸 15款 1.6L 自动舒适版", d.getFieldValue("title"));
		    assertEquals("指导价: 13.5万/下2.5万", d.getFieldValue("info_price"));
		    assertEquals("1 % 1.6L 自动舒适版 % 13.50万", d.getFieldValue("base_car_style_full_name_s"));
		    assertEquals("da zhong,1,上汽大众", d.getFieldValue("brand_firm_standard_s"));
		    assertEquals("风雅金/米色|手续3-7天|销东区|车在东区", d.getFieldValue("extend_info"));
		    assertEquals("|成交量:42", d.getFieldValue("user_complete_order_text"));
		    assertEquals("指导价: 13.5万/下18.52点", d.getFieldValue("info_price_percentage"));
		    assertEquals("指导价: 13.5万/下2.5万", d.getFieldValue("info_price_amount"));
		    assertEquals("现车", d.getFieldValue("resource_type_s"));
		    assertEquals("国产/现车", d.getFieldValue("subtitle"));
		    assertEquals("15款 1.6L 自动舒适版 1.8T", d.getFieldValue("base_car_style"));
		    
		    Collection<Object> base_car_NO = d.getFieldValues("base_car_NO");
		    ArrayList<Integer> expected = new ArrayList<>();
		    expected.add(1350);
		    expected.add(135000);
		    expected.add(135);
		    assertEquals(expected, base_car_NO);
		    
		    Collection<Object> last_price_NO = d.getFieldValues("last_price_NO");
		    expected = new ArrayList<>();
		    expected.add(1345);
		    expected.add(134500);
		    assertEquals(expected, last_price_NO);
		    
		    Collection<Object> parent_price_NO = d.getFieldValues("parent_price_NO");
		    expected = new ArrayList<>();
		    expected.add(1300);
		    expected.add(130000);
		    expected.add(13);
		    assertEquals(expected, parent_price_NO);
		    
		    Collection<Object> content = d.getFieldValues("base_car_year_info");
		    ArrayList<String> expected2 = new ArrayList<>();
		    expected2.add("15款");
		    expected2.add("2015款");
		    assertEquals(expected2, content);
		    
		    assertEquals(5, d.getFieldValue("niuniu_search_order_score"));
		    assertEquals(2205, d.getFieldValue("niuniu_search_company_id"));
		    assertEquals(0, d.getFieldValue("niuniu_search_is_unqualified_seller"));
		}
		
		{
			SolrInputDocument d = null;
	
		    // get all defaults
		    d = processAdd("niuniu",doc(f("id", "254823"),
	                f("post_status", "1"),
	                f("base_car_NO", "135"),
	                f("inner_color", "米色"),
	                f("outer_color", "风雅金"),
	                f("user_id", "37947"),
	                f("company_owner_id", ""),
	                f("user_name", "段磊"),
	                f("firm_name", "上汽大众"),
	                f("post_type", "0"),
	                f("guiding_price", "13.50"),
	                f("resource_type", "0"),
	                f("standard", "国产"),
	                f("base_car_year", "2015"),
	                f("configuration_remark", ""),
	                f("base_car_status", "1"),
	                f("base_car_style", "1.6L 自动舒适版"),
	                f("engine_capacity", "1.8T"),
	                f("brand", "大众"),
	                f("car_model_name", "朗逸"),
	                f("user_city_name", "上海"),
	                f("user_prov_name", "上海"),
	                f("remark", "呵呵哒"),
	                f("user_vip_level", "4"),
	                f("created_at", "2017-06-20T15:06:46.781541Z"),
	                f("updated_at", "2017-06-20T15:06:46.803441Z"),
	                f("maintained_at", "2017-06-20T15:06:46.780828Z"),
	                f("expired_at", "2017-07-20T15:06:46.78107Z"),
	                f("user_created_at", "2016-05-17T22:04:58.398932Z"),
	                f("expect_price", "11.00"),
	                f("discount_way", "2"),
	                f("discount_content", "2.50"),
	                f("user_level", "3"),
	                f("post_margin", "1000"),
	                f("base_price", "13.50"),
	                f("parent_base_price", "13.50"),
	                f("last_base_price", "13.50"),
	                f("aasm_state", ""),
	                f("source_prov_name", ""),
	                f("short_name", ""),
	                f("photo_image", "3fb435b2c7.jpg"),
	                f("non_member_price", ""),
	                f("configuration_price", "0.00"),
	                f("frame_number", ""),
	                f("standard_id", "1"),
	                f("car_model_status", "1"),
	                f("niuniu_search_resource_orders", "0"),
	                f("brand_id", "15"),
	                f("brand_pinyin", "da zhong"),
	                f("car_model_id", "99"),
	                f("base_car_id", "9942"),
	                f("car_in_area", "东区"),
	                f("take_car_area", "东区"),
	                f("ticket_pack_time", "手续3-7天"),
	                f("complete_order_count", "42"),
	                f("is_vip_resource", "0"),
	                f("is_batch_resource", "0"),
	                f("user_deposit", "1"),
	                f("promotion_resource_expired_at", "2017-09-20T04:52:22.277123Z"),
	                f("niuniu_search_is_unqualified_seller", "0"),
	                f("niuniu_search_company_id", "2205")));
		    
		    assertEquals("备注: 呵呵哒", d.getFieldValue("remark"));
		    assertEquals("2017-06-20T15:06:46.781Z", d.getFieldValue("created_at"));
		    assertEquals("段磊(上海)", d.getFieldValue("user_name"));
		    assertEquals("认证综展", d.getFieldValue("user_level_name"));
		    assertEquals("朗逸 15款 1.6L 自动舒适版", d.getFieldValue("title"));
		    assertEquals("指导价: 13.5万/下2.5万", d.getFieldValue("info_price"));
		    assertEquals("1 % 1.6L 自动舒适版 % 13.50万", d.getFieldValue("base_car_style_full_name_s"));
		    assertEquals("da zhong,1,上汽大众", d.getFieldValue("brand_firm_standard_s"));
		    assertEquals("风雅金/米色|手续3-7天|销东区|车在东区", d.getFieldValue("extend_info"));
		    assertEquals("|成交量:42", d.getFieldValue("user_complete_order_text"));
		    assertEquals("指导价: 13.5万/下18.52点", d.getFieldValue("info_price_percentage"));
		    assertEquals("指导价: 13.5万/下2.5万", d.getFieldValue("info_price_amount"));
		    assertEquals("现车", d.getFieldValue("resource_type_s"));
		    assertEquals("国产/现车", d.getFieldValue("subtitle"));
		    assertEquals("15款 1.6L 自动舒适版 1.8T", d.getFieldValue("base_car_style"));
		    assertEquals(1, d.getFieldValue("user_deposit"));
		    assertEquals(1505883142277l, d.getFieldValue("promotion_resource_expired_at_ms"));
		    
		    Collection<Object> base_car_NO = d.getFieldValues("base_car_NO");
		    ArrayList<Integer> expected = new ArrayList<>();
		    expected.add(1350);
		    expected.add(135000);
		    expected.add(135);
		    assertEquals(expected, base_car_NO);
		    
		    Collection<Object> last_price_NO = d.getFieldValues("last_price_NO");
		    expected = new ArrayList<>();
		    expected.add(0);
		    assertEquals(expected, last_price_NO);
		    
		    Collection<Object> parent_price_NO = d.getFieldValues("parent_price_NO");
		    expected = new ArrayList<>();
		    expected.add(0);
		    assertEquals(expected, parent_price_NO);
		    
		    Collection<Object> content = d.getFieldValues("base_car_year_info");
		    ArrayList<String> expected2 = new ArrayList<>();
		    expected2.add("15款");
		    expected2.add("2015款");
		    assertEquals(expected2, content);
		    
		    assertEquals(5, d.getFieldValue("niuniu_search_order_score"));
		    assertEquals(2205, d.getFieldValue("niuniu_search_company_id"));
		    assertEquals(0, d.getFieldValue("niuniu_search_is_unqualified_seller"));
		}
		
		{
			SolrInputDocument d = null;
	
		    // get all defaults
		    d = processAdd("niuniu",doc(f("id", "254823"),
	                f("post_status", "1"),
	                f("base_car_NO", "848"),
	                f("inner_color", "米色"),
	                f("outer_color", "风雅金"),
	                f("user_id", "37947"),
	                f("company_owner_id", ""),
	                f("user_name", "段磊"),
	                f("firm_name", "上汽大众"),
	                f("post_type", "0"),
	                f("guiding_price", "8.48"),
	                f("resource_type", "0"),
	                f("standard", "国产"),
	                f("base_car_year", "2015"),
	                f("configuration_remark", ""),
	                f("base_car_status", "1"),
	                f("base_car_style", "1.6L 自动舒适版"),
	                f("engine_capacity", "1.8T"),
	                f("brand", "大众"),
	                f("car_model_name", "朗逸"),
	                f("user_city_name", "上海"),
	                f("user_prov_name", "上海"),
	                f("remark", "呵呵哒"),
	                f("user_vip_level", "4"),
	                f("created_at", "2017-06-20T15:06:46.781541Z"),
	                f("updated_at", "2017-06-20T15:06:46.803441Z"),
	                f("maintained_at", "2017-06-20T15:06:46.780828Z"),
	                f("expired_at", "2017-07-20T15:06:46.78107Z"),
	                f("user_created_at", "2016-05-17T22:04:58.398932Z"),
	                f("expect_price", "11.00"),
	                f("discount_way", "2"),
	                f("discount_content", "2.50"),
	                f("user_level", "3"),
	                f("post_margin", "1000"),
	                f("base_price", "8.48"),
	                f("parent_base_price", "8.48"),
	                f("last_base_price", "8.48"),
	                f("aasm_state", ""),
	                f("source_prov_name", ""),
	                f("short_name", ""),
	                f("photo_image", "3fb435b2c7.jpg"),
	                f("non_member_price", ""),
	                f("configuration_price", "0.00"),
	                f("frame_number", ""),
	                f("standard_id", "1"),
	                f("car_model_status", "1"),
	                f("niuniu_search_resource_orders", "0"),
	                f("brand_id", "15"),
	                f("brand_pinyin", "da zhong"),
	                f("car_model_id", "99"),
	                f("base_car_id", "9942"),
	                f("car_in_area", "东区"),
	                f("take_car_area", "东区"),
	                f("ticket_pack_time", "手续3-7天"),
	                f("complete_order_count", "42"),
	                f("is_vip_resource", "0"),
	                f("is_batch_resource", "0"),
	                f("user_deposit", "1"),
	                f("promotion_resource_expired_at", "2017-09-20T04:52:22.277123Z"),
	                f("niuniu_search_is_unqualified_seller", "0"),
	                f("niuniu_search_company_id", "2205")));
		    
		    assertEquals("备注: 呵呵哒", d.getFieldValue("remark"));
		    assertEquals("2017-06-20T15:06:46.781Z", d.getFieldValue("created_at"));
		    assertEquals("段磊(上海)", d.getFieldValue("user_name"));
		    assertEquals("认证综展", d.getFieldValue("user_level_name"));
		    assertEquals("朗逸 15款 1.6L 自动舒适版", d.getFieldValue("title"));
		    assertEquals("指导价: 8.48万/下2.5万", d.getFieldValue("info_price"));
		    assertEquals("1 % 1.6L 自动舒适版 % 8.48万", d.getFieldValue("base_car_style_full_name_s"));
		    assertEquals("da zhong,1,上汽大众", d.getFieldValue("brand_firm_standard_s"));
		    assertEquals("风雅金/米色|手续3-7天|销东区|车在东区", d.getFieldValue("extend_info"));
		    assertEquals("|成交量:42", d.getFieldValue("user_complete_order_text"));
		    assertEquals("现车", d.getFieldValue("resource_type_s"));
		    assertEquals("国产/现车", d.getFieldValue("subtitle"));
		    assertEquals("15款 1.6L 自动舒适版 1.8T", d.getFieldValue("base_car_style"));
		    assertEquals(1, d.getFieldValue("user_deposit"));
		    assertEquals(1505883142277l, d.getFieldValue("promotion_resource_expired_at_ms"));
		    
		    Collection<Object> base_car_NO = d.getFieldValues("base_car_NO");
		    ArrayList<Integer> expected = new ArrayList<>();
		    expected.add(848);
		    expected.add(84800);
		    assertEquals(expected, base_car_NO);
		    
		    Collection<Object> last_price_NO = d.getFieldValues("last_price_NO");
		    expected = new ArrayList<>();
		    expected.add(0);
		    assertEquals(expected, last_price_NO);
		    
		    Collection<Object> parent_price_NO = d.getFieldValues("parent_price_NO");
		    expected = new ArrayList<>();
		    expected.add(0);
		    assertEquals(expected, parent_price_NO);
		    
		    Collection<Object> content = d.getFieldValues("base_car_year_info");
		    ArrayList<String> expected2 = new ArrayList<>();
		    expected2.add("15款");
		    expected2.add("2015款");
		    assertEquals(expected2, content);
		    
		    assertEquals(5, d.getFieldValue("niuniu_search_order_score"));
		    assertEquals(2205, d.getFieldValue("niuniu_search_company_id"));
		    assertEquals(0, d.getFieldValue("niuniu_search_is_unqualified_seller"));
		}
	}
	
	@Test
	public void testNiuniuProcessorParallelResource() throws Exception {
		SolrInputDocument d = null;

	    // get all defaults
	    d = processAdd("niuniu_parallel",doc(f("id", "12345"),
                f("post_status", "1"),
                f("base_car_NO", ""),
                f("inner_color", "米色"),
                f("outer_color", "风雅金"),
                f("user_id", "37947"),
                f("company_owner_id", ""),
                f("user_name", "段磊"),
                f("firm_name", "路虎(平行进口)"),
                f("post_type", "0"),
                f("guiding_price", "0.00"),
                f("resource_type", "0"),
                f("standard", "欧版"),
                f("base_car_year", "2017"),
                f("configuration_remark", "20寸轮 倒影 380音响 感应电尾 led氙灯, TFT液晶仪表盘"),
                f("base_car_status", "1"),
                f("base_car_style", "呵呵呵呵嘻嘻嘻"),
                f("brand", "路虎"),
                f("car_model_name", "揽胜运动3.0柴油"),
                f("user_city_name", "上海"),
                f("user_prov_name", "上海"),
                f("remark", "呵呵哒"),
                f("user_vip_level", "4"),
                f("created_at", "2017-06-20T15:06:46.781541Z"),
                f("updated_at", "2017-06-20T15:06:46.803441Z"),
                f("maintained_at", "2017-06-20T15:06:46.780828Z"),
                f("expired_at", "2017-07-20T15:06:46.78107Z"),
                f("user_created_at", "2016-05-17T22:04:58.398932Z"),
                f("expect_price", ""),
                f("discount_way", "4"),
                f("discount_content", "40.2"),
                f("user_level", "3"),
                f("post_margin", ""),
                f("base_price", "0.00"),
                f("aasm_state", ""),
                f("source_prov_name", ""),
                f("short_name", ""),
                f("photo_image", ""),
                f("non_member_price", ""),
                f("configuration_price", "0.00"),
                f("frame_number", ""),
                f("standard_id", "5"),
                f("car_model_status", "1"),
                f("niuniu_search_resource_orders", "0"),
                f("brand_id", "15"),
                f("brand_pinyin", "da zhong"),
                f("car_model_id", "10203"),
                f("base_car_id", "33867"),
                f("car_in_area", "东区"),
                f("take_car_area", "东区"),
                f("ticket_pack_time", "手续3-7天"),
                f("complete_order_count", "42"),
                f("is_vip_resource", "0"),
                f("is_batch_resource", "0"),
                f("niuniu_search_is_unqualified_seller", "0"),
                f("niuniu_search_company_id", "2205")));

	    assertEquals(5, d.getFieldValue("niuniu_search_order_score"));
	    assertEquals(2205, d.getFieldValue("niuniu_search_company_id"));
	    assertEquals(0, d.getFieldValue("niuniu_search_is_unqualified_seller"));
	    
	    {
		    Collection<Object> content = d.getFieldValues("parallel_configs");
		    ArrayList<String> expected = new ArrayList<>();
		    expected.add("380w音响");
		    expected.add("液晶仪表盘");
		    expected.add("20轮");
		    expected.add("led氙气大灯");
		    expected.add("倒车影像");
		    expected.add("电尾门");
		    
		    assertEquals(expected, content);
	    }
	    
	    {
	    	Collection<Object> base_car_NO = d.getFieldValues("base_car_NO");
		    ArrayList<Integer> expected = new ArrayList<>();
		    expected.add(0);
		    assertEquals(expected, base_car_NO);
		    
		    Collection<Object> last_price_NO = d.getFieldValues("last_price_NO");
		    expected = new ArrayList<>();
		    expected.add(0);
		    assertEquals(expected, last_price_NO);
		    
		    Collection<Object> parent_price_NO = d.getFieldValues("parent_price_NO");
		    expected = new ArrayList<>();
		    expected.add(0);
		    assertEquals(expected, parent_price_NO);
		    
		    Collection<Object> content = d.getFieldValues("base_car_year_info");
		    ArrayList<String> expected2 = new ArrayList<>();
		    expected2.add("17款");
		    expected2.add("2017款");
		    assertEquals(expected2, content);
	    }
	    
	    
	}
	
	static void checkNumDocs(int n) {
		SolrQueryRequest req = req();
		try {
			assertEquals(n, req.getSearcher().getIndexReader().numDocs());
		} finally {
			req.close();
		}
	}

	private void addDoc(String doc) throws Exception {
		Map<String, String[]> params = new HashMap<String, String[]>();
		MultiMapSolrParams mmparams = new MultiMapSolrParams(params);
		params.put(UpdateParams.UPDATE_CHAIN, new String[] { "niuniu" });
		SolrQueryRequestBase req = new SolrQueryRequestBase(h.getCore(), (SolrParams) mmparams) {
		};

		UpdateRequestHandler handler = new UpdateRequestHandler();
		handler.init(null);
		ArrayList<ContentStream> streams = new ArrayList<ContentStream>(2);
		streams.add(new ContentStreamBase.StringStream(doc));
		req.setContentStreams(streams);
		handler.handleRequestBody(req, new SolrQueryResponse());
		req.close();
	}

	SolrInputDocument processAdd(final String chain, final SolrInputDocument docIn) throws IOException {

		SolrCore core = h.getCore();
		UpdateRequestProcessorChain pc = core.getUpdateProcessingChain(chain);
		assertNotNull("No Chain named: " + chain, pc);

		SolrQueryResponse rsp = new SolrQueryResponse();

		SolrQueryRequest req = new LocalSolrQueryRequest(core, new ModifiableSolrParams());
		try {
			SolrRequestInfo.setRequestInfo(new SolrRequestInfo(req, rsp));
			AddUpdateCommand cmd = new AddUpdateCommand(req);
			cmd.solrDoc = docIn;

			UpdateRequestProcessor processor = pc.createProcessor(req, rsp);
			processor.processAdd(cmd);

			return cmd.solrDoc;
		} finally {
			SolrRequestInfo.clearRequestInfo();
			req.close();
		}
	}
	
	SolrInputField field(String name, float boost, Object... values) {
		SolrInputField f = new SolrInputField(name);
		for (Object v : values) {
			f.addValue(v, 1.0F);
		}
		f.setBoost(boost);
		return f;
	}
	
	SolrInputField f(String name, Object... values) {
		return field(name, 1.0F, values);
	}
	SolrInputDocument doc(SolrInputField... fields) {
		SolrInputDocument d = new SolrInputDocument();
		for (SolrInputField f : fields) {
			d.put(f.getName(), f);
		}
		return d;
	}
}
