package com.niuniu.synonym;

import org.apache.solr.SolrTestCaseJ4;
import org.junit.BeforeClass;

public class TestNiuniuSynonymFilterFactory extends SolrTestCaseJ4{
	@BeforeClass
	  public static void beforeClass() throws Exception {
	    initCore("solrconfig-niuniu.xml","schema-niuniu.xml");
	  }
}
