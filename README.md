##背景：  
***

###牛牛粗排  

  文本相关性，由于分数是较小的浮点数，为了提高精排阶段的可操作空间，把该分数放大10000倍  

###牛牛精排  

1. 排序因子  

	| 排序特征                | 特征含义       |使用类型| 特征权重  |
	| ---------------------- |:-------------	:| -----:| -----:|
	| is_vip_resource        | 会员置顶品牌       | 嵌入 |   1000  |
	| user_level             | 用户认证等级      |   嵌入 |     50  |
	| is_reliable_resource   | 靠谱金资源      |    嵌入 |      0 |
	| not_official_resource  | 自定义车型      |    嵌入 |   -2200|
	| is_batch_resource      | 批量发布资源      |    嵌入 |   -10    |
	| has_no_price           | 价格电议资源      |    嵌入 |   -30   |
	| niuniu_search_is_unqualified_seller  | 商家资质不合格      |    嵌入 |   -100  |
	| niuniu_search_is_vip   | 买卖通会员      |    嵌入|   15    |
	| niuniu_search_order_score  | 线上交易加分      |    嵌入 |    2   |
	| niuniu_search_is_unqualified_user  | 用户资质不合格      |    嵌入 |   -200  |
	| new_resource_feature  | 新发资源加分      |    独立 |   20    |
	| old_resource_punish_feature  | 老资源罚分      |    独立 |    -1200   |
	| geo_preference_feature  | 地域加权      |    独立 |   8   |
	| random_factor_feature  | 随机因子      |    独立 |   0.001    |
	| promotion_resource_feature  |  保证金资源     |    独立 |   800    |
	| unqualified_punish_resource_feature  |  未进行公司认证的，额外罚分  |    独立 |   -10   |
	
	嵌入的意思是该特征统一在NiuniuSearchFeatureFactory被引用，不需要额外在配置文件中注册  
	
	独立的意思是该特征在被排序链加权引用时需要先注册特征，例如：  

	
```xml
	<featureFactory name="old_resource_punish_feature" class="com.niuniu.search.features.NiuniuOldResourcePunishFeatureFactory" >  
		<str name="fields">updated_at_ms</str>  
		<str name="days_diff">5</str>  
	</featureFactory>  
```
	

	然后在排序链中引用：  
	
```  
	<niuniuSortChain name="wlsort1" class="com.niuniu.rankings.NiuniuRankingServiceChain" >  
		<str name="features">search_feature_4.7, new_resource_feature, old_resource_punish_feature, geo_preference_feature </str>  
		<str name="weights">1,2, -1200, 10</str>  	
	</niuniuSortChain>  
```
	
###牛牛重排  

1. 根据user_id做限制，每个人最多出2条资源，防止被个人霸屏  

2. 根据公司名称做限制，每个公司最多出4条资源，防止被一个公司霸屏  

3. 为每条资源随机加区间在(0,1)的分数，用于微调，例如，森度有5条帕萨特2229的资源，但是搜帕萨特2229，永远只能出2条相同的结果，剩下的3条资源永远无法在资源列表页得到曝光  

***

**为了解决同义词在构造布尔查询时的遗留问题，修改了同义词解析模块以及NiuniuQueryParser模块**  

solr内置的同义词映射模块支持的功能很简单，无法把词条的type带过来  

所以第一版的牛牛同义词模块就支持了type，从而在queryparser中根据term的type来构造相应的布尔查询  
    例如：新朗逸 => 朗逸#MODEL, 17款#STYLE  

        新朗逸分词和tagging后得到的结果是：朗逸#MODEL 17款#STYLE，在queryparser中就可以根据这些信息来构造完美的布尔查询：  
        car_model_name:朗逸 AND base_car_style:17款  

但是，同义词右侧的部分，有的时候需要用AND，例如新朗逸，而有的时候需要用OR  

例如2017 => 2017#PRICE, 17款#STYLE，而之前的解析模块无法灵活支持这种方式  
    
    解决方案：同义词模块默认使用AND来链接布尔查询  

    但是如果想根据同义词中的模块构造OR的布尔查询，就写成 2017 => 2017#SYNONYM_PRICE, 17款#SYNONYM_STYLE,同时在QueryParser解析的时候，同一个term对应的所有同义词先连接成一个OR的布尔子查询，然后再把这个子查询用search_level定义的方式（search_level=low则是OR，search_level!=low则是AND）放到最终的Query中  
    
2017朗逸2239 对应的最终的布尔查询： 

        search_level=high:  +(base_car_NO:2239^0.5 base_car_style:2239^0.5) +(+car_model_name:朗逸) +(base_car_NO:2017 base_car_style:17款^0.2)  

        search_level=low:   +(base_car_NO:2239^0.5 base_car_style:2239^0.5) +(+car_model_name:朗逸) (base_car_NO:2017 base_car_style:17款^0.2)  

可以看到根据2017构造出来的query分别是AND和OR  

在进行查询解析时，如果token是以"版"、"款"等结尾的，就要把"版"、"款"去掉去检索，以支持搜"尚锐版"可以检索到"尚锐型"   

###牛牛索引插件链  

NiuniuProcessorFactory.java负责主搜的逻辑，基本内容是把传给solr的doc进行实时新的逻辑计算，从而使得全量和增量可以使用一套逻辑  

BusinessCircleProcessorFactory.java 生意圈的实时打标逻辑，标记该生意圈信息是什么品牌  

ParallelConfigProcessorFactory.java 解析资源中的备注，给资源实时打上平行进口车的配置标签  


###Similarity  

粗排打分逻辑，计算文本相关性  

###Synonym  

同义词逻辑改动，用于支持现有同义词功能，例如继承同义词的TYPE，或者多重模式的同义词映射，例如 新帕=> 新款#STYLE, 帕萨特#MODEL  


